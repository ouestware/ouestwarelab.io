# OuestWare website

This repository contains [OuestWare's website](https://www.ouestware.com/en/) sources.
The fully static site is built in [TypeScript](https://www.typescriptlang.org/) on top of
[Next.js](https://nextjs.org/), with some [Skeleton.css](http://getskeleton.com/) help
for the layout.

To use Next.js as a static site generator, we started from the existing
[blog starter](https://github.com/vercel/next.js/tree/canary/examples/blog-starter) for _Next.js_.

## Deploy locally

Clone the repository, then open it and run:

```bash
npm install # to install dependencies
npm run dev # to start a development server
```

You can then open http://localhost:3000 in your browser.
The development server will watch the source base and automatically rebuild the code
and reload the pages in your browser.

## Export the website as static pages

Run `npm run export`.
It will build each pages, and export the whole website as HTML pages (with the related assets)
and store them in the `./out` folder, at the root of the project.

## Some words about **fully static** generation

_Next.js_ aims to build websites, with **_Single Page Applications_** like routing.
It means that the website will load some _Next.js_ JavaScript code, to handle routing,
frontend side rendering and more.

As we did only need some static pages generation, we wanted to avoid loading a large
JavaScript runtime that tackles issues we shouldn't have in a fully static website.

To make _Next.js_ export the lightest possible pages, we had to dig and use [a quite
hidden feature](https://github.com/vercel/next.js/pull/11949), and each page must have:

```typescript
export const config = {
  unstable_runtimeJS: false,
};
```
