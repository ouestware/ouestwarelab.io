import mdx from "@astrojs/mdx";
import react from "@astrojs/react";
import { defineConfig } from "astro/config";
import rehypeRewrite from "rehype-rewrite";

// https://astro.build/config
export default defineConfig({
  site: "https://www.ouestware.com",
  i18n: {
    defaultLocale: "fr",
    locales: ["fr", "en"],
    routing: {
      prefixDefaultLocale: false,
    },
  },
  image: {
    // disable limiInputPixels config in the Sharp-based image service to allow using high-res GIF assets
    service: {
      entrypoint: "astro/assets/services/sharp",
      config: {
        limitInputPixels: false,
      },
    },
  },
  markdown: {
    // Applied to .md and .mdx files
    shikiConfig: {
      // Choose from Shiki's built-in themes (or add your own)
      // https://github.com/shikijs/shiki/blob/main/docs/themes.md
      theme: "github-light",
      // Alternatively, provide multiple themes
      // https://shiki.style/guide/dual-themes#light-dark-dual-themes
      // experimentalThemes: {
      //   light: 'github-light',
      //   dark: 'github-dark',
      // },
      // Add custom languages
      // Note: Shiki has countless langs built-in, including .astro!
      // https://github.com/shikijs/shiki/blob/main/docs/languages.md
      langs: [],
      // Enable word wrap to prevent horizontal scrolling
      wrap: false,
      // Add custom transformers: https://shiki.style/guide/transformers
      // Find common transformers: https://shiki.style/packages/transformers
      transformers: [],
    },
    rehypePlugins: [
      // Some document tree rewrite for customization
      [
        rehypeRewrite,
        {
          selector: "h1,h2,h3,h4,h5,strong,img[title]",
          rewrite: (node) => {
            // add a caption title to images (/!\ only applied in md file not mdx)
            if (node.type === "element" && node.tagName === "img") {
              const title = node.properties.title + "";
              node.children = [
                {
                  ...node,
                },
                {
                  type: "element",
                  tagName: "figcaption",
                  children: [
                    {
                      type: "text",
                      value: title,
                    },
                  ],
                },
              ];
              node.value = "";
              node.tagName = "figure";
              node.properties = {};
              node.content = "";
            }
            // add a <span class='highlight'></span> in blog titles and strong
            else if (node.type === "element") {
              node.children = [
                {
                  type: "element",
                  tagName: "span",
                  properties: {
                    class: "highlight",
                  },
                  children: node.children,
                },
              ];
            }
          },
        },
      ],
    ],
  },
  integrations: [react(), mdx()],
});
