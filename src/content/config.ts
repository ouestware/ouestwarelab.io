// 1. Import utilities from `astro:content`
import { defineCollection, z } from "astro:content";

// 2. Define a `type` and `schema` for each collection
const blogCollection = defineCollection({
  type: "content", // v2.5.0 and later
  schema: ({ image }) =>
    z.object({
      title: z.string(),
      tags: z.array(z.string()),
      excerpt: z.string(),
      lang: z.enum(["fr", "en"]),
      author: z.string().optional(),
      date: z.string(),
      ref: z.string().optional(),
      ogImageURL: image().optional(),
      coverImageURL: image().optional(),
    }),
});

// 3. Export a single `collections` object to register your collection(s)
export const collections = {
  posts: blogCollection,
};
