---
title: "Copie Publique contributions 2024"
excerpt: "Our Copie Publique contributions for 2024"
lang: en
author: "OuestWare"
date: "2024-09-05T00:00:00.000Z"
tags: [donations, copie publique, redistribution]
ref: dons-copie-publique-2024
ogImageURL: ./copie-publique/copie-publique.svg
---

![Copie Publique logo](./copie-publique/copie-publique.svg)

We are a proud member of [Copie Publique](https://copiepublique.fr/en/), which federates companies contributing to fair remunerations for Free/Libre/Open-Source commons. Each member commits to dedicate part of his annual business revenue to Free/Libre/Open-Source projects chosen by its employees.

We decided to give 1% of our sales revenue, this year our "Copie publique" donations have to reach 3050€ HT.

As every year we chose projects which we consider important or helped us. Every one of us can add his ideas to the list. Then we discuss until reaching a consensus about the projects and the amounts to give ([more infos about our process in French only](https://www.ouestware.com/2023/06/27/dons-copie-publique-2023/)). Once donations are done we publish our donations list here and on the copiepublique.fr website.

Please consider joining us at [Copie Publique](https://copiepublique.fr/en) !

## Our 2023-2024 donations

| project          | sponsor link                                                                             | amount (€) | reasons                  |
| ---------------- | ---------------------------------------------------------------------------------------- | ---------- | ------------------------ |
| Let's encrypt    | [https://www.abetterinternet.org/sponsor/](https://www.abetterinternet.org/sponsor/)     | 450        | saved my day             |
| Internet Archive | [https://archive.org/donate](https://archive.org/donate)                                 | 450        | important public good    |
| Vite             | [https://github.com/sponsors/vitejs](https://github.com/sponsors/vitejs)                 | 450        | frequently used          |
| Astro            | [https://opencollective.com/astrodotbuild](https://opencollective.com/astrodotbuild)     | 200        | frequently used          |
| Mozilla (Firefox)| [https://foundation.mozilla.org/fr/donate/](https://foundation.mozilla.org/fr/donate/)   | 200        | frequently used          |
| OpenStreetMap    | [https://donate.openstreetmap.org/](https://donate.openstreetmap.org/)                   | 200        | important public good    |
| MapLibre         | [https://opencollective.com/maplibre/donate](https://opencollective.com/maplibre/donate) | 200        | young project to support |
| Mastodon         | [https://joinmastodon.org/sponsors#donate](https://joinmastodon.org/sponsors#donate)     | 200        | socially important       |
| Prettier         | [https://opencollective.com/prettier](https://opencollective.com/prettier)               | 100        | frequently used          |
| ESLint           | [https://opencollective.com/eslint](https://opencollective.com/eslint)                   | 100        | frequently used          |
| Docusaurous      | [https://opencollective.com/docusaurus](https://opencollective.com/docusaurus)           | 100        | frequently used          |
| Storybook        | [https://opencollective.com/storybook](https://opencollective.com/storybook)             | 100        | frequently used          |
| Open Web Docs    | [https://github.com/sponsors/openwebdocs](https://github.com/sponsors/openwebdocs)       | 100        | frequently used          |
| React-icons      | [https://github.com/sponsors/kamijin-fanta](https://github.com/sponsors/kamijin-fanta)   | 100        | frequently used          |
| Gimp             | [https://www.patreon.com/zemarmot](https://www.patreon.com/zemarmot)                     | 100        | frequently used          |
|                  | Total                                                                                    | 3050       |                          |
