---
title: "Dons Copie Publique 2023"
excerpt: "Avec le mois de Juin arrive la clôture de notre bilan et avec lui un de nos rituels préférés: nos contributions Copie publique !"
lang: fr
author: "OuestWare"
date: "2023-06-27T00:00:00.000Z"
tags: [dons, copie publique, redistribution]
ref: dons-copie-publique-2023
ogImageURL: ./copie-publique/copie-publique.svg
---

![Logo copie publique](./copie-publique/copie-publique.svg)

Avec le mois de Juin arrive la clôture de notre bilan et avec lui un de nos rituels préférés: nos contributions Copie publique !

[Copie Publique](https://copiepublique.fr/) fédère les entreprises qui contribuent à une juste rémunération du numérique libre en s’engageant à donner régulièrement à des projets Open Source.

Pour notre part, nous avons choisi de verser annuellement 1% de notre chiffre d’affaire. Cette année le montant total de nos dons "Copie publique" s’élèvera donc à 2900€ HT.

## Comment décidons nous des destinataires de nos dons ?

Comme pour toute décision chez nous on applique la règle de l’unanimité.
Concrètement nous listons les idées de projets dans un tableur puis nous les discutons une à une.

Parmi les arguments échangés nous avons identifié plusieurs motivations qui nous poussent à vouloir soutenir des projets :

- **jeune projet à soutenir**: projet récent que nous utilisons et que nous souhaitons soutenir pour assurer sa pérennité
- **pierre angulaire**: projet fondamentale pour notre activité
- **utilisation fréquente** : logiciels, librairie ou services que utilisés régulièrement
- **dépendance "saved my day"**: librairie qui a répondu à un besoin clef à un moment clef
- **bien commun important**: ressources libres de droits que nous utilisons et qui nous semble fondamentale même au delà de notre propre usage
- **socialement important**: projets annexes à notre activité professionnelle mais qui mérite d’être soutenus pour leur rôle dans nos sociétés

A l’inverse les raisons d’exclusions ont été :

- projet déjà supporté par une ou des entreprises
- nous contribuons déjà indirectement par un abonnement à un service lié

Cette année nous avons souhaité étendre un peu la liste des récipiendaires possibles en allant fouiller dans les dépendances de nos projets.
Pour ce faire nous avons exécuté la commande `npm fund` dans certains de nos dépôts de code `TypeScript` sur lesquels on a travaillé cette année.
Cette commande liste les paquets qui ont indiqué être ouverts aux dons dans leur déclaration sur npm (le registre des paquets node.js).

<aside>Github propose un système <a href="https://github.com/sponsors/explore">sponsors explore</a> qui automatise cela mais est restreint à l’environnement github: ne fonctionne que pour les dépôts hébergés sur github (nos projets sont hébergés majoritairement sur gitlab) et limité aux projets ayant activé le service <code>github sponsors</code>.</aside>

Nous avons parcouru rapidement à la main cette liste pour tenter d’identifier des librairies que nous ne connaissons pas mais qui reviennent souvent dans les dépendances sur lesquelles se base notre travail. Nous avons caressé l’idée de faire un script pour sortir les statistiques exhaustives mais nous avons renoncé.

![XKCD automation](https://imgs.xkcd.com/comics/automation.png "XKCD automation")

Cette année nous avons décidé d’utiliser trois niveaux d’importance et d’y associer à chacun un montant :

- très important : 500€
- important : 250€
- normal : 100€

En effet nous souhaitons soutenir des projets "coup de cœur" mais aussi des petits projets peu ou pas connu du tout et de répartir nos dons assez librement entre eux.
Pour définir quel projet est dans quelle catégorie nous nous en remettons à la discussion et décision à l’unanimité comme pour le reste.

## Comment donner ?

Puis vient l’étape où il faut trouver un moyen pour réaliser un versement pour chaque projet.
C’est fastidieux mais très intéressant.

En effet ça oblige à se demander qui est derrière tel logiciel, comment il est financé etc.
On apprend beaucoup de choses et nous filtrons rapidement la liste car pas mal de projets auxquels nous avions pensé n’acceptent pas de dons.
Soit parce qu’ils sont déjà bien financés et n’acceptent pas de dons explicitement ([Add a donate button on Jitsi](https://github.com/jitsi/jitsi-meet/issues/5183#issuecomment-600261850)) soit parce que nous n’avons pas trouvé de moyens de faire un don (sans avoir été jusque demander dans une issue comment donner).

Aller chercher cette information permet de découvrir un peu mieux l’écosystème de ses propres dépendances. Nous avons par exemple découvert par le `npm fund` que beaucoup de librairies sont supportées par des mainteneurs "professionnels" ([exemple](https://github.com/sponsors/sindresorhus)) qui ont un très large portefeuille de paquets. Ou encore, qu’une même entreprise ([ThinkMill](https://www.thinkmill.com.au/labs)) supporte deux paquets (react-select et classnames) que nous utilisons très très souvent. Et au passage faire la belle découverte de leur autre produit [KeystoneJS](https://keystonejs.com) qui est un équivalent très intéressant de [Strapi](https://strapi.io/).

Au final quatre moyens de versement ont été utilisés:

<aside>
seul Github Sponsors indique explicitement au moment du paiement le pourcentage de gestion prélevé. Mais cet élément n’a pas été pris en compte.
</aside>

- Open Collective
- Patreon
- Github sponsors
- paiement direct

Notre plateforme favorite est `Open Collective`, simple, efficace. Nous aimons la possibilité de faire un don ponctuel d’un montant libre, de retrouver facilement de vraies factures pour chaque paiement...

Patreon complique un peu les choses pour nous car leur concept est de supporter sur le long terme. Or dans notre cas pour le moment nous souhaitons rester sur des dons annuels.
Nous avons tout de même pu utiliser Patreon en choisissant un abonnement mensuel ou annuel pour l’annuler aussitôt le tout premier versement réalisé.
C’est un peu un hack mais au final ça correspond à notre besoin.
Ce qui rend les choses compliquées c’est que les développeurs peuvent restreindre les montants et types de dons sur patreon.
Aussi on a du abandonner de donner à un projet qui n’acceptait que des abonnement de 1€/mois là où nous souhaitons donner 100€ d’un coup...

Mais de ces difficultés ressort un questionnement sur notre gestion des dons.
En effet la logique d’abonnement de patreon nous a rappelé qu’il est sans doute plus facile pour un mainteneur de gérer des revenus réguliers que des dons ponctuels par à-coup.
C’est une question ouverte mais nous pourrions envisager de réaliser une partie de notre contribution par abonnement.
Reste que cela nous demanderait plus de travail de gestion (comptable) car nous passerions d’un paiement unique à douze paiements par an (et donc autant de factures...).

Nous espérons que ce retour d’expérience pourra inspirer voire convaincre d’autres à nous rejoindre chez [Copie Publique](https://copiepublique.fr/) !

## Liste de nos dons 2022-2023

| projet                   | lien sponsor                                                                               | montant (€/$) | raisons                     |
| ------------------------ | ------------------------------------------------------------------------------------------ | ------------- | --------------------------- |
| Maplibre                 | https://opencollective.com/maplibre/donate                                                 | 500           | **jeune projet à soutenir** |
| debian                   | https://www.debian.org/donations                                                           | 500           | **pierre angulaire**        |
| Mozilla pour mdn/Firefox | https://donate.mozilla.org/fr/                                                             | 250           | **utilisation fréquente**   |
| OSM                      | https://donate.openstreetmap.org/                                                          | 250           | **bien commun important**   |
| Wikimedia                | https://donate.wikimedia.org/                                                              | 250           | **bien commun important**   |
| Gimp                     | https://www.patreon.com/zemarmot                                                           | 250           | **utilisation fréquente**   |
| React-icons              | https://github.com/sponsors/kamijin-fanta                                                  | 100           | **utilisation fréquente**   |
| React-router             | https://opencollective.com/react-router                                                    | 100           | **utilisation fréquente**   |
| keepassXC                | https://opencollective.com/keepassxc                                                       | 100           | **utilisation fréquente**   |
| mastodon                 | https://www.patreon.com/mastodon                                                           | 100           | **socialement important**   |
| Bootstrap                | https://opencollective.com/bootstrap                                                       | 100           | **utilisation fréquente**   |
| htop                     | https://opencollective.com/htop                                                            | 100           | **utilisation fréquente**   |
| rimraf, node-glob        | https://github.com/sponsors/isaacs                                                         | 100           | **utilisation fréquente**   |
| Signal                   | https://signal.org/donate/                                                                 | 100           | **socialement important**   |
| react-pdf                | https://opencollective.com/react-pdf-wojtekmaj                                             | 100           | **saved my day**             |
|                          | Total                                                                                      | 2900          |                             |
