---
title: "La sortie Gephi Lite v0.6"
excerpt: "Un sprint productif avec des gros morceaux dedans"
lang: fr
author: "OuestWare"
date: "2025-02-26T00:00:00.000Z"
tags: [gephi lite, floss]
ref: gephi-lite-0.6
ogImageURL: ./gephi-lite/gephi-logo.svg
---

Comme ça devient [une sorte de coutume](/2024/02/16/gephi-lite-0-4-fr), nous avons dédié une semaine de notre temps à Gephi Lite juste avant d’aller au [FOSDEM](https://fosdem.org/2025/). Et ce sprint a été particulièrement productif ! Côté ambition, on voulait faire de la maintenance, dégrossir le backlog et publier des expérimentations faites récemment. Voici les principales nouveautés :

- Des **filtres topologiques**, en plus des filtres déjà existants sur les attributs des nœuds et des liens.
- Un nouveau format de fichier en JSON pour sauvegarder un réseau avec son contexte dans Gephi Lite (filtres, apparence...).
- Une bibliothèque TypeScript pour **piloter** Gephi Lite depuis d'autres applications Web, basée sur l'API [_Broadcast Channel_](https://developer.mozilla.org/fr/docs/Web/API/Broadcast_Channel_API).
- Une nouvelle méthode pour observer l'**incertitude** dans la méthode de Louvain.

## Nouvelles fonctionnalités classiques

### Les filtres topologiques

Les filtres de Gephi Lite ont été conçus pour être plus simples et légers que le système d'arbre logique de Gephi. Malheureusement, jusqu'ici, certains cas d'usage très fréquents dans l'analyse visuelle des réseaux n'étaient simplement pas possibles dans Gephi Lite :

1. Filtrer uniquement la principale composante connexe
2. Cacher les feuilles, et plus généralement les chaînes se terminant par des feuilles
3. Uniquement filtrer le voisinage d'un nœud

Nous avions déjà prévu dans Gephi Lite les filtres topologiques. Ils sont maintenant implémentés !

1. Le filtre des [composantes connexes](<https://fr.wikipedia.org/wiki/Composante_connexe_(th%C3%A9orie_des_graphes)>) permet de ne garder que les N plus grandes composantes connexes.
2. Le filtre par [_k_-cœur](<https://fr.wikipedia.org/wiki/D%C3%A9g%C3%A9n%C3%A9rescence_(th%C3%A9orie_des_graphes)>) permet en gros de ne garder que les nœuds avec un degré supérieur à _k_. Par exemple, le 2-cœur d'un graphe est le graphe, justement sans les chaînes qui se terminent par des feuilles.
3. Enfin, le filtre des [réseaux égocentrés](https://fr.wikipedia.org/wiki/Analyse_des_r%C3%A9seaux_sociaux#R%C3%A9seaux_personnels_et_r%C3%A9seaux_complets) permet de ne garder que le sous-graphe à une distance N d'un nœud donné.

import VideoTopologicalFilters from "./gephi-lite-0-6/gephi-lite-topological-filters.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoTopologicalFilters} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

### L'attribut degré dynamique

Jusqu'ici les attributs des nœuds et liens étaient statiques au sens où ils sont stockés dans l'état du graphe et ne sont modifiés que ponctuellement à la main ou par une statistique. C'est le cas du degré des nœuds. Pourtant en analyse de réseau, le degré est une notion topologique : c'est le nombre de liens d'un nœud. Si le nombre de liens change le degré devrait changer aussi.

Concrètement dans Gephi Lite l'attribut degré était un attribut comme un autre que l'utilisateur⋅rice pouvait mettre à jour en executant la statistique idoine. Si la topologie du graph change suite par exemple à un filtre ou à la suppression d'un lien, le degré ne change pas. Sa mise à jour est toujours possible, mais elle doit être déclenchée à la main en executant la statistique Degré.

Nous avons pour remédier à ce problème ajouter le concept d'attributs dynamiques dans Gephi Lite. Les attributs dynamiques sont des attributs dont la valeur est définie par une fonction prenant comme paramètres l'identifiant du nœud ou lien et la topologie du graphe courant. Un nouveau mécanisme permet à Gephi Lite de mettre à jour ces attributs dynamiques automatiquement à chaque changement de topologie.

Pour le moment, nous n'avons implémenté que le degré comme attribut de nœud dynamique. Mais le système est en place pour facilement en ajouter d'autres si besoin.

import VideoDynamicDegree from "./gephi-lite-0-6/gephi-lite-dynamic-degree.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoDynamicDegree} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

Notez que seul le degré simple a été rendu dynamique. Le degré pondéré demande lui de préciser quel est l'attribut de lien à considérer comme pondération. Il n'est pas possible de le rendre dynamique sans devoir demander à l'utilisateur⋅rice un attribut de pondération. Cela complexifiait trop la fonctionnalité. Nous avons donc laissé le degré pondéré de côté comme attribut statique calculable depuis le panneau des statistiques.

### Les nouveautés dans le panneau "Apparence"

Nous avons ajouté quelques fonctionnalités dans le panneau "Apparence".

Il est maintenant possible d'ajouter une seconde dimension pour colorer les nœuds et/ou les liens. Cette nouvelle fonctionnalité, intitulée "Nuancer la couleur", permet de colorer les éléments comme avant, mais ensuite d'interpoler leurs couleurs vers une couleur fixe, en fonction d'un attribut numérique.

Par exemple, on représente souvent les communautés détectées avec la méthode de Louvain dans la couleur des nœuds. Avec cette nouvelle fonctionnalité, ça permet de représenter sommairement, en plus, un _score_ de nœuds.

![Un réseau dans Gephi Lite, avec la couleur des nœuds "nuancée" par un attribut](./gephi-lite-0-6/gephi-lite-shadings.png)

Dans le même registre, il est maintenant possible :

- De modifier **la profondeur** des liens, c'est-à-dire l'ordre dans lequel ils sont dessinés, qui détermine lesquels sont au-dessus et donc plus visibles
- De **couper les labels** de nœuds et de liens trop longs

### Le nouveau format JSON

La plus importante différence entre Gephi Lite et Gephi, est que Gephi Lite est essentiellement dynamique. C'est-à-dire que dans Gephi Lite, on configure des filtres et des variables graphiques, et à chaque instant, Gephi Lite déduit quels nœuds et liens afficher, et comment. C'est ce qui lui permet d'**expliquer** le graphe avec la légende. Mais jusqu'ici, ce contexte des filtres et des variables graphiques n'existait que dans Gephi Lite, et il n'était pas possible de le partager. Pour adresser ce besoin, on a défini **un nouveau format JSON**, qui contient toutes les données du graphe : la topologie du graphe, les données de rendu, les métadonnées des nœuds et des liens, mais aussi l'état de l'apparence du graphe ou encore les filtres.

Ce nouveau format permet essentiellement de sauver un graphe, et de le rouvrir tout en conservant les paramètres d'apparence du graphe, ainsi que ses filtres. Et comme on peut facilement créer un permalien vers Gephi Lite avec un graphe donné, ça permet par exemple d'intégrer une `iframe` dans cette page web, avec Gephi Lite qui explore le graphe de démo utilisé pour la capture d'écran précédente, pour illustrer les nœuds nuancés :

<div className="imageblock">
  <iframe
    width="100%"
    height="600px"
    allowFullScreen="true"
    style="border: 1px solid lightgray"
    src="https://gephi.org/gephi-lite/?file=https://gist.githubusercontent.com/jacomyal/08b9cdd4c629f64c299eaa8b922bc37b/raw/a85f7bb53a6f6bb17cf3f5c0991142708f120146/arctic-shaded.json"
  ></iframe>
  <div className="title">
    <a href="https://gephi.org/gephi-lite/?file=https://gist.githubusercontent.com/jacomyal/08b9cdd4c629f64c299eaa8b922bc37b/raw/a85f7bb53a6f6bb17cf3f5c0991142708f120146/arctic-shaded.json">
      Le graphe Arctic, dans Gephi Lite, avec des nœuds nuancés
    </a>
  </div>
</div>

## Intégrer Gephi Lite dans des applications web

Chez OuestWare, on intègre des interfaces basées sur [sigma.js](https://www.sigmajs.org/) dans beaucoup d'applications web. Et si certains cas d'usage sont très légers, certains de nos clients ont une culture de l'analyse visuelle des réseaux qui fait qu'ils voudraient parfois avoir directement Gephi dans leurs applications. Gephi Lite était déjà intégrable facilement comme une application web tierce à condition de lui fournir une URL vers un fichier de réseau. Mais il n'était pas possible d'imaginer des interactions plus complexes pour améliorer l'intégration. On a creusé la question pour un client et on a profité de ce sprint pour finaliser une première version d'un mécanisme de pilotage de Gephi Lite depuis d'autres pages web.

_Note :_
_Pour que ça puisse fonctionner, le Gephi Lite doit nécessairement être déployé sur le même domaine que l'application web._

Comment ça marche :

1. Pour piloter un Gephi Lite depuis une application web, il faut ouvrir Gephi Lite avec un paramètre `broadcast` dans un nouvel onglet
2. Lorsque Gephi Lite démarre avec un paramètre `broadcast` dans la requête de l'URL, alors il écoute les messages émis sur la [_Broadcast Channel_](https://developer.mozilla.org/fr/docs/Web/API/Broadcast_Channel_API) au nom correspondant.
3. L'application web peut alors émettre des messages sur le _Broadcast Channel_.
4. Gephi Lite reçoit, interprète ces messages pour y réagir en conséquences.
5. Gephi Lite peut à son tour émettre des événements pour prévenir son hôte de mises à jour ou autre (non implémenté pour le moment)

Pour simplifier la vie des développeurs d'un tel mécanisme (en premier lieu nous-même), nous avons développé une bibliothèque TypeScript [`@gephi/gephi-lite-broadcast`](https://www.npmjs.com/package/@gephi/gephi-lite-broadcast), qui rend transparent la couche communication (broadcast et messages), et offre à la place une API simple, avec des promesses.

La toute première implémentation de ce mécanisme a permis d'ajouter dans Gephi Lite un bouton "Cloner le graphe dans un nouvel onglet" (avec l'apparence et les filtres). Nous verrons dans la section suivante comment nous avons pu utiliser cette fonctionnalité.

import VideoNewTab from "./gephi-lite-0-6/gephi-lite-new-tab.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoNewTab} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

Nous sommes très fiers de cette nouvelle fonctionnalité qui ouvre de nombreuses possibilités pour l'usage de Gephi Lite depuis d'autres applications. La peinture est encore fraîche, mais nous ne manquerons pas de faire évoluer les choses rapidement, car nous allons être les premiers utilisateurs de ce mécanisme.

## Observer l'incertitude dans la méthode de Louvain

En décembre dernier, on a participé à l'atelier _Unblackboxing Community Detection_, organisé par [Mathieu Jacomy](https://vbn.aau.dk/en/persons/mathieuj), et avec [Guillaume Plique](https://medialab.sciencespo.fr/equipe/guillaume-plique/), [Benjamin Ooghe-Tabanou](https://medialab.sciencespo.fr/equipe/benjamin-ooghe-tabanou/), [Andrea Benedetti](https://dipartimentodesign.polimi.it/it/personale/andrea.benedetti) et [Tommaso Elli](https://dipartimentodesign.polimi.it/it/personale/tommaso.elli), et sponsorisé par le centre [MASSHINE](https://www.en.ssh.aau.dk/research/research-priorities/computationel-ssh-masshine) de l'[Université d'Aalborg](https://www.en.aau.dk/). Cet atelier faisait suite à [une précédente Gephi Week](https://gephi.wordpress.com/2022/10/16/gephi-week-2022-debriefing/), où une équipe avait commencé à travailler sur des stratégies pour pouvoir évaluer ou observer quels "choix" sont certains et lesquels sont ambigus, dans la méthode de Louvain pour la détection des communautés.

Et l'une des stratégies explorées est la suivante :

1. On exécute la méthode de Louvain N fois (par exemple 50 ou 100 fois).
2. On compte, pour chaque lien, le pourcentage des exécutions de Louvain où ses deux extrémités sont dans la même communauté. On appelle ça le **score de co-appartenance** du lien.
3. On calcule ensuite un **score d'ambiguïté** pour chaque lien, qui vaut `Score x (1 - Score) x 4`, avec `Score` son score de co-appartenance. Ce score est toujours entre 0 et 1, mais est maximal pour les liens les plus ambigus, c'est-à-dire ceux dont les extrémités sont dans la même communauté exactement 50% du temps.
4. Enfin, on peut agréger pour chaque nœud **la moyenne des scores d'ambiguïté** des liens qui lui sont attachés.

Il ne reste plus qu'à mettre en évidence les liens et nœuds les plus ambigus. Et grâce aux nouvelles fonctionnalités de Gephi Lite de ce sprint, voici comment on peut voir ça dans Gephi Lite, en un clic !

1. Gephi Lite offre une nouvelle "Statistique", tout en bas dans la liste, intitulée "Ambiguïté des liens dans la méthode de Louvain".
2. Cette statistique permet de calculer d'un coup tous les scores décrits précédemment. Il stocke également pour chaque lien la communauté de sa source d'après une exécution de Louvain en particulier (ça sera utile pour la visualisation !).
3. Tout en bas du formulaire de cette statistique, un nouveau bouton **"Observer visuellement l'ambiguïté"** permet d'ouvrir dans un nouvel onglet le graphe courant, mais avec des paramètres d'apparence très particuliers :

- Le fond d'écran est gris sombre.
- Les nœuds sont blancs, et leur taille est proportionnelle à la moyenne des scores d'ambiguïté de leurs liens.
- Les liens sont colorés par la communauté de leur source, mais cette couleur est nuancée par leurs scores d'ambiguïté : plus un lien est ambigu, plus il est blanc.
- Les liens les moins ambigus sont affichés devant.

import VideoUncertainty from "./gephi-lite-0-6/gephi-lite-uncertainty.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoUncertainty} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

On apprécie pas mal cette nouvelle casquette que Gephi Lite acquière doucement de "terrain d'expérimentation" de la communauté. Ce rendu sur la visualisation de l'ambiguïté nous plaît, nous a amené à re-prioriser le _backlog_, et au final est peu intrusive (voire franchement cachée). On a hâte de savoir quelles autres opportunités émergeront des prochaines rencontres de la communauté Gephi !
