---
title: "Dons Copie Publique 2024"
excerpt: "Nos contributions Copie publique pour l'année 2024"
lang: fr
author: "OuestWare"
date: "2024-09-05T00:00:00.000Z"
tags: [dons, copie publique, redistribution]
ref: dons-copie-publique-2024
ogImageURL: ./copie-publique/copie-publique.svg
---

![Logo copie publique](./copie-publique/copie-publique.svg)

Nous sommes membres de la communauté [Copie Publique](https://copiepublique.fr/), qui fédère les entreprises qui contribuent à une juste rémunération du numérique libre en s’engageant à donner régulièrement à des projets Open Source.

Pour notre part, nous avons choisi de verser annuellement 1% de notre chiffre d'affaires. Cette année le montant total de nos dons "Copie publique" s’élèvera donc à 3050€ HT.

Comme chaque année, nous choisissons en interne des projets qui nous semblent importants et/ou qui nous ont bien aidé cette année. Chacun propose des projets puis on discute jusqu'à s'entendre sur les projets et les montants ([plus d'infos sur le processus](https://www.ouestware.com/2023/06/27/dons-copie-publique-2023/)). Après avoir réalisé les versements, nous publions ici et sur le site de copiepublique.fr la liste de nos dons.

Vous aussi rejoignez-nous chez [Copie Publique](https://copiepublique.fr/) !

## Liste de nos dons 2023-2024

| projet           | lien sponsor                                                                             | montant (€) | raisons                 |
| ---------------- | ---------------------------------------------------------------------------------------- | ----------- | ----------------------- |
| Let's encrypt    | [https://www.abetterinternet.org/sponsor/](https://www.abetterinternet.org/sponsor/)     | 450         | saved my day            |
| Internet Archive | [https://archive.org/donate](https://archive.org/donate)                                 | 450         | bien commun important   |
| Vite             | [https://github.com/sponsors/vitejs](https://github.com/sponsors/vitejs)                 | 450         | utilisation fréquente   |
| Astro            | [https://opencollective.com/astrodotbuild](https://opencollective.com/astrodotbuild)     | 200         | utilisation fréquente   |
| Mozilla (Firefox)| [https://foundation.mozilla.org/fr/donate/](https://foundation.mozilla.org/fr/donate/)   | 200         | utilisation fréquente   |
| OpenStreetMap    | [https://donate.openstreetmap.org/](https://donate.openstreetmap.org/)                   | 200         | bien commun important   |
| MapLibre         | [https://opencollective.com/maplibre/donate](https://opencollective.com/maplibre/donate) | 200         | jeune projet à soutenir |
| Mastodon         | [https://joinmastodon.org/sponsors#donate](https://joinmastodon.org/sponsors#donate)     | 200         | socialement important   |
| Prettier         | [https://opencollective.com/prettier](https://opencollective.com/prettier)               | 100         | utilisation fréquente   |
| ESLint           | [https://opencollective.com/eslint](https://opencollective.com/eslint)                   | 100         | utilisation fréquente   |
| Docusaurous      | [https://opencollective.com/docusaurus](https://opencollective.com/docusaurus)           | 100         | utilisation fréquente   |
| Storybook        | [https://opencollective.com/storybook](https://opencollective.com/storybook)             | 100         | utilisation fréquente   |
| Open Web Docs    | [https://github.com/sponsors/openwebdocs](https://github.com/sponsors/openwebdocs)       | 100         | utilisation fréquente   |
| React-icons      | [https://github.com/sponsors/kamijin-fanta](https://github.com/sponsors/kamijin-fanta)   | 100         | utilisation fréquente   |
| Gimp             | [https://www.patreon.com/zemarmot](https://www.patreon.com/zemarmot)                     | 100         | utilisation fréquente   |
|                  | Total                                                                                    | 3050        |                         |
