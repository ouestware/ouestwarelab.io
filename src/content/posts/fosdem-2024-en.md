---
title: "FOSDEM 2024"
excerpt: "Join us at the Free and Open source Software Developers' European Meeting aka FOSDEM"
lang: en
author: "OuestWare"
date: "2024-01-17T00:00:00.000Z"
tags: [floss, conference, research]
ref: fosdem-2024
ogImageURL: ./fosdem-2024/FOSDEM_logo.svg
---

We proudly set « Free culture » as one of our motto on this very website’s home page. It’s not some conventional marketing phrase but a true pilar for our team.

It’s an ecosystem we belong to. We use and create Free and Open source Software daily. For us, being part of such an ecosystem means more than writing/using code.

We [explained recently (FR only)](/2023/06/27/dons-copie-publique-2023/) how we support FLOSS software maintainers through yearly donations. In this blog post we would like to describe an other very important aspect: speaking about free and open source software.

## The Free and Open source Software Developers' European Meeting

[![FOSDEM website](./fosdem-2024/FOSDEM_banner.jpeg "FOSDEM website")](https://fosdem.org/2024/)

The **Free and Open source Software Developers' European Meeting** (_FOSDEM_) is an annual conference organized at the Université Libre de Bruxelles from 2001. Many thematic sessions called « *devroom* » are organized in parallel to discuss specific technologies or subjects linked to FLOSS communities.

Some are about coding language, databases, retro-computing or even community management, legal issues, etc. The conference is free to attend without any registrations: there is a very nice sharing, liberty and diversity mood flowing all around ([take a glimpse at it: faces of FOSDEM from Diégo Antolinos-Basso](https://toujoursdeja.fr/projects/faces-of-fosdem/)).

<div class="imageblock">
<iframe width="100%" height="500px" src="https://toujoursdeja.fr/projects/faces-of-fosdem/2023.html"
></iframe>
<div class='title'><a href="https://toujoursdeja.fr/projects/faces-of-fosdem/2023.html">Faces of FOSDEM 2023</a></div></div>

We join FOSDEM every year for some years now. It’s even the place where the three of us (Paul, Alexis and Benoît) met all together for the first time, long time before we created OuestWare.

Benoît co-organized the Graph Devroom dedicated to graph based technologies such as the Neo4j database. Alexis and Paul gave their first FOSDEM talk in this very same devroom in 2013 and 2015 respectively ([Explore and visualize graphs with sigma.js](https://archive.fosdem.org/2013/schedule/event/sigmajs/) and [Manylines, a graph web publication platform with storytelling features](https://archive.fosdem.org/2015/schedule/event/graph_manylines/)).

![Alexis presents the new version of Sigma.js](./fosdem-2024/fosdem_2015_alexis_sigmajs.jpg "Alexis presents the new version of Sigma.js")

We met in this graph devroom to speak about common subjects although we were there for different reasons: Benoît was at that time a pre-sales consultant at [Neo4j](https://neo4j.com/) and was helping their « *dev evangelists* » team who organized the session; Alexis as the creator and developer of a network visualisation library [Sigma.js](https://www.sigmajs.org/); Paul as a research lab CTO looking for publication venues which could welcome R&D talks coming from Social Sciences but written by and for engineers.

Feels like a good summary of what happens in this conference: developers, maintainers, users meet to discuss recent works. Nothing really new for academic or professional worlds but such a place is quite unique for open source communities who spread all over the web.

Besides, this event has always been a very important momentum for teams who make the trip to Brussels. At least, it’s been in our case!

Now that you know more about our dear FOSDEM conference, let’s talk about this year edition!

## Open Research devroom

Since 2020, Paul has co-organized the Open research devroom. He stepped through the looking glass after having given several talks. The [Open Research devroom](https://fosdem.org/2024/schedule/track/open-research/) proposes talks about the development and/or use of open source software in a « research » context, seen as the broad meaning of work with knowledge: academic research, investigative journalism, activism, preservation, curations...

To better discover what it’s like, the best is to read
[Mathieu Jacomy’s post](https://reticular.hypotheses.org/1825) (Paul’s former colleague and Alexis' brother) who co-organizes the devroom. Or for a more concise version head up to [the devroom webpage](https://research-fosdem.github.io/).

![The public of the open research tools & technologies devroom](./fosdem-2024/FOSDEM_2020_open_research.jpg "The public of the open research tools & technologies devroom , FOSDEM 2020 - CC-BY-SA Mathieu Jacomy")

After having organized a physical edition in 2020, two online editions in 2021 and 2022 and an hybrid one last year, FOSDEM goes back to In Real Life only event in 2024. But we (Open Research devroom) wished to keep the great features of hybrid conferences: enhancing speakers diversity by removing travel barriers (money and legal issues) while trying to reduce the event carbon footprint as much as we can by offering a no travel option (if talks are to be given on-site only, they are broadcasted live on the web).

This devroom is a place to discuss software for research, difficulties to bridge academic research and software development, epistemology of software mediated research practices, experimentation feedbacks, initiatives to foster FLOSS communities development in academia... We are very grateful to FOSDEM’s staff
to give use such a marvelous space for those topics.

Organizing and joining the open research devroom means a lot to us at OuestWare as we support many research teams by providing them custom research software.

## Gephi Lite

We will have to great honour to open the open research devroom line-up by trying to answer this question: where does Gephi Lite comes from?

![Gephi Lite screen capture](./fosdem-2024/FOSDEM_gephi-lite.png "Gephi Lite screen capture")

We propose to tell the very academic origin of this open source software we first released in 2022. On the way we would like to depict how we couldn’t have done this without the broader Gephi community. Gephi Lite was built on many previous experiments mainly coming from academia. Our talk would like to tell this story and reflects on the academic/associative/business dynamics which underlined this adventure.

Please join us at the Open Research devroom to learn more about this [on Saturday, February the 4th, at 10h30 CET](https://fosdem.org/2024/schedule/event/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite/) on site or online.

## Open Source Railway Designer

![OSRD logo](./fosdem-2024/FOSDEM_logo_osrd.svg)

We’ve been working for a few years with the [Open Source Railway Designer (OSRD) (SNCF Réseaux)](/en/references/#osrd) team. This project aims at creating a set of open source software able to manage a railway infrastructure digital twin at scale. For instance one as large as the French railway network. We help at this very ambitious project by contributing to the web user interfaces (data edition and visualisation).

This project has a special place in our hearts. First because we have always loved to play with trains! Then because railway infrastructures is, to us, a crucial piece of the commons for our present and future effort to reduce our carbon footprint. Last, this project is open in many ways: not only [the code is open source](https://github.com/osrd-project/osrd) but the project is part of the greater initiative [Open Rail Association](https://openrailassociation.org/) which fosters sharing and using open source technologies among railway actors beyond national boundaries.

It’s therefore not a surprised to see the railway subject popping out from the FOSDEM schedule: [Railway and Open Transport devroom](https://fosdem.org/2024/schedule/track/railways-and-open-transport/). See, open source software brings to many different topics!

![Calculating a strip map for a Nantes-Marseille train travel plan](./fosdem-2024/FOSDEM_warping.png "Calculating a strip map for a Nantes-Marseille train travel plan")

Alexis will present in the Railway devroom his recent work on implementing « *strip map* »: a cartographic representation of a travel plan which wraps the geography to align it on a given axis. Such a strip map allow to show cartographic (taken from openstreetmap) and railway infrastructure context linked to a travel plan in a very condensed visualisation. To learn more, come watch his show on [Saturday, February the 4th at 12h25 CET](https://fosdem.org/2024/schedule/event/fosdem-2024-2594-bending-geographic-maps-for-enhanced-railway-space-time-diagrams/).

The OSRD project will be presented by Loïc Hamelin (project leader at SNCF Réseaux) in the very nice [Public Code and Digital Public Goods devroom](https://fosdem.org/2024/schedule/track/public-code-and-digital-public-goods/) at 18h30 CET : [Open Source Railway Designer (OSRD): why SNCF Réseau start an open source project ?](https://fosdem.org/2024/schedule/event/fosdem-2024-2052-open-source-railway-designer-osrd-why-sncf-rseau-start-an-open-source-project-/)

<br>
<br>
We can’t wait to join this 2024 FOSDEM edition who will for sure be very rich for us.
We hope to have convinced you that there is a lot to be learned there. We focused this post on our own news but please go browse [FOSDEM very rich schedule](https://fosdem.org/2024/schedule/).

You’ll be there?
Please come say hello!
