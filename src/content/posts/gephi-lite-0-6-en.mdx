---
title: "Gephi Lite v0.6 Release"
excerpt: "A productive sprint with major improvements"
lang: en
author: "OuestWare"
date: "2025-02-26T00:00:00.000Z"
tags: [gephi lite, floss]
ref: gephi-lite-0.6
ogImageURL: ./gephi-lite/gephi-logo.svg
---

As it has become [somewhat of a tradition](/2024/02/16/gephi-lite-0-4-fr), we dedicated a week of our time to Gephi Lite just before heading to [FOSDEM](https://fosdem.org/2025/). And this sprint was particularly productive! Our initial goal was to focus on maintenance, backlog reduction and publish results of recent experimentations. Here are the main outputs:

- **Topological filters**, in addition to the existing attribute-based filters for nodes and edges.
- A new JSON file format to save a network along with its context in Gephi Lite (filters, appearance, etc.).
- A TypeScript library to **control** Gephi Lite from other web applications, based on the [_Broadcast Channel_](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API) API.
- A new method to reveal Louvain clustering **uncertainty**.

## New Features

### Topological Filters

Gephi Lite's filters were designed to be simpler and lighter than Gephi's logical tree system. Unfortunately, some very common use cases in visual network analysis were not possible until now:

1. Filtering only the main connected component
2. Hiding leaf nodes and, more generally, chains ending in leaf nodes
3. Filtering only the neighborhood of a given node

We had already planned topological filters for Gephi Lite, now they are fully implemented!

1. The [connected components](<https://en.wikipedia.org/wiki/Connected_component_(graph_theory)>) filter keeps only the N largest connected components.
2. The [_k_-core](<https://en.wikipedia.org/wiki/Degeneracy_(graph_theory)>) filter retains nodes with a degree greater than _k_. For example, the 2-core of a graph is the graph minus the chains ending in leaves.
3. Finally, the **ego network** filter keeps only the subgraph within a distance of N from a given node.

import VideoTopologicalFilters from "./gephi-lite-0-6/gephi-lite-topological-filters.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoTopologicalFilters} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

### The dynamic degree attribute

So far node and edge attributes were static. Attributes' values are stored in the data graph and only updated when users edit it manually or through the statistics panel. However, some attributes such as node degree are in network statistics a topological concept. A node Degree is the number of its edges. If edges change nodes degrees should too.

Till version 0.6 node degree was a standard node attribute one could create/update through the degree statistic. If the network topology was to change, those degree values became somewhat outdated. Getting up-to-date degree values after filtering or edge deletion used to require to recompute degree statistic.

To solve this issue we added dynamic attributes to Gephi Lite. **Dynamic attributes** are attributes whose value is defined by a function taking the item identifier and the current network topology as parameters. A new reactive mechanism ensures those functions to be triggered when needed, i.e. when the current network topology changes.

For now we only implemented the node degree as a dynamic attribute. But the architecture is now ready to welcome more if needed.

import VideoDynamicDegree from "./gephi-lite-0-6/gephi-lite-dynamic-degree.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoDynamicDegree} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

Please note that only the unweighted degree has been ported as a dynamic attribute. Weighted degree computation requires to chose one edge attribute as weights. Asking this to the user would have brought too much overhead complexity. We decided to put the weighted degree aside keeping it as a standard attribute for now.

### New Features in the "Appearance" Panel

We added several features to the "Appearance" panel.

It is now possible to add a second dimension for coloring nodes and/or edges. This new feature, called "Color Shading", allows elements to be colored as before but then interpolated towards a fixed color based on a numerical attribute.

For example, community detection using the Louvain method often determines node color. With this new feature, it can also represent a _score_ associated with nodes.

![A network in Gephi Lite with nodes shaded based on an attribute](./gephi-lite-0-6/gephi-lite-shadings.png)

Additionally, it is now possible to:

- Modify **the depth** of edges
- **Truncate labels** of nodes and edges that are too long

### The New JSON Format

The key difference between Gephi Lite and Gephi is that Gephi Lite is primarily dynamic. In Gephi Lite, you configure filters and graphical variables, and it determines which nodes and edges to display and how to style them. This approach allows Gephi Lite to **explain** the graph with a legend. However, this contextual information (filters and graphical settings) was only available within Gephi Lite and couldn't be shared.

To address this, we defined **a new JSON format** that contains all graph data: topology, rendering data, node and edge metadata, and even graph appearance and filter settings.

This new format allows users to save a graph and reload it while preserving its appearance and filters. Since Gephi Lite can generate permalinks for specific graphs, it enables seamless integration, such as embedding an `iframe` in this page with Gephi Lite showcasing the demo graph used in the screenshot above:

<div className="imageblock">
  <iframe
    width="100%"
    height="600px"
    allowFullScreen="true"
    style="border: 1px solid lightgray"
    src="https://gephi.org/gephi-lite/?file=https://gist.githubusercontent.com/jacomyal/08b9cdd4c629f64c299eaa8b922bc37b/raw/a85f7bb53a6f6bb17cf3f5c0991142708f120146/arctic-shaded.json"
  ></iframe>
  <div className="title">
    <a href="https://gephi.org/gephi-lite/?file=https://gist.githubusercontent.com/jacomyal/08b9cdd4c629f64c299eaa8b922bc37b/raw/a85f7bb53a6f6bb17cf3f5c0991142708f120146/arctic-shaded.json">
      The Arctic graph in Gephi Lite with shaded nodes
    </a>
  </div>
</div>

## Integrating Gephi Lite into Web Applications

At OuestWare, we integrate [sigma.js](https://www.sigmajs.org/) into many web applications. While some use cases are lightweight, some clients have a strong culture of visual network analysis and would like to have Gephi directly within their applications. Gephi Lite was already embeddable as long as the hosting application could provide an URL to a graph file. But it was not possible to imagine more complex interaction than 'open file' to closer integrations. We worked on that precise issue in a recent project and we dedicated some time in this iteration to finalize the first version of a mechanism to control Gephi Lite from other web pages.

_Note:_
_For this to work, Gephi Lite must be deployed on the same domain as the web application._

Here’s how it works:

1. To control Gephi Lite from a webpage, you can open it in a new tab (with a `broadcast` parameter)
2. When Gephi Lite starts with such a `broadcast` parameter in the URL, it listens for messages on the corresponding [_Broadcast Channel_](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API)
3. The webpage can then emit messages on the _Broadcast Channel_.
4. Gephi Lite receives and react to the received messages.
5. Gephi Lite can also emit events to warn the webpage of updates or something else (not yet implemented)

To simplify the developers' life (including ourselves), we developed a TypeScript library [`@gephi/gephi-lite-broadcast`](https://www.npmjs.com/package/@gephi/gephi-lite-broadcast) that abstracts messaging into a simple API using promises.

We added the very first implementation of this mechanism in Gephi Lite to implement a "Clone the graph in a new tab" button (with appearance and filters). See next section for another use of that feature.

import VideoNewTab from "./gephi-lite-0-6/gephi-lite-new-tab.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoNewTab} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

We are very proud of that feature which opens new doors of the use of Gephi Lite in existing web applications. Beware fresh painting! But we will quickly extend and polish things as we will for sure be part of the early adopters crowd.

## Observing Uncertainty in the Louvain Method

Last December we joined the _Unblackboxing Community Detection_ workshop, organised by [Mathieu Jacomy](https://vbn.aau.dk/en/persons/mathieuj) featuring [Guillaume Plique](https://medialab.sciencespo.fr/equipe/guillaume-plique/), [Benjamin Ooghe-Tabanou](https://medialab.sciencespo.fr/equipe/benjamin-ooghe-tabanou/), [Andrea Benedetti](https://dipartimentodesign.polimi.it/it/personale/andrea.benedetti) and [Tommaso Elli](https://dipartimentodesign.polimi.it/it/personale/tommaso.elli), and funded by the [MASSHINE](https://www.en.ssh.aau.dk/research/research-priorities/computationel-ssh-masshine) center at [Aalborg University](https://www.en.aau.dk/). This effort originates from [the 2022 Gephi Week](https://gephi.wordpress.com/2022/10/16/gephi-week-2022-debriefing/), where a team started working on strategies to evaluate and visualize ambiguity in Louvain community detection results.

One approach involved:

1. Running Louvain N times (e.g., 50 or 100 times)
2. Computing an **edge co-membership score** for each edge (the percentage of times both endpoints were in the same community)
3. Calculating an **ambiguity score** for each edge
4. Aggregating the **average ambiguity score** per node

To calculate and visualise those scores we can now use Gephi Lite as follows:

1. In Gephi Lite "Statistics" panel, you'll find a new item called "Louvain edges ambiguity".
2. This statistic computes in one shot both edges and nodes metrics described above.
3. At the very bottom of this panel you'll find a button called "Observe ambiguity visually (will open a new tab)". Use it to open the current graph with a dedicated appearance state to reveal ambiguity :

- Graph background is dark.
- Nodes are white and their size is proportional to their average ambiguity score.
- Edges are colored by their source community, but this color is shaded by the edge ambiguity score: the more ambiguous, the whiter.
- Less ambiguous edges are displayed on top of ambiguous ones.

import VideoUncertainty from "./gephi-lite-0-6/gephi-lite-uncertainty.webm";

<video preload="metadata" controls="controls" width="100%">
  <source src={VideoUncertainty} type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"' />
</video>

We’re excited to see what other opportunities will emerge from the Gephi community’s future discussions!
