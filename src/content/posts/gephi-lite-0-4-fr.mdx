---
title: "La sortie Gephi Lite v0.4"
excerpt: "Une nouvelle version issue d’une itération `Talk Driven Development`"
lang: fr
author: "OuestWare"
date: "2024-02-16T00:00:00.000Z"
tags: [gephi lite, floss]
ref: gephi-lite-0.4
ogImageURL: ./gephi-lite/gephi-logo.svg
---

import { Image } from "astro:assets";

import PostImage from "../../components/PostImage.astro";
import gephi_lite_range_filter from "./gephi-lite/gephi_lite_range_filter.gif";

export const components = { img: PostImage };

Nous avons dédié une semaine de notre temps à Gephi Lite juste avant d’aller au [FOSDEM](https://www.ouestware.com/2024/01/17/fosdem-2024-en/).

Le résultat de cette itération est la version 0.4 dont nous détaillons les nouveautés dans ce post.

## Talk Driven Development

Soyons francs : notre roadmap a été <s>inspirée</s> déterminée par [la présentation que nous avons donnée dans la Open Research devroom à propos de la genèse du projet](https://fosdem.org/2024/schedule/event/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite/). Nous souhaitions dans cette présentation expliquer que ce que nous faisons aujourd’hui dans Gephi Lite a été rendu possible par une longue histoire, largement ancrée dans la recherche académique. Gephi et une longue liste d’outils web pour la visualisation et l’exploration de réseaux en ligne ont été conçus, développés, utilisés et soutenus par de nombreux acteurs académiques.

<video preload="metadata" controls="controls" width="100%">
  <source
    src="https://video.fosdem.org/2024/ub4132/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite.av1.webm#t=10"
    type='video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"'
  />
  <source
    src="https://video.fosdem.org/2024/ub4132/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite.mp4#t=10"
    type="video/mp4"
  />
</video>

Nous voulions également montrer notre beau Gephi Lite ! Aussi, nous avons décidé de créer un réseau documentant les prototypes, personnes et organisations ayant joué un rôle dans l’écosystème Gephi et ainsi rendu possible Gephi Lite. Ce réseau a été le support de notre démonstration des fonctionnalités de Gephi Lite pendant notre présentation.

Ce réseau a été à l’origine d’une partie importante des améliorations apportées par la version 0.4.

## Édition de données

En effet, ce réseau de l’écosystème Gephi en ligne a été créé directement dans Gephi Lite. Depuis la version 0.3 il est en effet possible de créer et éditer les données d’un réseau.

![Formulaire de création de liens](./gephi-lite/gephi_lite_edge_creation.png "Formulaire de création de liens")

### Modèle vs. Édition

Dans Gephi Lite, nous utilisons un modèle de données des attributs qui permet de spécifier comment les utiliser dans les différents paramétrages de visualisation.

![Formulaire du modèle des attributs dans le panneau Graphe.](./gephi-lite/gephi_lite_04_attributes_model.png "Formulaire du modèle des attributs dans le panneau Graphe.")

Ce modèle est très simple : un attribut peut être `qualitatif`, `quantitatif`, ou les deux. Un attribut est `qualitatif` si ses valeurs sont des informations textuelles et `quantitatif` si ses valeurs sont des nombres calculables. Cette configuration permet à l’interface de proposer des modules de filtrage et paramétrage visuel spécifiques. Ce modèle n’est pas utilisé pour contrôler le type des données, mais seulement une aide à l’expérience utilisateur.

Le premier problème rencontré lors de la création du réseau : un clash entre l’édition et le modèle. L’édition ou la création des attributs d’un nœud ou d’un lien ne mettait pas à jour le modèle.
Et tout au long de la création et l’édition manuelle des 61 nœuds et des 88 liens de notre réseau, nous nous sommes heurtés à quelques autres difficultés...

Premièrement, le formulaire de création/édition des attributs n’était pas du tout lié au modèle. Nous devions recréer les mêmes attributs à chaque création et bien entendu en créant des erreurs typographiques au passage. Dans la version 4.0, ce formulaire est maintenant lié au modèle proposant systématiquement un champ de saisi pour chaque attribut connu.

Les erreurs typographiques sur le nom des attributs généraient de nouvelles entrées dans le modèle qui ne pouvaient être supprimées ! Il est maintenant possible de supprimer un attribut du modèle. Cependant, cela doit être fait à la main, car il n’est pas possible de déterminer à coup sûr quand un attribut du modèle doit être supprimé. Pour aider l’utilisateur à éditer le modèle, nous avons ajouté des indications reflétant l’usage des attributs dans le réseau. Ces indications permettent de repérer rapidement les cas potentiellement problématiques.

![L'attribut `URL` n'a aucune valeur dns 47 nœuds](./gephi-lite/gephi_lite_attribute_usage_indications.png "Modèle des attributs montrant le nombre de valeurs manquantes")

Pour accélérer la création des nœuds et liens, le champ `id` est dorénavant optionnel. Une valeur unique est automatiquement attribué si ce champ est laissé vide.

Et pour finir, nous avons rendu les erreurs de validation du formulaire visibles ! Le formulaire était déjà validé, mais sans aucun retour visuel...

Pour résumer, Gephi Lite est maintenant bien équipé pour les tâches de création ou d’édition de réseaux. Notez cependant que pour les besoins d’édition de données en masse, il faudra passer par une métrique personnalisée et donc écrire un script en JavaScript. Ou alors télécharger votre GEXF et utiliser le superbe Laboratoire de Données de Gephi.

![Laboratoire de Données de Gephi](./gephi-lite/gephi_lite_gephi_data_laboratory.png "Laboratoire de Données de Gephi")

### Filtrage par plage

Une fois le réseau créé, nous souhaitions animer l’apparition des différents acteurs de l’écosystème dans le temps. Pour ce faire, nous avons ajouté un attribut `start_year` (année de départ) aux nœuds et utilisé un filtre par plage sur celui-ci. Nous avons alors rencontré [deux problèmes](https://github.com/gephi/gephi-lite/issues/123), et avons donc amélioré ce filtre. Voici le résultat :

<PostImage
  src={gephi_lite_range_filter}
  imageClass="full-width"
  alt="On the left side, a range filter allow to filter nodes on their start_year attribute by setting a range of values to keep. The animation shows the upper bound of the range slowly changing from 2007 till 2024. In parallel on the right side the nodes and their edges are appearing progressively following the filter state."
  title="The Gephi graph-online ecosystem from 2007 to 2024"
/>

### Images des nœuds

Pour illustrer la place centrale prise par les membres de cette communauté, nous avons décidé d’ajouter des images de fonds au nœud de type personne. Ceci est rendu possible par le programme `node image` de [sigma.js](https://www.sigmajs.org/) qui a été développé à l’occasion de notre travail pour [graph commons](https://www.ouestware.com/references/#graphcommons).

Pour intégrer cela dans Gephi Lite, nous avons ajouté un module dans le panneau Apparence qui permet de sélectionner un attribut de nœud contenant une URL pointant vers une image. ⚠ L’hébergeur de cette image doit impérativement permettre des requêtes [Cross-origins](https://fr.wikipedia.org/wiki/Cross-origin_resource_sharing) (ou autoriser le domaine où est hébergé Gephi Lite, par défaut gephi.org).

![Dans le panneau Apparence](./gephi-lite/gephi_lite_node_image.png "Sélection d'un attribut d'image permet d'afficher les photos des personnes présentes dans le réseau")

Il est important de noter que pour le moment, il n’est pas possible de spécifier en GEXF (ni dans un aucun autre format de sérialisation) qu’une image doit être utilisée pour les nœuds. Cela a pour conséquence que Gephi Lite ne peut configurer cette fonctionnalité à l’ouverture d’un réseau. Il faut aller la paramétrer à la main après chaque ouverture de graphe.

Ceci pourrait changer en ajoutant une fonctionnalité dans les [spécifications GEXF](https://gexf.net/) ou en dotant Gephi Lite de son [propre format de fichier interne](https://github.com/gephi/gephi-lite/issues/34) sauvegardant les métadonnées de l’espace de travail.

<br />
<br />
<br />
Voici le réseau que nous avons créé, n’hésitez pas à l’explorer :

<iframe
  src="https://gephi.org/gephi-lite/?file=https%3A%2F%2Fwww.ouestware.com%2Ftalks%2FGephiLite%40FOSDEM2024%2Fgephi-lite.gexf"
  width="100%"
  style="height: min(60vh, 600px)"
  alt="exploration interactive du réseau dans une iframe pointant vers Gephi Lite "
></iframe>
<div class="title" style="width:100%; text-align:center">
  Exploration interactive de la communauté `réseau sur le web` de Gephi utilisant Gephi Lite
  <br />
  <a
    href="https://gephi.org/gephi-lite/?file=https%3A%2F%2Fwww.ouestware.com%2Ftalks%2FGephiLite%40FOSDEM2024%2Fgephi-lite.gexf"
    target="blank"
    rel="noopener"
  >
    ouvrir dans un nouvel onglet
  </a>
</div>

<br />
<br />
<br />

## Community Driven Development

Nous avons aussi travaillé sur les retours apportés par la communauté.

Nous avons intégré les traductions créées dans [Weblate](https://hosted.weblate.org/projects/gephi/gephi-lite/):

- en Hongrois 🇭🇺 grâce à Lajos Kékesi [@ludvig55](https://github.com/ludvig55/)
- en Coréen 🇰🇷 grâce à Hoil KIM [@hiikiim](https://hosted.weblate.org/user/hiikiim/)

Parmi l’effort de débogage, nous avons clos deux tickets ouverts par nos utilisateurs : [#116](https://github.com/gephi/gephi-lite/issues/116) et [#115](https://github.com/gephi/gephi-lite/issues/115).

Si vous avez des idées d’améliorations pour Gephi Lite, n’hésitez pas à [créer un ticket sur github](https://github.com/gephi/gephi-lite/issues/new) !
Cela nous aiderait grandement à prioriser les prochains développements.

## Maintenance

Pour terminer, nous avons investi du temps pour mettre à jour les dépendances du projet et améliorer divers tests.

Nous avons abandonné l’outil de compilation [Create React App](https://create-react-app.dev/) au profit de [Vite](https://vitejs.dev/), et avons mis à jour sigma.js en version 3. [Sigma.js](https://www.sigmajs.org/) est le pilier qui a rendu possible la création d’une version de Gephi sur le web. Nous développons cette librairie en collaboration avec [Guillaume Plique](https://medialab.sciencespo.fr/equipe/guillaume-plique/) et l’utilisons pour bon nombre de nos clients. Nous travaillons depuis plusieurs mois sur la version 3. Cette version est pour le moment en version béta mais elle sera publiée en version stable sous peu. L’utiliser dans Gephi Lite était une étape importante pour valider cette nouvelle version. Au passage Alexis a amélioré le packaging pour préparer la publication d’une version stable. La mise au point de cette nouvelle version a été grandement accélérée par une commande d’Arthur Bigeard, qui utilise sigma.js pour le développement de son produit [G.V()](https://gdotv.com/). Nous reparlerons bientôt de cette version 3.

## À suivre...

Cet article n’est pas exhaustif. Vous pouvez lire [le change log](https://github.com/gephi/gephi-lite/blob/main/CHANGELOG.md#041) pour voir toutes les améliorations et changements opérés dans cette version.

Notre prochaine itération sur Gephi Lite devrait se dérouler avant l’été 2024 à l’occasion d’une nouvelle Gephi week !

Nous sommes très reconnaissant à la communauté Gephi pour tous ces travaux qui ont petit à petit créé un écosystème humain et technique rendant Gephi Lite possible. Nous espérons que notre présentation au FOSDEM aura montré à quel point nous sommes fiers de faire partie de cette formidable communauté !
