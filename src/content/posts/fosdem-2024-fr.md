---
title: "FOSDEM 2024"
excerpt: "Rejoignez nous à la conférence européenne des développeurs de logiciels libres et ouverts aka le FOSDEM"
lang: fr
author: "OuestWare"
date: "2024-01-17T00:00:00.000Z"
tags: [floss, conférence, recherche]
ref: fosdem-2024
ogImageURL: "./fosdem-2024/FOSDEM_logo.svg"
---

Nous érigeons la « culture du libre » fièrement sur la page d’accueil de ce site. Ce n’est pas un vague argument marketing, mais un pilier important de notre entreprise.

C’est un écosystème auquel nous appartenons. Parce que nous utilisons et produisons des logiciels libres et ouverts quotidiennement. Pour nous, faire partie de cet écosystème implique plus que la seule consommation/production de code.

Nous [racontions en juin dernier](/2023/06/27/dons-copie-publique-2023/) que nous participons au soutien des mainteneurs de logiciels libres qui nous ont aidés à travers des dons annuels. Dans cet article, nous souhaitons décrire un autre aspect important : raconter le logiciel libre.

## The Free and Open source Software Developers' European Meeting

[![FOSDEM website](./fosdem-2024/FOSDEM_banner.jpeg "FOSDEM website")](https://fosdem.org/2024/)

Le **Free and Open source Software Developers' European Meeting** (_FOSDEM_) est une conférence annuelle organisée à l’Université Libre de Bruxelles depuis 2001.
Cette conférence est composée de plusieurs sessions thématiques appelées « *devroom* » discutant chacune d’une technologie ou d’un sujet lié aux communautés du logiciel libre.

On y retrouve des sessions sur des langages de programmation, des bases de données, du rétrocomputing ou encore la gestion des communautés, les questions légales, etc. La conférence est librement accessible, organisée sur le campus Stolbosch de l’ULB : il y règne une très belle ambiance de liberté, diversité et d’échange ([un aperçu : faces of FOSDEM de Diégo Antolinos-Basso](https://toujoursdeja.fr/projects/faces-of-fosdem/)).

<div class="imageblock">
<iframe width="100%" height="500px" src="https://toujoursdeja.fr/projects/faces-of-fosdem/2023.html"
></iframe>
<div class='title'><a href="https://toujoursdeja.fr/projects/faces-of-fosdem/2023.html">Faces of FOSDEM 2023</a></div></div>

Nous y participons depuis de nombreuses années. C’est même là que nous (Paul, Alexis et Benoît) nous sommes rencontrés pour la première fois bien avant que OuestWare ne soit créée.

Benoît y a co-organisé la Graph Devroom dédiée aux technologies d’analyse de données utilisant des modèles de réseaux comme les base de données et notamment Neo4j. Alexis et Paul ont donné leur toute première présentation au FOSDEM dans cette même devroom en 2013 et 2015 respectivement ([Explore and visualize graphs with sigma.js](https://archive.fosdem.org/2013/schedule/event/sigmajs/) et [Manylines, a graph web publication platform with storytelling features](https://archive.fosdem.org/2015/schedule/event/graph_manylines/)).

![](./fosdem-2024/fosdem_2015_alexis_sigmajs.jpg "Alexis présente la nouvelle version de Sigma.js")

Nous nous retrouvions donc dans cette même session pour parler de sujets très liés, mais avec des rôles très différents. Benoît était à l’époque consultant avant vente chez [Neo4j](https://neo4j.com/) et aidait l’équipe de « *dev evangelists* » de son employeur. Alexis en tant que créateur et mainteneur d’une librairie de visualisation de réseau [Sigma.js](https://www.sigmajs.org/). Paul en tant que directeur technique d’un laboratoire de recherche cherchant des lieux de publication qui accueillent des travaux de Recherche et Développement venant de recherche en Sciences Humaines, mais racontés par et pour des ingénieurs.

C’est un bon résumé de ce qui se passe dans cette conférence. Les développeurs, mainteneurs, utilisateurs de logiciels libres se retrouvent pour discuter de leurs travaux récents. Rien de très original pour des milieux académiques ou professionnels, mais il s’agit ici de créer un lieu qui rassemble des acteurs qui le reste du temps sont très peu en contact.

Par ailleurs cet événement a toujours été un moment privilégié pour créer du lien pour les équipes s’y déplaçant. En tout cas, c’est comme ça que nous l’avons toujours vécu !

Voilà pour le contexte de cette conférence chère à notre cœur.

Passons maintenant à ce que nous allons y faire cette année !

## Open Research devroom

Depuis 2020, Paul co-organise une devroom. Il est passé de l’autre côté du miroir en quelque sorte après avoir donné plusieurs talks.
L’[Open Research devroom](https://fosdem.org/2024/schedule/track/open-research/) est une session dédiée au développement ou à l’usage des logiciels libres et ouverts dans un contexte de « recherche ».
Il faut entendre le mot « recherche » au sens très large de travaux sur la connaissance : recherche académique, journalisme d’investigation, activisme, documentaliste, muséographie...

Pour mieux comprendre de quoi il s’agit le mieux est de lire le [post écrit par notre cher ami Mathieu Jacomy](https://reticular.hypotheses.org/1825) (ancien collègue de Paul et frère d’Alexis) qui co-organise la devroom. Ou pour un texte plus bref voir le [site de la devroom](https://research-fosdem.github.io/).

![Le public de la open research tools & technologies devroom](./fosdem-2024/FOSDEM_2020_open_research.jpg "Le public de la open research tools & technologies devroom, FOSDEM 2020 - CC-BY-SA Mathieu Jacomy")

Après avoir fait une édition en physique uniquement en 2020, deux éditions en ligne en 2021 et 2022 et enfin une édition hybride l’année dernière, le FOSDEM revient à une formule en présentiel uniquement cette année. Mais nous (Open Research devroom) avons souhaité garder la possibilité de l’hybride, car cela permet d’améliorer la diversité des conférenciers⋅ères en permettant de participer sans avoir à voyager. Cette barrière est très forte pour des raisons pécuniaires et légales (visa). C’est aussi un facteur tendant à augmenter le bilan carbone de l’événement (si les présentations sont forcément données en présentielles, elles sont néanmoins retransmises en direct sur le web).

Cette devroom est un lieu où l’on discute de logiciels pour la recherche, des difficultés de mêler développement et recherche académique, d’analyse épistémologique sur le rôle des données et technologies pour les méthodes de recherche, compte rendu d’expérimentations utilisant des logiciels ou des matériels libres, des initiatives pour favoriser l’émergence de communautés autour du développement dans les labos de recherche... Nous sommes très reconnaissants au FOSDEM de donner un tel espace à ces sujets qui nous semblent importants.

OuestWare accompagne de nombreuses équipes de recherche dans leurs besoins en logiciels. Participer à l’organisation de cette session nous tient donc très à cœur.

## Gephi Lite

Nous aurons l’immense privilège d’ouvrir la journée de l’Open Research devroom en tentant de répondre à cette question : d’où vient le logiciel Gephi Lite que nous avons publié en 2022 ?

![Capture d'écran de Gephi Lite](./fosdem-2024/FOSDEM_gephi-lite.png "Capture d'écran de Gephi Lite")

Nous souhaitons revenir sur la genèse grandement académique de ce logiciel libre et tenter d’expliquer pourquoi c’est notre équipe qui mène ce projet au sein de la communauté Gephi. Notre souhait est à la fois de créditer l’ensemble des expériences passées qui ont précédé et rendu possible Gephi Lite, mais aussi de réfléchir aux roles des différents acteurs académiques et privés de cette aventure.

Vous en saurez plus en nous rejoignant à la Open Research devroom [le samedi 4 février à 10h30](https://fosdem.org/2024/schedule/event/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite/) sur place ou en ligne.

## Open Source Railway Designer

![logo du projet OSRD](./fosdem-2024/FOSDEM_logo_osrd.svg)

Nous collaborons depuis plusieurs années avec les équipes du projet [Open Source Railway Designer (OSRD) de la SNCF Réseaux](/references/#osrd).
Ce projet vise à créer une infrastructure logicielle open source capable de gérer une copie numérique d’une infrastructure ferroviaire d’ampleur. Par exemple celle du réseau ferré français.
Nous aidons à ce vaste projet sur les fonctionnalités d’éditeur en ligne et de visualisation.

Ce projet nous plaît beaucoup. D’abord, car cela nous permet de jouer avec des trains ! Ensuite parce que les réseaux ferrés nous semblent être un bien commun de toute première importance pour notre présent et encore plus pour un avenir moins émetteur de carbone. Enfin ce projet embrasse la culture du libre: non seulement [son code est publié sous licence libre](https://github.com/osrd-project/osrd) mais il fait partie d’une initiative plus large appelée [Open Rail Association](https://openrailassociation.org/) qui travaille au partage transnational des technologies numériques libres pour le ferroviaire.

Et naturellement, le sujet des technologies du ferroviaire est dorénavant discuté au FOSDEM dans [la Railway and Open Transport devroom](https://fosdem.org/2024/schedule/track/railways-and-open-transport/) depuis 2023. Vous voyez que les logiciels libres et ouverts mènent à discuter d’une très grande variété de sujets !

![Calcul d'une strip map pour un trajet entre Nantes et Marseille](./fosdem-2024/FOSDEM_warping.png "Calcul d'une strip map pour un trajet entre Nantes et Marseille")

Alexis présentera dans cette devroom son travail récent sur l’implémentation d’un « *strip map* » : représentation cartographique d’un trajet déformant la géographie pour centrer le trajet sur un axe. Ce strip map permet de représenter tous les détails du contexte cartographique (issus d’OpenStreetMap) et de l’infrastructure ferroviaire liée à un itinéraire de train.
Pour en savoir plus, venez l’écouter [samedi 4 février à 12h25](https://fosdem.org/2024/schedule/event/fosdem-2024-2594-bending-geographic-maps-for-enhanced-railway-space-time-diagrams/).

Le projet OSRD sera lui présenté par Loïc Hamelin dans l’excellente [Public Code and Digital Public Goods devroom](https://fosdem.org/2024/schedule/track/public-code-and-digital-public-goods/) à 18h30 : [Open Source Railway Designer (OSRD): why SNCF Réseau start an open source project ?](https://fosdem.org/2024/schedule/event/fosdem-2024-2052-open-source-railway-designer-osrd-why-sncf-rseau-start-an-open-source-project-/)
<br>
<br>
Nous sommes impatients de vivre cette édition 2024 du FOSDEM qui sera très riche pour nous.
Nous espérons vous avoir convaincu qu’il y a beaucoup de choses à y apprendre. Si nous nous sommes concentrés ici sur notre actualité nous vous invitons à parcourir le [programme très varié du FOSDEM](https://fosdem.org/2024/schedule/). Nul doute que vous y trouverez des sujets qui vous intéresseront.

Vous y serez ?
Faites nous signe, les occasions ne manqueront pas de s’y rencontrer.
