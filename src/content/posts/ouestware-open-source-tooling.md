---
title: "OuestWare open-source tooling"
excerpt: "Open source is one of OuestWare fundamental values. Our technical ecosystem had to be based on libre and open source tools. In this post, I present our choices regarding the daily tools our team needs."
tags: [open-source, software]
lang: en
author: "Benoît Simard"
date: "2019-11-14T00:00:00.000Z"
ref: les-outils-open-source-de-ouestware
---

Open source is one of OuestWare fundamental values.
Our technical ecosystem had to be based on libre and open source tools.
In this post, I present our choices regarding the daily tools our team needs.

Sorry this post is only available in french for now.
Switch to french website version to read it in french.
Otherwise feel free to ping us if you'd like an english version.
