---
title: "The release of Gephi Lite v0.4"
excerpt: "A new version from our Talk Driven Development iteration"
lang: en
author: "OuestWare"
date: "2024-02-16T00:00:00.000Z"
tags: [gephi lite, floss]
ref: gephi-lite-0.4
ogImageURL: ./gephi-lite/gephi-logo.svg
---

We spent one week on Gephi Lite just before going to [FOSDEM](https://www.ouestware.com/2024/01/17/fosdem-2024-en/).

The result of this iteration is the release of the version 0.4. Let’s take a tour around what’s new.

## Talk Driven Development

Let’s be honest: our roadmap has been <s>inspired</s> driven by [the talk we gave in the Open Research devroom about the genesis of the project](https://fosdem.org/2024/schedule/event/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite/). We wanted to depict how what we are doing now in Gephi Lite is coming from far back into academia where Gephi and lot of network visualisation/exploration web based tools were designed, developed, used and supported.

<video preload="metadata" controls="controls" width="100%" >
    <source src="https://video.fosdem.org/2024/ub4132/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite.av1.webm#t=10" type="video/webm; codecs=&quot;av01.0.08M.08.0.110.01.01.01.0&quot;">
    <source src="https://video.fosdem.org/2024/ub4132/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite.mp4#t=10" type="video/mp4">
</video>

But we also wanted to show how neat is our Gephi Lite! So we decided to create a network of those prototypes, people and organisation who took part in the Gephi ecosystem which made Gephi Lite possible and demo Gephi Lite with it.

And that’s where our first set of new features are coming from.

## Data edition

Indeed, we created our Gephi ecosystem network directly in Gephi Lite. The v0.3 released data edition features, so we decided to use it!

![Edge creation modal](./gephi-lite/gephi_lite_edge_creation.png "Edge creation modal")

### Data model vs. data edition

In Gephi Lite, we are using a very loose graph data model which allow to use node and edge attributes differently for visualisation.

![Atributes' Model form in the graph panel.](./gephi-lite/gephi_lite_04_attributes_model.png "Atributes' Model form in the graph panel.")

Attributes can be `qualitative`, `quantitative` or both. `Qualitative` means the values of these attributes are textual information where `quantitative` ones are numbers we can do maths on. Depending on this configuration filtering and appearance will behave differently. This model is not a typing system checking data quality it’s a User Interface aid.

The first problem we encountered is a clash between that model and the data edition feature. In Gephi Lite one can create, edit and delete data attributes of one node/edge either by selecting one or by creating one.

But doing so was not updating the attribute model.
And while editing 61 nodes and 88 edges by hand, we bumped on a few other issues.

First the attributes form in edit/creation modals was not built using the model attributes, which led to wasting time filling the same attribute names over and over, and of course mistyping more than once. Now the form follows the model.

Typos in attribute names were also creating new entries in the attribute model which could not be deleted before! We can’t safely know when an attribute model is deprecated. We therefore chose not to delete model entries automatically but rather let the user delete model entries by hand adding some indications about attributes usage in the graph.

![The `URL` attribute is missing in 47 nodes](./gephi-lite/gephi_lite_attribute_usage_indications.png "number of missing values indications in the attributes model form")

To speed up manual node/edge creation, we also made the `id` input optional, generating a unique id when left blank.

And finally we made validation errors visible! The form was already validated but there were no UI feedbacks about it...

To sum up, Gephi Lite is now fully equipped to support your manual graph creation and edition tasks. Note however that if you need mass edition you will have to use some custom metric scripts, and thus write some code. Or download your GEXF and use the great Gephi Desktop data table.

![Gephi Data Laboratory](./gephi-lite/gephi_lite_gephi_data_laboratory.png "Gephi Data Laboratory")

### Filter range

The next step was to animate the appearance of the different actors of our network in time. To do this, we added a `start_year` node attribute and use the range filter on it. We stumbled upon [two issues](https://github.com/gephi/gephi-lite/issues/123) while doing so, and thus invest some time to make the range filter UX a little better. Here is the result:

![On the left side, a range filter allow to filter nodes on their start_year attribute by setting a range of values to keep. The animation shows the upper bound of the range slowly changing from 2007 till 2024. In parallel on the right side the nodes and their edges are appearing progressively following the filter state.](./gephi-lite/gephi_lite_range_filter.gif "The Gephi graph-online ecosystem from 2007 to 2024")

### Node images

To better illustrate that people are central in that ecosystem we decided to use a feature of [sigma.js](https://www.sigmajs.org/): node images. Thanks to the work we’ve done in [graph commons](https://www.ouestware.com/en/references/#graphcommons) it’s now possible to render an image as a node background.

We added a graph appearance module which allow to select a node attribute to set images. This attribute must contain a URL to a picture hosted somewhere online. ⚠ The host of the image must allow any [Cross-origins](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) (or allow gephi.org domain).

![In the graph appearance panel](./gephi-lite/gephi_lite_node_image.png "Selecting a image URL attribute to display pictures of people in the network")

Note that for the moment we can’t assign a node image attribute in GEXF or other serialization format. This means that Gephi Lite does not know when opening a graph file if there is a node image attribute to be used. So, when opening our graph in gephi lite you will not see any node image until you manually set the node image appearance settings yourself.

This might change in the future, either by adding new feature in [GEXF specifications](https://gexf.net/) or when Gephi Lite will have its [own internal file format](https://github.com/gephi/gephi-lite/issues/34) to store workspace metadata, including that kind of extra information.

<br>
<br>
<br>
Finally, here is the network we created, feel free to explore it:

<iframe src="https://gephi.org/gephi-lite/?file=https%3A%2F%2Fwww.ouestware.com%2Ftalks%2FGephiLite%40FOSDEM2024%2Fgephi-lite.gexf" width="100%" style="height: min(60vh, 600px)" alt="Interactive exploration of the network using Gephi Lite in an Iframe">
</iframe>
<div class="title" style="width:100%; text-align:center">Interactive exploration of our Gephi on the web community network using Gephi Lite<br/><a href="https://gephi.org/gephi-lite/?file=https%3A%2F%2Fwww.ouestware.com%2Ftalks%2FGephiLite%40FOSDEM2024%2Fgephi-lite.gexf" target="blank" rel="noopener">open it in a new tab</a></div>
<br>
<br>
<br>

## Community Driven Development

We also worked on feedbacks from the community.

We integrated translations created in [weblate](https://hosted.weblate.org/projects/gephi/gephi-lite/):

- in Hungarian 🇭🇺 thanks to Lajos Kékesi [@ludvig55](https://github.com/ludvig55/)
- in Korean 🇰🇷 thanks to Hoil KIM [@hiikiim](https://hosted.weblate.org/user/hiikiim/)

And among the debug effort, we’ve closed two issues reported from users: [#116](https://github.com/gephi/gephi-lite/issues/116) and [#115](https://github.com/gephi/gephi-lite/issues/115).

If you think using Gephi Lite could be made better, please [report issues on GitHub](https://github.com/gephi/gephi-lite/issues/new)!
That would help us a lot to set the roadmap.

## Maintenance

Finally, we spent some time updating the stack and worked on Quality Assurance.

We’ve migrated from [Create React App](https://create-react-app.dev/) to [Vite](https://vitejs.dev/) and we upgraded sigma.js to version 3. You may know that [sigma.js](https://www.sigmajs.org/) library is one of the pillars that enabled building a Gephi on the web. We develop this library in collaboration with [Guillaume Plique](https://medialab.sciencespo.fr/equipe/guillaume-plique/) and use it for many customers. We are working on this new version 3. It’s currently in beta but we are about to release it. Using it in Gephi Lite was one important step to test this new version. On the way Alexis worked on packaging it to prepare the official release. The development of this new version was greatly accelerated by a contract signed with Arthur Bigeard who uses sigma.js in his product [G.V()](https://gdotv.com/). Stay tuned to learn more about this version 3.

## To be continued...

This post is not exhaustive. Please read [the change log](https://github.com/gephi/gephi-lite/blob/main/CHANGELOG.md#041) to see the full picture.

Our next iteration of work on Gephi lite might happen before summer as a new Gephi Week is cooking!

We are very grateful to the Gephi community who helped us a lot in paving the way to Gephi Lite. We hope our talk showed how proud we are to be part of this amazing community!
