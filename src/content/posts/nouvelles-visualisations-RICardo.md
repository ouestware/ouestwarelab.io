---
title: "De nouvelles visualisations pour le projet RICardo"
excerpt: "Pourquoi et comment visualiser les statistiques du commerce mondial des XIXe et XX siècles ?"
tags: [dataviz, conception, open-source]
lang: fr
author: "Paul Girard"
date: "2020-10-19T00:00:00.000Z"
slug: nouvelles-visualisations-RICardo
ref: nouvelles-visualisations-RICardo
ogImageURL: ./ricardo/ricardo_og.png
---

![le site RICardo](./ricardo/ricardo_home.png)

Le projet de recherche RICardo vise à renouveler la perception des dynamiques de mondialisation en construisant un jeu de données rassemblant les statistiques de commerce bilatéral de nombreuses entités politiques au long de XIXe et XXe siècles. En regroupant ces statistiques historiques, il devient possible d’analyser les effets régionaux de la mondialisation. Par ailleurs cette richesse est aussi un défi de complexité: les entités commerçantes sont de nature très diverse (pays, villes, groupe de pays, zones coloniales...) et les valeurs des échanges commerciaux diffèrent en fonction des points de vue que représentent les multiples sources. Des défis passionnants pour l’analyse visuelle de données !

Ce projet a une [longue histoire](https://ricardo.medialab.sciences-po.fr/#!/about) à laquelle nous avons contribué récemment à travers [une prestation visant à améliorer les visualisations du site d’exploration visuelle](/references/#ricardo).

Dans cet article, nous expliquons et documentons les choix qui ont été faits face aux enjeux de la conception de visualisation de données dans un cadre interprétatif complexe.

<div style="width:550px;margin:0 auto">
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">You can’t grab a data set and slide into a great <a href="https://twitter.com/hashtag/DataViz?src=hash&amp;ref_src=twsrc%5Etfw">#DataViz</a>. <br><br>Rather, you make one decision at a time with what you have in your toolbox. 🧰🔧⚙️ <a href="https://t.co/8qouUNUA0p">pic.twitter.com/8qouUNUA0p</a></p>&mdash; Alli Torban (@AlliTorban) <a href="https://twitter.com/AlliTorban/status/1321074456853385217?ref_src=twsrc%5Etfw">October 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
</div>

## Le site avant notre intervention

![Les quatres vues World](./ricardo/ricardo_old_vues.png)

Le site a été créé en 2016 pour accompagner la publication de la première version des données. [Une communication](https://dh2016.adho.org/abstracts/177) (voir [les slides](https://medialab.github.io/ricardo/)) à la conférence Digital Humanities 2016 explicite les choix qui ont été faits à l’époque pour proposer une exploration progressive du commerce international aux XIXe et début XXe siècles.

Il propose tout d’abord une vue agrégée du commerce au niveau mondial ([page World](https://ricardo.medialab.sciences-po.fr/#!/world)), puis une vue détaillée du commerce d’un pays ([page Country](https://ricardo.medialab.sciences-po.fr/#!/reporting/)) et, à un niveau encore plus fin, permet de comparer le commerce d’une paire de pays afin de mettre en perspective les divergences entre leur statistique ([page bilateral](https://ricardo.medialab.sciences-po.fr/#!/bilateral/)). Une nouvelle vue a été apportée à cet ensemble afin de documenter les sources des données utilisées et de visualiser le degré de complétude de la base ([page Metadata](https://ricardo.medialab.sciences-po.fr/#!/metadata/)).

Ce site a été développé en angularJS à l’aide de D3.js pour les visualisations et de Python Flask pour l’API qui sert les données stockées dans une base SQLite ([code source](https://github.com/medialab/ricardo)).

## Quelles évolutions apportées ?

Pour aider [Béatrice Dedinger](https://www.sciencespo.fr/histoire/en/researcher/Beatrice%20Dedinger/76163.html) (responsable scientifique du projet) à concevoir et prioriser les évolutions à mener, nous avons proposé une prestation en mode agile. À l’instar du mode au forfait, nous facturons le temps passé et non la réalisation d’une fonctionnalité qui doit dans ce cas être spécifiée en détail a priori. Le mode agile a en effet l’avantage de permettre de concevoir ensemble ce qui doit être fait en prenant en compte les contraintes scientifiques, techniques et design.
Notre objectif est simple : apporter aux utilisateurs le meilleur outil possible pour un budget fixe et une liste d’idées comme points de départ.
Nous avons commencé le projet par un atelier de co-conception durant lequel nous avons discuté une à une chaque idée d’évolution venant de Béatrice mais aussi celles que nous avions en tête.
Cette étape est très importante car échanger nos points de vue permet non seulement une meilleure conception mais aussi d’assurer un meilleur partage des enjeux et contraintes de chacun.
Cette acculturation est un socle très précieux pour la suite du projet quand il faudra faire face aux imprévus.

![Quelques notes prises pendant l'atelier de conception](./ricardo/ricardo_workshop_notes.jpg)

Une fois les idées d’évolutions précisées (conception et niveau de difficulté), Béatrice a pu nous indiquer un ordre de priorité.
La suite est _"simple"_, un rendez-vous hebdomadaire permet de rendre compte de ce qui a été fait en combien de temps afin de décider ensemble des nouvelles priorités (itérations inspirées des méthodes agiles).

Dans la suite de ce texte, nous détaillons, les principales évolutions que nous avons apportées à RICardo en précisant les enjeux en termes de visualisation et de design interactif. Nous n’entrerons pas dans le détail des évolutions techniques comme par exemple notre toute première tâche qui a consisté à mettre à jour les versions des dépendances techniques et à moderniser la méthode d’empaquetage du code client afin de partir sur des bases saines pour accueillir les évolutions.

## Ajout de permaliens

Un site d’exploration visuelle permet de générer de nombreuses visualisations en fonction des choix faits par l’utilisateur quant aux différents paramètres de filtrage. Chaque vue (World, Country, Bilateral et metadata) est une application de visualisation à part entière qui peut être appliquée sur une multitude d’objets de la base de données (les entités dans le cas de RICardo). Par ailleurs chaque page propose des moyens de filtrage afin de préciser chaque point de vue (filtrage temporel dans le cas de RICardo). Ainsi une vue n’est pas une page web mais bien une multitude de pages possibles. C’est le modèle des Single Page Web App. Pourtant l’utilisateur visite bien un site web, et ses actions (choix dans les vues) le guident dans diverses pages. Aussi il est préférable de refléter les actions de sélection/filtrage dans l’URL de la vue afin de signifier à l’utilisateur et à son navigateur qu’il a bien changé de page (même si c’est un changement de page _virtuel_ car le contenu HTML est géré par le JavaScript dans le navigateur).

Au-delà du respect des principes de la navigation web, refléter les actions dans l’URL permet de tout simplement bénéficier de ce qu’est une URL où chaque vue :

- devient une étape dans l’historique de navigation
- dispose d’une URL unique lui permettant d’être référencée

Cette dernière fonctionnalité est aussi appelée permalien et est particulièrement utile afin de partager des visualisations entre collègues ou depuis des publications. La version des données serait nécessaire à un véritable lien pérenne.

Ainsi il est dorénavant possible de référencer le commerce de la France entre 1821 et 1870 et notamment la comparaison des valeurs de commerce avec ses trois principaux partenaires de l’époque (Royaume-Uni, États-Unis et Belgique) à l’aide d’une URL :

[`https://ricardo.medialab.sciences-po.fr/#!/reporting/France?selectedMinDate=1821&selectedMaxDate=1870&heatmapOrder=average&heatmapField=totalcomparison=UnitedStatesofAmerica%7CUnitedKingdom%7CBelgium`](https://ricardo.medialab.sciences-po.fr/#!/reporting/France?selectedMinDate=1821&selectedMaxDate=1870&heatmapOrder=average&heatmapField=total&comparison=UnitedStatesofAmerica%7CUnitedKingdom%7CBelgium)

Certes la lisibilité de cette URL n’est pas optimale mais c’est le prix à payer pour l’exhaustivité !

## Les taux de change

Pour permettre d’analyser le commerce international à partir de nombreuses sources venant de pays du monde entier, il faut pouvoir comparer les valeurs rapportées.
Or chaque pays relève les flux de commerce dans sa propre monnaie. Une opération de conversion de toutes les valeurs de commerce en une monnaie commune est donc nécessaire. Pour la faciliter, Béatrice a recueilli les taux de change en pound sterling d’une centaine de devises sur la période c.1800-1938.

Cette importante base de taux de change n’était jusqu’ici pas explorable et nous avons décidé de la mettre en valeur en ajoutant une nouvelle vue qui a pour objectif de :

1. permettre de lister les devises connues ;
2. indiquer pour quelles années le taux est connu ;
3. montrer les effets inflationnistes/déflationnistes que peuvent avoir les taux sur les valeurs de commerce converties.

![La nouvelle vue Taux de change](./ricardo/ricardo_exchange_rates.png "La nouvelle vue Taux de change")

Nous avons donc opté pour une série de petites courbes (small-multiple) permettant de lister toutes les devises d’un coup. On répond ainsi au premier objectif surtout en ajoutant un champs permettant de filtrer le nom des devises.

Nous avons ensuite choisi de représenter chaque devise sous la forme d’une courbe indiquant la valeur de son taux de conversion par année afin de répondre aux objectifs deux et trois.

La courbe a l’avantage d’être automatiquement perçue comme une valeur qui change dans le temps. Puisque nous devons faire avec la contrainte de la hauteur fixe induite par l’empilement des devises, elle a l’inconvénient d’utiliser la hauteur en pixel pour représenter la valeur de chaque point (Les cartes de chaleur résolvent bien se problème voir plus bas).

Nous ne pouvons pas utiliser plus de pixels pour représenter des taux de très grande valeur comparés à d’autres. Il faut abandonner l’idée que les courbes soient comparables en valeur absolue. Concentrons-nous plutôt sur notre objectif de comparer les évolutions internes des taux plutôt que de les comparer entre eux.
&nbsp;<aside>Les gens bien élevés font commencer les barres en zéro, Arthur Charpentier, 24/11/2016, https://freakonometrics.hypotheses.org/49561 via [@coulmont](https://twitter.com/coulmont/status/1325502150378663937)</aside>
Pour représenter aux mieux les évolutions des taux nous avons choisi de casser une règle classique de visualisation de courbes : **l’axe des ordonnées ne commence pas à 0 !**

En période de stabilité monétaire, les taux stables vont osciller autour d’une valeur moyenne qui peut être très éloignée de zéro.

![Taux de change du Alexandrian piastre par rapport au pound sterling](./ricardo/ricardo_alexandrian_piastre_to_sterling_pound.png)

Aussi pour voir au mieux les évolutions, même mineures, mieux vaut adapter l’échelle localement, c’est-à-dire l’adapter aux plages de valeurs de chaque devise.

Il faut cependant veiller à bien indiquer que cet espace des possibles est unique pour chaque courbe, ce que nous faisons ici en indiquant systématiquement en graduation de l’axe des ordonnées le minimum et le maximum. L’axe des ordonnées étant placé à gauche de la courbe il se trouve juste en dessous du label de la devise. Ainsi la lecture d’un nouveau bloc se fait naturellement d’abord par le nom de la devise puis par les valeurs minimales et maximales du taux de change. On pose ainsi le contexte de lecture au plus tôt.

![Taux de change des Roupies indiennes par rapport au pound sterling](./ricardo/ricardo_indian_rupee_to_sterling_pound.png)

Et comme toujours ces visualisations permettent de découvrir des valeurs qui semblent incohérentes comme le taux de change de la roupie indienne en 1828 !

## GeoPolHist

<aside>Une publication scientifique détaillant la création de ce jeux de données est en cours de revue</aside>

Un nouveau jeu de données appelé GeoPolHist a été construit afin d’ajouter une dimension politique à la base RICardo. Il liste toutes les entités géopolitiques du monde et leurs liens de dépendance politique depuis 1816 jusqu’à nos jours. Un [site d’exploration visuelle](https://medialab.github.io/GeoPolHist/) spécifique a été créé pour GeoPolHist par Arnaud Pichon au médialab de Sciences Po.

Ces données pourraient permettre de contextualiser l’histoire commerciale des entités commerçantes de RICardo en donnant accès à leur trajectoire politique au long des XIXe et XXe siècles. Telle entité était-elle indépendante ou sous le contrôle d’une autre à cette date ? À l’inverse est-ce que cette entité en contrôlait d’autres à telle date ? Pourquoi le commerce du "Royaume des deux Siciles" s’arrête en 1860 ? Etc.

Ici notre mission était de trouver une manière de représenter ce contexte politique le plus brièvement possible pour inciter les visiteurs à visiter le site GeoPolHist qui explore en détail tous ces liens politiques historiques.

Nous avons donc réfléchi à une synthèse graphique de GeoPolHist pouvant trouver une place dans les pages du site RICardo déjà très chargées en informations visuelles et qui propose un lien vers le site dédié pour découvrir le détail.

L’idée a été d’ajouter une frise politique pour chaque entité commerciale présentant deux informations agrégées :

- sur quelle période cette entité est-elle souveraine ?
- comment a évolué le nombre de ses dépendances dans le temps ?

![France GeoPolHist essai sur deux lignes](./ricardo/ricardo_GeoPolHirst_2_stripes_France.jpeg)

 <aside>Illustration d’un avantage du télétravail : partager les essais avec ses collègues par copies d’écrans dans l’outil de discussion ce qui les archivent et permet de les récupérer quand on écrit un post de blog.</aside>
Ce premier prototype propose les deux informations sur deux lignes : les hachures représentent les périodes de non-souveraineté, la densité de rouge le nombre de dépendances; on réutilise le principe des pages qui prennent la largeur comme axe temporel.

Pour cette frise en carte de chaleur, nous avons créé des blocs par période et non par année. En effet ce qui importe le plus ici sont les moments de rupture. Ces blocs permettent en effet de représenter deux variables : l’évolution du nombre (variation de l’opacité du rouge) et surtout la fréquence des changements (répartition horizontale des barres blanches).

Cela répond aux objectifs en laissant les détails de la liste précise des dépendances ou des entités souveraines ainsi que les types de liens (colonie, occupation, annexion...) à l’[outil dédié](https://medialab.github.io/GeoPolHist/) dont le lien est ajouté dans le titre du bloc.

Problème: il est très courant qu’une entité ait une des deux barres totalement vide car toujours souveraine ou sans aucune dépendance. Or cet empilement prend de la place en hauteur, ce qui est critique dans une page déjà très longue. Aussi nous avons tenté de la passer sur une seule ligne.

![Belize GeoPolHist sur deux lignes](./ricardo/ricardo_GeoPolHirst_2_stripes_Belize.jpeg)

Grâce aux motifs en hachure on peut superposer les deux ! Sauf que le fond étant une carte de chaleur, son opacité variable rend des hachures noires difficiles à distinguer sur fond rouge à faible opacité, et des hachures blanches invisibles s’il n’y aucune dépendance. Le problème ainsi posé la solution paraît évidente : des doubles hachures noire/blanche !

![Bulgaria GeoPolHist sur une seul ligne](./ricardo/ricardo_GeoPolHirst_1_stripe_Bulgaria.jpeg)

Voici le code créant le motif à double hachure noire/blanche en SVG avec d3.js ([voir ce code dans son contexte](https://github.com/medialab/ricardo/blob/0740a5446d6ca2011c817da2b4c4d5825fe1941a/client/src/js/directives/politicalStatuses.directive.js#L105-L129)).

```javascript
// hatch pattern
const pattern = svg
  .append("defs")
  .append("pattern")
  .attr("id", "diagonalHatch")
  .attr("patternUnits", "userSpaceOnUse")
  .attr("patternTransform", "rotate(45 0 0)")
  .attr("width", 6)
  .attr("height", 6);
pattern
  .append("line")
  .attr("x1", 1)
  .attr("y1", 0)
  .attr("x2", 1)
  .attr("y2", 6)
  .attr("stroke", "#000")
  .attr("stroke-width", 1);
pattern
  .append("line")
  .attr("x1", 2)
  .attr("y1", 0)
  .attr("x2", 2)
  .attr("y2", 6)
  .attr("stroke", "#EEE")
  .attr("stroke-width", 1);
```

Cette frise politique a été ajoutée aux deux pages centrées sur des entités (Reporting et Partner) qui ont été créées en refondant la page Country.

## <del>Country</del> Reporting et Partner

La dernière page sur laquelle nous sommes intervenus est Country. Créée pour la première version du site, cette page permettait d’explorer le commerce de certaines entités de la base RICardo, celles dites ‘reporting’. Un reporting est une entité dont le commerce est décrit _exhaustivement_ par les statistiques qu’il publie. Ainsi, le commerce de toutes les autres entités commerçantes qui sont désignées dans les tableaux de commerce des reportings mais qui ne publient pas elles-mêmes de statistiques commerciales ne pouvait pas être exploré dans cette page Country. Une évolution majeure a été de franchir un nouveau pas dans l’exploitation du potentiel de la base RICardo en créant une nouvelle page, la page Partner, ce qui a par là-même conduit à rebaptiser la page Country en Reporting. La page Partner exploite toutes les données collectées par les reportings afin de reconstituer de façon indirecte le commerce bilatéral d’une entité partner. Cet ajout a été imaginé suite au travail de recherche mené sur la base exploitant ces vues indirectes [(Girard et Dedinger 2018)](https://spire.sciencespo.fr/notice/2441/6h7io1v56e8k4qtht2cuvjcfa5).

Nous avons en quelque sorte dupliqué la page Reporting pour l’adapter au cas particulier des partners. La page Partner propose une vision qui reste partielle du commerce de l’entité puisqu’elle est recréée à partir des données des seuls reportings qui l’enregistrent dans leurs tableaux statistiques. Nous n’avons accès qu’à quelques reflets de son commerce vus dans les miroirs des reportings. Le commerce bilatéral du partner ainsi reconstitué ne pouvant prétendre à l’exhaustivité, trois décisions ont été prises :

1. pas de courbes de commerce total car nous savons qu’elles seraient fausses
2. impossible de calculer un indicateur de part de commerce
3. ajouter un histogramme du nombre de Reportings citant le Partner dans le temps afin de mieux évaluer la complétude de la vision indirecte

Pour le point 1. aucune autre conséquence que de supprimer une visualisation. Travailler les points 2 et 3 sur la page Partner nous a permis d’améliorer aussi la page Reporting puisque nous y avons ajouté ce même histogramme du nombre de citations de partenaires (3.), et retravailler la liste des partenaires (2.) a abouti à améliorer également celle de la page Reporting.

### Des cartes de chaleur de commerce

Construire la page Partner nous a obligé à imaginer comment créer une liste des Reportings le citant à l’instar de la liste des partenaires dans la page Reporting avec la contrainte de non-exhaustivité nous interdisant de calculer une part de commerce.

![Page Reporting](./ricardo/ricardo_partners_of_France.jpeg)

Nous avons alors développé une visualisation reprenant le principe des small-multiples mais en utilisant des cartes de chaleur représentant le volume de commerce (et non les parts). L’enjeu ici est de trouver un moyen de permettre de repérer les évolutions internes des intensités de commerce de chaque reporting tout en permettant de distinguer l’importance relative des reportings pour le partner visualisé.

![Page partner: reportings citant la France](./ricardo/ricardo_reporters_of_France.jpeg)

Nous avons choisi d’utiliser une échelle locale pour faire varier l’intensité de la carte de chaleur. L’année pour laquelle le commerce entre le reporting R et le partner est maximum a une opacité maximum quel que soit le volume absolu. Ainsi chaque ligne décrit bien les évolutions du commerce de chaque couple reporting/partner.
Pour signifier les différences en valeur absolue des différents reportings, nous utilisons une échelle de couleur ordinale utilisant des terciles pour représenter la valeur moyenne du volume de commerce du reporting. Nous créons ainsi trois classes de reportings au volume de commerce avec le partner pouvant être qualifié de faible, moyen ou important.
La possibilité de changer l’ordre des lignes par volume moyen permet par ailleurs de commencer la liste par les reportings les plus importants (commercialement parlant) pour le partner exploré.

Cette nouvelle visualisation par carte de chaleur terminée, Béatrice trouvait la lecture des évolutions d’intensité de commerce très parlante (et nous aussi). Ainsi nous avons réutilisé ce composant pour refaire la liste des partenaires dans la page reporting. Au delà de l’intérêt interprétatif cela permet de gagner en cohérence surtout pour ces deux pages reporting et partner qui sont les deux faces d’un même miroir.

Pour ce faire, il faut abandonner l’objectif porté par la précédente version (page Country) de représenter l’évolution de la balance commerciale. Cette information est intéressante mais difficile à interpréter surtout sur un très grand nombre de partenaires commerciaux. Nous avons donc proposé d’utiliser la métrique déjà utilisée pour classer les partenaires : la part moyenne de commerce. Plus un partenaire a été important relativement aux autres, plus cette part moyenne augmente même si ce partenaire est enregistré que sur un nombre d’années réduit, car bien entendu nous ne comptons pas zéro en cas d’absence de valeur.

![Page reporting](./ricardo/ricardo_partners_of_France_new_version.jpeg)

Puisque dans ce cas nous pouvons utiliser un tout de référence (le commerce total) et donc calculer des parts, il est aussi possible d’utiliser une échelle d’intensité globale et non locale. Cela permet de comparer les opacités entre lignes et donc de n’utiliser qu’une couleur. La lecture est simplifiée. L’inconvénient est que l’évolution de la part de commerce des partenaires moins importants est moins lisible. Cela étant dit, elle reste bien perceptible car, les valeurs représentées étant des parts, le champ des possibles reste limité à 100 valeurs. L’utilisation d’une carte de chaleur est particulièrement adaptée ici.

Une application de visualisations interactives forme un tout dont la cohérence importe beaucoup sur la valeur apportée aux utilisateurs. Ajouter une page _"dérivée"_ a nécessairement un impact sur la page d’origine. Une grande difficulté est dans le maintien d’un équilibre entre cohérence (heatmap en small multiple dans reporting et partner) et spécificité (échelle locale et globale).

## Conclusion

En gérant cette prestation en mode agile, nous avons pu engager une collaboration fructueuse tant sur la partie conception que sur les parties techniques. Un enjeu particulièrement important pour des projets de recherche notamment en Humanités Numériques où la complexité des données et l’exigence du cadre interprétatif demandent des solutions spécifiques à chaque situation.

Nous espérons que ce partage d’expérience pourra aider d’autres équipes dans leur propre projet !
