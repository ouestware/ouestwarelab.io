---
title: "Les outils open-source de OuestWare"
excerpt: "L'open-source est une valeur fondamentale à OuestWare. Ainsi quand il fallut monter notre écosystème technique, nous nous sommes naturellement tournés vers les outils libres. Dans cet article je vous présenterai les outils que nous avons choisis et pourquoi."
tags: [open-source, software]
lang: fr
author: "Benoît Simard"
date: "2019-11-14T00:00:00.000Z"
ref: les-outils-open-source-de-ouestware
---

L'open-source est une valeur fondamentale à OuestWare.
Ainsi quand il fallut monter notre écosystème technique, nous nous sommes naturellement tournés vers les outils libres. Dans cet article je vous présenterai les outils que nous avons choisis et pourquoi.

Mais avant tout, de quoi avons-nous besoin ?

- Un système de groupware avec mail, agenda et gestion des contacts
- Outil de tchat / communication en temps réel
- Archivage / partage de fichiers
- Un gestionnaire de source couplé à un système d'intégration continue
- Un site web avec les statistiques de visite respectueux de la vie privée

## Gestion des courriels : ProtonMail

L'auto-hébergement de mes courriels personnels pendant 10 ans m'a fait voir les limites de cette solution : mettre à jour régulièrement les systèmes de détection des spams, faire attention que son serveur SMTP ne soit pas blacklisté, vérifier que les courriels envoyés n'arrivent pas en tant que spam, etc.

C'est faisable, mais pour faire les choses dans les règles de l'art pour assurer sécurité et fiabilité est un travail en soi que je préfère laisser à de spécialistes.

C'est pour cela que nous avons opté pour [ProtonMail](https://protonmail.com/ "Site officiel de ProtonMail").

![Logo Protonmail](./open-source-tooling/Protonmail_logo.svg)

### Les plus

- Tout est chiffré : les courriels (GPG), le stockage et la communication aux serveurs
- [Open-source](https://github.com/ProtonMail)
- Gestion facile des comptes
- possibilité d'utiliser un client lourd comme [Thunderbird](https://www.thunderbird.net/fr/) par l'intermédiaire du [bridge](https://protonmail.com/bridge/)

### Les moins

- Ne permet pas de créer des mailing-lists, ni de faire des transferts automatiques (pratique pour une adresse contact)
- Ne possède pas (encore) de solution d'agenda et de contact intégré
- Le système de notification sur l'application mobile qui n'est vraiment pas au point

## La communication temps réel : Mattermost

Vu que l'équipe est souvent en télétravail ou en déplacement, nous voulions un outil de communication temps réel.
Mon premier réflexe fut de proposer un chan IRC (simple, utile et performant), mais après avoir fait la liste de nos besoins, j'ai révisé mon choix :

- Voir l'historique de toutes les conversations que ce soit sur le mobile ou le laptop
- Faire des recherches
- Pouvoir partager du contenu multimédia, comme une image

Rien que pour l'historique sur plusieurs devices, IRC est tombé à l'eau ...

C'est comme cela que le choix de [Mattermost](https://mattermost.com/ "Site de Mattermost") est devenu évident !

![Logo Mattermost](https://ucarecdn.com/8cd90d9d-8902-4845-a15b-f4664e5fcfb3/-/format/auto/-/quality/lighter/-/max_icc_size/10/-/resize/1288x/)

### Les plus

- application android parfaitement fonctionnelle
- système de hook facilitant l'intégration de bots
- [Open-source (MIT)](https://github.com/mattermost/mattermost-server)

### Les moins

- impossible de se connecter à plusieurs serveurs sur l'application mobile
- un marketplace pas très développé

## Codes-sources et déploiement : GitLab

Le cœur de notre activité est la création de logiciels. Nous avons besoin d'un système de gestion des codes-sources intégrant la gestion des versions, des tickets, un système d'intégration continue (CI)...

Notre choix s'est porté sur la forge open source proposée par [GitLab](http://gitlab.com).

Si il était envisageable de déployer un serveur gitlab en interne, notre ambition étant de contribuer aux communautés open source nous souhaitions que notre forge soit en grande partie publique.
Nous avons ainsi préféré soutraiter l'hébergement et la maintenance de notre forge à gitlab directement pour ne pas risquer de casser notre outil principal de travail par manque de maintenance.

C'est aussi une manière indirecte de contribuer financièrement au développement en achetant le service proposé par l'équipe portant le projet.

![capture d'écran gitlab](./open-source-tooling/gitlab_screenshot.png)

### les plus

- une excellente forge avec un système de CI très puissant
- une interface web complète

### les moins

- une interface web complexe

## Site internet : Jekyll

Tout entreprise a besoin de se présenter sur le web !

Pour démarrer nous voulions quelque chose d'ultra simple, une page qui présente l'entreprise avec nos domaines d'expertises, et un système de bloggin.

Sans intéraction des visiteurs (pas de commentaires sur les blogs, et pas de formulaire de contact, un simple mailto ^^), point besoin d'un site dynamique, un site statique en HTML est largement suffisant, et c'est plus robuste que n'importe quelle autre solution :)

![Logo Jekyll](https://www.userx.co.za/assets/images/journal/jekyll-logo-820x418.png "Logo Jekyll")

### Les plus

- [Open-Source (MIT)](https://github.com/jekyll/jekyll)
- Simple à prendre en main et à mettre en place
- Peut être hébergé par [GitLab](https://gitlab.com/ "Site de GitLab")
- Son écosystème, il y a beaucoup de plugin

### Les moins

- La mise en place de la gestion multi-lingue
- Ne génère que des sites statiques :)
- Son langage de scripting (liquid) est limité

## Statistiques Web : Matomo

En tant que développeur web, je n'aime pas être tracké sur internet, c'est pourquoi j'utilise tout un tas de plugins dans mon navigateur qui bloque tous les systèmes de tracking (ou en tout cas la plupart).

Et en tant que développeur web, j'aime bien avoir les statistiques de visite de mes sites webs.

Oui c'est un paradox, enfin quoique ... on peut toujours analyser les logs d'accès !

[AWStat](https://awstats.sourceforge.io/ "Site d'AWStat") sait le faire, mais que dire de son interface ...

C'est la que [Matomo](https://matomo.org/ "Site de Matomo") (anciennement Piwik) fait son entrée.

![Interface de Matomo](https://themes.matomo.org/Blurange/images/0.1.1/dashboard.png "Interface de Matomo")

### Les plus

- Open-Source (GPLv3) : https://github.com/matomo-org/matomo
- Gère l'analyse des logs d'accès, mais aussi le tracking par image, par code JS, l'AB-testing, ...
- Fait la correspondance IP / lieu géographique
- Il existe même une application pour mobile

### Les moins

- Ne gère pas de stream sur les logs d'accès
- Manque d'intéractions sur les graphiques affichés, comme la sélection d'une plage de date directement sur le graphique des visites, et que les autres graphiques se mettent à jour dynamiquement.
- Ne fonctionne pas avec ne base de données time-series comme [InfluxDb](https://www.influxdata.com/) ou [Promotheus](https://prometheus.io/)
