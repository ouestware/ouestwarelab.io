import { FaHandshakeAngle } from "react-icons/fa6";

import type { Event } from "./types.ts";

const events: Event[] = [
  {
    id: "fosdem-2025",
    dataType: "event",
    date: new Date("2025-02-01"),
    url: "https://fosdem.org/2025/",
    label: {
      fr: "FOSDEM 2025",
      en: "FOSDEM 2025",
    },
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    description: {
      fr: (
        <>
          Notre sortie belge annuelle, on rencontre nos ami·e·s d'OSRD et du médialab de Sciences-Po. Paul aide à
          organiser la salle Open Research, et on présente une conférence. On rencontre aussi pour la première fois
          Arthur de <a href="https://gdotv.com/">G.V()</a>, qui a financé la v3 de sigma.js.
        </>
      ),
      en: (
        <>
          Our yearly belgian tour, and a new occasion to meet people from OSRD and Sciences-Po médialab. Paul helps
          managing the Open Research room, and we have one presentation. We also meet for the first time Arthur from{" "}
          <a href="https://gdotv.com/">G.V()</a>, who sponsored sigma.js's v3.
        </>
      ),
    },
  },
  {
    id: "fosdem-2024",
    dataType: "event",
    date: new Date("2024-02-03"),
    url: "https://fosdem.org/2024/",
    label: {
      fr: "FOSDEM 2024",
      en: "FOSDEM 2024",
    },
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    description: {
      fr: "Notre sortie belge annuelle, on rencontre nos ami·e·s d'OSRD et du médialab de Sciences-Po. Paul aide à organiser la salle Open Research, et on présente deux conférences.",
      en: "Our yearly belgian tour, and a new occasion to meet people from OSRD and Sciences-Po médialab. Paul helps managing the Open Research room, and we have two presentations.",
    },
  },
  {
    id: "fosdem-2023",
    dataType: "event",
    date: new Date("2023-02-04"),
    url: "https://fosdem.org/2023/",
    label: {
      fr: "FOSDEM 2023",
      en: "FOSDEM 2023",
    },
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    description: {
      fr: "Notre sortie belge annuelle, on rencontre nos ami·e·s d'OSRD et du médialab de Sciences-Po. Paul aide à organiser la salle Open Research, mais on ne présente rien cette année.",
      en: "Our yearly belgian tour, and a new occasion to meet people from OSRD and Sciences-Po médialab. Paul helps managing the Open Research room, but we don't present anything that year.",
    },
  },
  {
    id: "fosdem-2022",
    dataType: "event",
    date: new Date("2022-02-05"),
    url: "https://fosdem.org/2022/",
    label: {
      fr: "FOSDEM 2022",
      en: "FOSDEM 2022",
    },
    location: {
      fr: <i>en ligne</i>,
      en: <i>online</i>,
    },
    description: {
      fr: "Une édition du FOSDEM en ligne à cause de l'épidémie de COVID19. Paul co-organise et anime la salle Open Research Tools and Technologies.",
      en: "An online edition of FOSDEM due to the COVID19 epidemic. Paul co-organizes and hosts the Open Research Tools and Technologies room.",
    },
  },
  {
    id: "fosdem-2021",
    dataType: "event",
    date: new Date("2021-02-06"),
    url: "https://fosdem.org/2021/",
    label: {
      fr: "FOSDEM 2021",
      en: "FOSDEM 2021",
    },
    location: {
      fr: <i>en ligne</i>,
      en: <i>online</i>,
    },
    description: {
      fr: "Une édition du FOSDEM en ligne à cause de l'épidémie de COVID19. Paul co-organise et anime la salle Open Research Tools and Technologies.",
      en: "An online edition of FOSDEM due to the COVID19 epidemic. Paul co-organizes and hosts the Open Research Tools and Technologies room.",
    },
  },
  {
    id: "fosdem-2020",
    dataType: "event",
    date: new Date("2020-02-06"),
    url: "https://fosdem.org/2021/",
    label: {
      fr: "FOSDEM 2020",
      en: "FOSDEM 2020",
    },
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    description: {
      fr: "Notre sortie belge annuelle, on rencontre nos ami·e·s du médialab de Sciences-Po. Paul co-organise la salle Open Research Tools and Technologies.",
      en: "Our yearly belgian tour, and a new occasion to meet people from Sciences-Po médialab. Paul co-organizes the Open Research Tools and Technologies room.",
    },
  },
  {
    id: "gephi-week-2022",
    dataType: "event",
    reference: "gephi-lite",
    date: new Date("2022-08-29"),
    url: "https://gephi.wordpress.com/2022/10/16/gephi-week-2022-debriefing/",
    label: {
      fr: "Gephi Week 2022",
      en: "Gephi Week 2022",
    },
    location: {
      fr: "Paris, France",
      en: "Paris, France",
    },
    description: {
      fr: (
        <>
          On se rencontre avec la communauté Gephi. Benoit aide à mettre à jour le connecteur Neo4J, Alexis aide à
          développer un plugin pour l'export Retina, et Paul aide à faire avancer les spécifications du format GEXF.
          Aussi, on commence à imaginer Gephi Lite.
        </>
      ),
      en: (
        <>
          We meet with the Gephi community. Benoit helps update the Neo4J connector, Alexis helps to develop a plugin
          for Retina export, and Paul helps to advance the specifications of the GEXF format. Also, we are starting to
          envision Gephi Lite.
        </>
      ),
    },
  },
  {
    id: "gephi-week-2024",
    dataType: "event",
    reference: "gephi-lite",
    date: new Date("2024-06-10"),
    url: "https://gephi.wordpress.com/2024/06/13/gephi-week-2024-peek-from-the-inside/",
    label: {
      fr: "Gephi Week 2024",
      en: "Gephi Week 2024",
    },
    location: {
      fr: "Copenhague, Danemark",
      en: "Copenhagen, Denmark",
    },
    description: {
      fr: (
        <>
          On se rencontre avec les développeurs de Gephi. On intègre la métrique{" "}
          <a href="https://observablehq.com/@jacomyma/a-validity-metric-for-interpreting-distances-in-a-network-m">
            <em>connected-closeness</em>
          </a>{" "}
          dans Gephi Lite. On planifie tous ensemble la mise à jour du site web de Gephi, et on debug Gephi Lite.
        </>
      ),
      en: (
        <>
          We meet with the Gephi developers. We integrate the{" "}
          <a href="https://observablehq.com/@jacomyma/a-validity-metric-for-interpreting-distances-in-a-network-m">
            connected-closeness
          </a>{" "}
          metric in Gephi Lite. We plan altogether the Gephi website update, and we debug Gephi Lite.
        </>
      ),
    },
  },
  {
    id: "gephi-workshop-2025",
    dataType: "event",
    reference: "gephi-lite",
    date: new Date("2024-12-16"),
    url: "https://vis.social/@jacomyma@mas.to/113668864864496476",
    label: {
      fr: "Gephi Workshop - Unblackboxing Community Detection",
      en: "Gephi Workshop - Unblackboxing Community Detection",
    },
    location: {
      fr: "Paris, France",
      en: "Paris, France",
    },
    description: {
      fr: (
        <>
          On se rencontre avec <a href="https://vbn.aau.dk/en/persons/mathieuj">Mathieu Jacomy</a>,{" "}
          <a href="https://medialab.sciencespo.fr/equipe/guillaume-plique/">Guillaume Plique</a>,{" "}
          <a href="https://medialab.sciencespo.fr/equipe/benjamin-ooghe-tabanou/">Benjamin Ooghe-Tabanou</a>,{" "}
          <a href="https://dipartimentodesign.polimi.it/it/personale/andrea.benedetti">Andrea Benedetti</a> et{" "}
          <a href="https://dipartimentodesign.polimi.it/it/personale/tommaso.elli">Tommaso Elli</a> pour travailler
          ensemble sur des méthodes d'évaluation de l'incertitude dans les algorithmes de détection de communautés dans
          les graphes.
        </>
      ),
      en: (
        <>
          We meet with <a href="https://vbn.aau.dk/en/persons/mathieuj">Mathieu Jacomy</a>,{" "}
          <a href="https://medialab.sciencespo.fr/equipe/guillaume-plique/">Guillaume Plique</a>,{" "}
          <a href="https://medialab.sciencespo.fr/equipe/benjamin-ooghe-tabanou/">Benjamin Ooghe-Tabanou</a>,{" "}
          <a href="https://dipartimentodesign.polimi.it/it/personale/andrea.benedetti">Andrea Benedetti</a> and{" "}
          <a href="https://dipartimentodesign.polimi.it/it/personale/tommaso.elli">Tommaso Elli</a> to work together on
          methods to evaluate uncertainty in networks community detection algorithms.
        </>
      ),
    },
  },
  {
    id: "sigma-sprint-2021",
    dataType: "event",
    date: new Date("2021-10-04"),
    url: "https://gephi.wordpress.com/2022/10/16/gephi-week-2022-debriefing/",
    label: {
      fr: "Sprint sigma.js v2.0",
      en: "Sigma.js v2.0 sprint",
    },
    location: {
      fr: "Nantes, France",
      en: "Nantes, France",
    },
    description: {
      fr: (
        <>
          Avec les gens du médialab de Sciences-Po, on se réunit quelques jours à Nantes pour finaliser la version 2.0
          de sigma.js. On en profite pour refaire complètement le site, et les exemples de code.
        </>
      ),
      en: (
        <>
          With the people from the Sciences-Po médialab, we are meeting for a few days in Nantes to finalize version 2.0
          of sigma.js. We are also taking the opportunity to completely redo the website and the code examples.
        </>
      ),
    },
  },
  {
    id: "osrd-event-2023",
    dataType: "event",
    reference: "osrd",
    date: new Date("2023-12-13"),
    url: "https://osrd.fr/en/",
    label: {
      fr: "Lancement du projet OSRD",
      en: "OSRD project launch",
    },
    location: {
      fr: "Paris, France",
      en: "Paris, France",
    },
    description: {
      fr: <>Un événement interne à la SNCF présente les différents outils métier d'OSRD aux premiers utilisateurs.</>,
      en: <>An internal SNCF event introduces the various OSRD business tools to the first users.</>,
    },
  },
  {
    id: "osrd-event-2024",
    dataType: "event",
    reference: "osrd",
    date: new Date("2024-12-11"),
    url: "https://osrd.fr/en/",
    label: {
      fr: "OSRD#2",
      en: "OSRD#2",
    },
    location: {
      fr: "Paris, France",
      en: "Paris, France",
    },
    description: {
      fr: <>Un événement organisé par l'équipe d'OSRD pour annoncer les premiers utilisateurs industriels du projet.</>,
      en: <>An event organized by the OSRD team to announce the first industrial users of the project.</>,
    },
  },
  {
    id: "ouestware-creation",
    dataType: "event",
    date: new Date("2019-10-04"),
    url: "https://www.ouestware.com/",
    icon: FaHandshakeAngle,
    label: {
      fr: "Création de OuestWare",
      en: "OuestWare creation",
    },
    location: {
      fr: "Nantes, France",
      en: "Nantes, France",
    },
  },
];

export default events;
