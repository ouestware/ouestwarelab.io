import type { Publication } from "./types.ts";

const releases: Publication[] = [
  // Gephi Lite:
  {
    id: "gephi-lite-0-4",
    dataType: "publication",
    reference: "gephi-lite",
    type: "release",
    label: {
      fr: "Gephi Lite v0.4",
      en: "Gephi Lite v0.4",
    },
    description: {
      fr: <>C'est surtout de la maintenance, mais aussi on peut afficher des images dans les noeuds.</>,
      en: <>It's mostly about maintenance, but it also allows displaying images in nodes.</>,
    },
    date: new Date("2024-02-14"),
    event: "fosdem-2024",
    url: "https://gephi.org/gephi-lite/",
  },
  {
    id: "gephi-lite-0-3",
    dataType: "publication",
    reference: "gephi-lite",
    type: "release",
    label: {
      fr: "Gephi Lite v0.3",
      en: "Gephi Lite v0.3",
    },
    description: {
      fr: (
        <>
          Cette version introduit principalement la recherche dans le graphe ainsi que la légende. Plus d'info dans{" "}
          <a href="https://twitter.com/ouestware/status/1679474842263859201">ce fil Twitter</a>.
        </>
      ),
      en: (
        <>
          This version mainly introduces graph search and the caption module. More details in{" "}
          <a href="https://twitter.com/ouestware/status/1679474842263859201">this Twitter thread</a>.
        </>
      ),
    },
    date: new Date("2023-07-13"),
    url: "https://gephi.org/gephi-lite/",
  },
  {
    id: "gephi-lite-0-2",
    dataType: "publication",
    reference: "gephi-lite",
    type: "release",
    label: {
      fr: "Gephi Lite v0.2",
      en: "Gephi Lite v0.2",
    },
    description: {
      fr: (
        <>
          Première sortie "publique" après un sprint dédié à rendre cette première version aussi utilisable que
          possible.
        </>
      ),
      en: <>First "public" release, after a sprint dedicated to make things work.</>,
    },
    date: new Date("2023-04-04"),
    url: "https://gephi.org/gephi-lite/",
  },
  {
    id: "gephi-lite-0-1",
    dataType: "publication",
    reference: "gephi-lite",
    type: "release",
    label: {
      fr: "Gephi Lite v0.1",
      en: "Gephi Lite v0.1",
    },
    description: {
      fr: <>Le premier prototype de Gephi Lite qui fonctionne. Sortie plutôt confidentielle.</>,
      en: <>The first working prototype of Gephi Lite. The release was rather confidential.</>,
    },
    date: new Date("2023-02-05"),
    url: "https://gephi.org/gephi-lite/",
  },

  // Sigma.js:
  {
    id: "sigma-3-0",
    dataType: "publication",
    reference: "sigmajs",
    type: "release",
    label: {
      fr: "Sigma.js v3.0",
      en: "Sigma.js v3.0",
    },
    description: {
      fr: <>Inclut une refonte complète du système de programme, et une réécriture de tout le tooling.</>,
      en: <>Includes a full rewrite of the programs system, and a reboot of the tooling.</>,
    },
    date: new Date("2024-03-21"),
    url: "https://sigmajs.org",
  },
  {
    id: "sigma-2-0",
    dataType: "publication",
    reference: "sigmajs",
    type: "release",
    authors: ["OuestWare", "Sciences-Po médialab"],
    label: {
      fr: "Sigma.js v2.0",
      en: "Sigma.js v2.0",
    },
    description: {
      fr: (
        <>
          La première version de sigma.js basée sur <a href="https://graphology.github.io/">Graphology</a>.
        </>
      ),
      en: (
        <>
          The first sigma.js version based on <a href="https://graphology.github.io/">Graphology</a>.
        </>
      ),
    },
    date: new Date("2022-06-23"),
    url: "https://sigmajs.org",
  },

  // React Sigma:
  {
    id: "react-sigma-1-0",
    dataType: "publication",
    type: "release",
    authors: ["Benoit Simard"],
    label: {
      fr: "React Sigma v1.0",
      en: "React Sigma v1.0",
    },
    description: {
      fr: <>La bibliothèque "officielle" pour utiliser sigma.js dans des applications React.</>,
      en: <>The "official" library to use sigma.js within React based applications.</>,
    },
    date: new Date("2021-11-01"),
    url: "https://sim51.github.io/react-sigma/",
  },

  // Retina:
  {
    id: "retina-1-0-beta-1",
    dataType: "publication",
    type: "release",
    label: {
      fr: "Retina",
      en: "Retina",
    },
    description: {
      fr: (
        <>
          Un outil open-source pour partager des visualisations de réseaux en ligne, financé par le{" "}
          <a href="https://cis.cnrs.fr/">Centre Internet et Société</a>.
        </>
      ),
      en: (
        <>
          An open-source tool to share network visualizations online, sponsored by the{" "}
          <a href="https://cis.cnrs.fr/en/">Centre Internet et Société</a>.
        </>
      ),
    },
    date: new Date("2021-11-01"),
    url: "https://gitlab.com/ouestware/retina",
  },
];

export default releases;
