import type { Customer } from "../types";
import logo from "./logo_ENSAD_PSL.png";

const customer: Customer = {
  id: "ensadlab",
  name: { fr: "EnsadLab", en: "EnsadLab" },
  url: "https://www.ensadlab.fr/",
  logo,
};
export default customer;
