import type { Customer } from "../types";
import logo from "./logo_medialab.svg";

const customer: Customer = {
  id: "medialab",
  name: {
    fr: "Sciences Po médialab",
    en: "Sciences Po médialab",
  },
  url: "http://medialab.sciencespo.fr",
  logo,
};

export default customer;
