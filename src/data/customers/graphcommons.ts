import type { Customer } from "../types";
import logo from "./logo_graphcommons.jpg";

const customer: Customer = {
  id: "graphcommons",
  name: { fr: "Graph commons", en: "Graph commons" },
  url: "https://graphcommons.com/",
  logo,
  showOnHome: true,
};
export default customer;
