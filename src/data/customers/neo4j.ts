import type { Customer } from "../types";
import logo from "./logo_neo4j.svg";

const customer: Customer = { id: "neo4j", name: { fr: "Neo4j", en: "Neo4j" }, url: "https://www.neo4j.com", logo };
export default customer;
