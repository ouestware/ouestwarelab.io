import type { Customer } from "../types";
import logo from "./logo_SNCF_Reseau.svg";

const customer: Customer = {
  id: "SNCFReseaux",
  name: { fr: "SNCF Réseau", en: "SNCF Réseau" },
  url: "https://www.sncf-reseau.com/",
  logo,
  showOnHome: true,
};
export default customer;
