import type { Customer } from "../types";
import logo from "./logo_UMRAE.png";

const customer: Customer = {
  id: "UMRAE",
  name: {
    fr: "Unité Mixte de Recherche en Acoustique Environnementale (UMRAE)",
    en: "Unité Mixte de Recherche en Acoustique Environnementale (UMRAE)",
  },
  url: "https://www.umrae.fr/",
  logo,
};
export default customer;
