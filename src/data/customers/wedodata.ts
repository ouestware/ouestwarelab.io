import type { Customer } from "../types";
import logo from "./logo_wedodata.svg";

const customer: Customer = {
  id: "wedodata",
  name: { fr: "Wedodata", en: "Wedodata" },
  url: "https://wedodata.fr/",
  logo,
  showOnHome: true,
};
export default customer;
