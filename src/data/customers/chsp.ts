import type { Customer } from "../types";
import logo from "./logo_CHSP.png";

const customer: Customer = {
  id: "CHSP",
  name: { fr: "Centre d'Histoire de Sciences Po", en: "Sciences Po Centre for History" },
  url: "https://www.sciencespo.fr/histoire/",
  logo,
};
export default customer;
