import type { Customer } from "../types";
import logo from "./logo_ETIS.svg";

const customer: Customer = {
  id: "ETIS",
  name: {
    fr: "ETIS, Équipe Traitement de l'Information et Systèmes (ETIS)",
    en: "ETIS, Équipe Traitement de l'Information et Systèmes",
  },
  url: "https://www.etis-lab.fr",
  logo,
};
export default customer;
