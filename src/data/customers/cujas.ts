import type { Customer } from "../types";
import logo from "./logo_cujas.jpg";

const customer: Customer = {
  id: "cujas",
  showOnHome: true,
  name: {
    fr: "Bibliothèque Cujas",
    en: "Cujas Library",
  },
  url: "http://biu-cujas.univ-paris1.fr/",
  logo,
};

export default customer;
