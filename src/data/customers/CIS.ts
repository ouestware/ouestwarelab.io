import type { Customer } from "../types";
import logo from "./logo_CNRS_CIS.jpg";

const customer: Customer = {
  id: "CIS",
  name: { fr: "Centre Internet et Société CIS-CNRS", en: "The Center for Internet and Society CIS-CNRS" },
  url: "https://cis.cnrs.fr",
  logo,
  showOnHome: true,
};
export default customer;
