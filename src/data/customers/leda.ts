import type { Customer } from "../types";
import logo from "./logo_LEDA_dauphine.svg";

const customer: Customer = {
  id: "dauphineLEDA",
  name: { fr: "LEDA, Paris Dauphine, PSL", en: "LEDA, Paris Dauphine, PSL" },
  url: "https://leda.dauphine.fr/",
  logo,
};
export default customer;
