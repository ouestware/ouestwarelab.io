import type { Customer } from "../types";
import logo from "./logo_gdotv.png";

const customer: Customer = {
  id: "gdotv",
  name: { fr: "G.V()", en: "G.V()" },
  url: "https://gdotv.com/",
  logo,
};
export default customer;
