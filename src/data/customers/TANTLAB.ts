import type { Customer } from "../types";
import logo from "./logo_tantlab.png";

const customer: Customer = {
  id: "TANTLAB",
  name: {
    fr: "The Techno-Anthropology Lab, Aalborg University",
    en: "The Techno-Anthropology Lab, Aalborg University",
  },
  url: "https://www.tantlab.aau.dk",
  logo,
};
export default customer;
