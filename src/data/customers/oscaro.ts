import type { Customer } from "../types";
import logo from "./logo_oscaro.jpg";

const customer: Customer = {
  id: "oscaro",
  name: { fr: "Oscaro.com", en: "Oscaro.com" },
  url: "https://www.oscaro.com/#",
  logo,
};
export default customer;
