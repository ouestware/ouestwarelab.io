import type { Customer } from "../types";
import logo from "./logo_sigma.svg";

const customer: Customer = {
  id: "sigmajs",
  name: { fr: "Sigma.js", en: "Sigma.js" },
  url: "https://sigmajs.org",
  logo,
};
export default customer;
