import type { Customer } from "../types";
import logo from "./logo_datactivist.png";

const customer: Customer = {
  id: "datactivist",
  name: {
    fr: "Datactivist",
    en: "Datactivist",
  },
  url: "https://datactivist.coop",
  logo,
};

export default customer;
