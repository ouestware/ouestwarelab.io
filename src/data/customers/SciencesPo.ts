import type { Customer } from "../types";
import logo from "./logo_sciencespo.svg";

const customer: Customer = {
  id: "SciencesPo",
  name: { fr: "Sciences Po", en: "Sciences Po" },
  url: "https://www.sciencespo.fr",
  logo,
  showOnHome: true,
};
export default customer;
