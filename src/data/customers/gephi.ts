import type { Customer } from "../types";
import logo from "./logo_gephi.svg";

const customer: Customer = {
  id: "gephi",
  name: { fr: "Gephi", en: "Gephi" },
  url: "https://gephi.org",
  logo,
};
export default customer;
