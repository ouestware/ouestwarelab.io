import type { Customer } from "../types";
import logo from "./logo_mel_horizontal.png";

const customer: Customer = {
  id: "mel",
  name: {
    fr: "Métropole Européenne de Lille",
    en: "Métropole Européenne de Lille",
  },
  url: "https://www.lillemetropole.fr/",
  logo,
};

export default customer;
