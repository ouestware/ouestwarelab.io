import BackgroundHoppe from "./home/bg_cujas.png";
import BackgroundFOSDEM from "./home/bg_fosdem.jpg";
import BackgroundGephi from "./home/bg_gephi-lite.jpg";
import BackgroundGraphCommons from "./home/bg_graphcommons.jpg";
import BackgroundOSRD from "./home/bg_osrd.jpg";
import BackgroundSigma from "./home/bg_sigma.jpg";
import LogoHoppe from "./home/logo_cujas_white.png";
import LogoFOSDEM from "./home/logo_fosdem_white.svg";
import LogoGephi from "./home/logo_gephi_white.svg";
import LogoGraphCommons from "./home/logo_graphcommons_white.png";
import LogoOSRD from "./home/logo_osrd_white.svg";
import LogoSigma from "./home/logo_sigma_white.svg";
import type { Achievement } from "./types.ts";

const achievements: Achievement[] = [
  {
    dataType: "achievement",
    id: "graphcommons",
    label: {
      fr: "On aide GraphCommons à développer leur application web",
      en: "We help GraphCommons developing their platform",
    },
    logo: LogoGraphCommons,
    image: BackgroundGraphCommons,
    url: {
      fr: "/references#graphcommons",
      en: "/references#graphcommons",
    },
  },
  {
    dataType: "achievement",
    id: "osrd",
    label: {
      fr: "On aide SNCF Réseau à développer OSRD",
      en: "We help SNCF Réseau coding OSRD",
    },
    logo: LogoOSRD,
    image: BackgroundOSRD,
    url: {
      fr: "/references#osrd",
      en: "/references#osrd",
    },
  },
  {
    dataType: "achievement",
    id: "hoppe",
    label: {
      fr: "On a conçu un outil d'exploration pour des chercheurs en histoire du droit",
      en: "We designed an exploration tool for law history scholars",
    },
    logo: LogoHoppe,
    image: BackgroundHoppe,
    url: {
      fr: "/references#hoppe",
      en: "/references#hoppe",
    },
  },
  {
    dataType: "achievement",
    id: "gephi-lite",
    label: {
      fr: "On porte et on développe Gephi Lite",
      en: "We carry out and develop Gephi Lite",
    },
    logo: LogoGephi,
    image: BackgroundGephi,
    url: {
      fr: "/references#gephi-lite",
      en: "/references#gephi-lite",
    },
  },
  {
    dataType: "achievement",
    id: "sigma",
    label: {
      fr: "On développe et on maintient sigma.js",
      en: "We develop and maintain sigma.js",
    },
    logo: LogoSigma,
    image: BackgroundSigma,
    url: {
      fr: "/references#sigmajs",
      en: "/references#sigmajs",
    },
  },
  {
    dataType: "achievement",
    id: "fosdem",
    label: {
      fr: "On aide à organiser la salle Open Research au FOSDEM",
      en: "We help organizing the Open Research room at FOSDEM",
    },
    logo: LogoFOSDEM,
    image: BackgroundFOSDEM,
    url: {
      fr: "/2024/01/17/fosdem-2024-en",
      en: "/2024/01/17/fosdem-2024-fr",
    },
  },
];

export default achievements;
