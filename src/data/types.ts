import { type ImageMetadata } from "astro";
import { type CollectionEntry } from "astro:content";
import { type ComponentType } from "react";

import { type TranslationObject } from "../i18n";

export type Customer = {
  skip?: boolean;

  id: string; // unique slug (might be used later for permalinks)
  name: TranslationObject;
  url: string;
  logo?: ImageMetadata;
  showOnHome?: boolean;
};

export type Tag = {
  id: string; // unique slug (might be used later for permalinks)
  label: TranslationObject;
};

export type Achievement = {
  dataType: "achievement";
  id: string; // unique slug (might be used later for permalinks)
  label: TranslationObject;
  logo?: ImageMetadata;
  image?: ImageMetadata;
  url: TranslationObject;
};

export type Event = {
  dataType: "event";
  id: string; // unique slug (might be used later for permalinks)
  reference?: string; // an optional attached event ID
  icon?: ComponentType; // an optional icon to override the dataType's one
  date: Date;
  location?: TranslationObject;
  label: TranslationObject;
  description?: TranslationObject;
  image?: ImageMetadata;
  url: string;
};

export type Publication = {
  dataType: "publication";
  id: string; // unique slug (might be used later for permalinks)
  type: "release" | "paper" | "conference" | "production";
  event?: string; // an optional attached event ID
  reference?: string; // an optional attached event ID
  authors?: string[];
  date: Date;
  location?: TranslationObject;
  label: TranslationObject;
  description?: TranslationObject;
  image?: ImageMetadata;
  url: string;
  media?: JSX.Element;
};

export type Post = {
  dataType: "post";
  lang: string;
  author: string;
  date: Date;
  title?: string;
  excerpt?: string;
  rawData: CollectionEntry<"posts">;
};

export type Reference = {
  skip?: boolean;

  id: string; // unique slug (used in permalinks)
  customerId?: string | string[];
  serviceId?: string;

  // Contents:
  title: TranslationObject;
  subtitle?: TranslationObject;
  links?: { label: TranslationObject; url: TranslationObject }[];
  abstract: TranslationObject; // length ~=250 chars
  tags: string[]; // array of Tag IDs
  captures?: {
    image: ImageMetadata;
    comment: TranslationObject; // length ~=120 chars
  }[];
};

export type Service = {
  skip?: boolean;

  id: string; // unique slug (used in permalinks)

  // If not specified, we'll select the first reference that matches the service id:
  showcasedReferenceId?: string;

  // Contents:
  image: ImageMetadata;
  title: TranslationObject;
  description: TranslationObject;
  catchPhrase: TranslationObject;
  yourNeeds: TranslationObject[]; // to be rendered as li
  ourSolutions: TranslationObject[]; // to be rendered as li
};

export type FullData = {
  tags: Tag[];
  customers: Promise<Customer[]>;
  services: Service[];
  references: Promise<Reference[]>;
};

export interface HeadMeta {
  name?: string;
  property?: string;
  content: string;
}
