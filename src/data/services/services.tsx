import { type Service } from "../types";
import ConsultingIllustration from "./illus-consulting.svg";
import DataValorisationIllustration from "./illus-data-valorisation.svg";
import FullProjectIllustration from "./illus-full-project.svg";
import OpenSourceIllustration from "./illus-open-source.svg";

const services: Service[] = [
  {
    id: "full-project",
    title: {
      fr: "Développement sur-mesure",
      en: "Custom development",
    },
    image: FullProjectIllustration,
    description: {
      fr: (
        <>
          <p>
            Nous créons une <strong className="highlight">application web sur-mesure</strong> répondant à vos besoins
            spécifiques depuis la modélisation des données, la conception et l'intégration des interfaces web, et
            jusqu'au déploiement sur l'infrastructure de votre choix.
          </p>
          <p>
            Le développement sur-mesure vous permet d'appliquer les technologies les mieux adaptées à vos
            problématiques, garantissant <strong className="highlight">un résultat parfaitement adapté</strong>.
            Exploiter vos données métier prend une tout autre ampleur quand base de données, visualisations et
            algorithmes sont construits spécifiquement pour répondre au besoin.
          </p>
        </>
      ),
      en: (
        <>
          <p>
            We create a <strong className="highlight">custom web application</strong> to meet your specific needs from
            data modeling, web interface design and integration till deployment on the chosen infrastructure.
          </p>
          <p>
            With custom development, your needs are met by using the best-fitted technologies to reach the{" "}
            <strong className="highlight">most efficient solution</strong>. Operating your data takes a whole new
            dimension when database, data visualizations and algorithms are built specifically to meet your
            requirements.{" "}
          </p>
        </>
      ),
    },
    catchPhrase: {
      fr: "Vos enjeux sont nos contraintes, notre expertise la solution pour y apporter une réponse précisément adaptée.",
      en: "Your business challenges are our constraints, our expertise is the key to bring a precisely tailored solution.",
    },
    yourNeeds: [
      {
        fr: "Vous souhaitez une application web délivrant un service de qualité.",
        en: "You need a web application providing quality service.",
      },
      {
        fr: (
          <>
            <strong className="highlight">Votre besoin est spécifique</strong>, les solutions génériques ne conviennent
            pas.
          </>
        ),
        en: (
          <>
            <strong className="highlight">Your requirements are specific</strong>, generic solutions doesn't suit.
          </>
        ),
      },
      {
        fr: "Votre environnement technique est complexe et nécessite une intégration soignée.",
        en: "Your technical environment is complex and requires a seamless integration.",
      },
    ],
    ourSolutions: [
      {
        fr: (
          <>
            Une <strong className="highlight">équipe full stack</strong> expérimentée.
          </>
        ),
        en: (
          <>
            A experienced <strong className="highlight">full stack team</strong>.
          </>
        ),
      },
      {
        fr: "Transparence et agilité vous donnant le contrôle.",
        en: "Agile methods giving you complete control.",
      },
      {
        fr: "Un code source de qualité qui sécurise votre investissement.",
        en: "Quality source code to secure your investment.",
      },
    ],
    showcasedReferenceId: "secret-neo4j-datascape",
  },
  {
    id: "data-valorization",
    title: {
      fr: "Valorisation de données",
      en: "Data valorization",
    },
    image: DataValorisationIllustration,
    description: {
      fr: (
        <>
          <p>
            Nous vous aidons à exploiter vos données par des moyens d'analyse, de structuration, d'édition,
            d'exploration visuelle ou de publication.
          </p>
          <p>
            Vos données sont le centre de notre attention, que ce soit pour identifier des possibilités de valorisation
            à travers un audit ou pour accompagner un projet déjà défini en apportant une brique d'analyse ou de
            visualisation. Nous proposons des ateliers{" "}
            <strong className="highlight">
              <strong className="highlight">data sprint</strong>
            </strong>
            , qui rassemblent dans un temps et un lieu donné les regards métiers, techniques et utilisateurs autour du
            jeu de données.{" "}
          </p>
        </>
      ),
      en: (
        <>
          <p>We help value your data through modeling, curation, analysis, visual exploration or publication means.</p>
          <p>
            Let's focus on your data either to identify possible ways to value it trough an audit or by joining your
            existing project by adding some analyses or visualization expertises. We propose a specific workshop format
            called <i>"data sprint"</i> to gather stake holders, engineers and designers around the data-sets in the
            same place at the same time.
          </p>
        </>
      ),
    },
    catchPhrase: {
      fr: "Traiter ensemble les différents enjeux permet de faire converger la diversité des savoir-faire, des objectifs et des contraintes de chaque expertise pour assurer une valorisation optimale.",
      en: "Mixing the different points of views allows to better align skills, objectives and constraints by designing the best way to value your data.",
    },
    yourNeeds: [
      {
        fr: "Vous cherchez un moyen de mieux valoriser vos données.",
        en: "You are looking for a better way to value your data.",
      },
      {
        fr: (
          <>
            Il vous manque une expertise avancée pour <strong className="highlight">analyser vos données</strong>.
          </>
        ),
        en: (
          <>
            Your team miss an advanced skill to <strong className="highlight">analyze your data</strong>.
          </>
        ),
      },
      {
        fr: "Vos données gagneraient à être équipées d'un instrument dédié.",
        en: "Your data would have a greater impact with a dedicated instrument",
      },
    ],
    ourSolutions: [
      {
        fr: "Une équipe mêlant les approches techniques, données et usages",
        en: "A team mixing technical, data and design skills",
      },
      {
        fr: (
          <>
            <strong className="highlight">Data sprint</strong> : un format d'atelier spécifique
          </>
        ),
        en: (
          <>
            <strong className="highlight">Data sprint</strong>: a specific workshop format
          </>
        ),
      },
      {
        fr: (
          <>
            <strong className="highlight">L'analyse visuelle</strong> de données facilitant la compréhension des enjeux.
          </>
        ),
        en: (
          <>
            A <strong className="highlight">visual analysis</strong> approach to foster data issues understanding.
          </>
        ),
      },
    ],
    showcasedReferenceId: "lasso",
  },
  {
    id: "opensource",
    title: {
      fr: "Code et Données ouvertes",
      en: "Open Source and Open Data",
    },
    image: OpenSourceIllustration,
    description: {
      fr: (
        <>
          <p>
            Nos environnements de travail sont construits à l'aide de nombreuses ressources libres et ouvertes
            inter-dépendantes. Il n'est pas rare d'avoir un problème ou de souhaiter améliorer le comportement d'une de
            ces briques.
          </p>
          <p>
            Pourtant chaque projet open source a son mode de fonctionnement, son équipe, ses priorités, sa temporalité.
            Dans un contexte de surmenage des mainteneurs, il n'est pas évident d'arriver à améliorer son environnement
            de travail en contribuant à des ressources existantes.
          </p>
        </>
      ),
      en: (
        <>
          <p>
            Our working environment are build on top of many open and inter-dependent ressources. One often meets an
            issue or seeks to improve an existing open source software which supports our daily workflows.
          </p>
          <p>
            Yet, each open source project has his own team, habits, priorities and planning. In a context of maintainers
            overwork, improving it's working environment by pushing improvements into existing open source resources can
            turn into complex situations.
          </p>
        </>
      ),
    },
    catchPhrase: {
      fr: "Notre grande expérience des modes de fonctionnement des projets libres et ouverts pourra vous aider à résoudre efficacement ces difficultés ou à publier vos propres projets.",
      en: "Being accustomed to open source projects way of working, we can help you resolve existing issues, improve existing dependencies or publish your own project.",
    },
    yourNeeds: [
      {
        fr: "Vous avez des difficultés avec une brique open-source.",
        en: "You meet issues with an open-source software.",
      },
      {
        fr: "Vous désirez fluidifier vos processus de développement ou de données.",
        en: "You need to streamline your code or data workflows.",
      },
      {
        fr: "Vous vous demandez comment publier correctement du code ou des données.",
        en: "You seek help to correctly open some code or data-sets.",
      },
    ],
    ourSolutions: [
      {
        fr: (
          <>
            Ajout de fonctionnalités à une <strong className="highlight">brique open source</strong>
          </>
        ),
        en: (
          <>
            Add new features to an <b className="highlight">open source software</b>
          </>
        ),
      },
      {
        fr: "Être vos médiateurs auprès de communautés open source.",
        en: "Act as mediators towards open source communities.",
      },
      {
        fr: "Améliorer votre boîte à outils.",
        en: "Develop better tooling.",
      },
    ],
    showcasedReferenceId: "hyphe",
  },
  {
    id: "consulting",
    title: {
      fr: "Conseils et accompagnement",
      en: "Consulting and support",
    },
    image: ConsultingIllustration,
    description: {
      fr: (
        <>
          <p>
            Apporter une aide ponctuelle peut débloquer des situations complexes, renforcer des spécifications en phase
            de conception, ajouter un regard complémentaire sur un projet en cours pour un coût maîtrisé.
          </p>
          <p>
            Que ce soit en réalisant un audit ou un proposant une formation nous saurons trouver ensemble la meilleure
            forme à donner à une intervention ponctuelle.
          </p>
        </>
      ),
      en: (
        <>
          <p>
            Bring one-time help can aid overcome complex situations, make better specifications or add complementary
            perspectives tp ongoing projects for a reasonable cost.
          </p>
          <p>We will find together the best fitted form for a one-time intervention such as reviews, training, etc.</p>
        </>
      ),
    },
    catchPhrase: {
      fr: "Nous apportons une expertise ciblée pour renforcer ou accompagner votre équipe.",
      en: "We support your team by bringing supplementary focused expertises.",
    },
    yourNeeds: [
      {
        fr: (
          <>
            Vous aimeriez un <strong className="highlight">regard extérieur</strong> pour apporter de nouvelles idées.
          </>
        ),
        en: (
          <>
            You need a <strong className="highlight">different perspective</strong> to bring some fresh ideas.
          </>
        ),
      },
      {
        fr: "Une expertise ciblée manque à votre projet.",
        en: "A focused expertise could boost your project.",
      },
    ],
    ourSolutions: [
      {
        fr: (
          <>
            <strong className="highlight">Audit</strong> de code et/ou de données
          </>
        ),
        en: (
          <>
            Code and/or data <strong className="highlight">review</strong>
          </>
        ),
      },
      {
        fr: "Formation",
        en: "Training",
      },
      {
        fr: "Étude technique",
        en: "Specifications",
      },
    ],
    showcasedReferenceId: "expo-test",
  },
];

export default services.filter((service) => !service.skip);
