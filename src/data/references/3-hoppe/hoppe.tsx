import type { Reference } from "../../types";
import hoppe_editor_genealogy from "./hoppe_editor_genealogy.png";
import hoppe_network_editor_author from "./hoppe_network_editor_author.png";
import hoppe_search_authors from "./hoppe_search_authors.png";

const REFERENCE_HOPPE: Reference = {
  id: "hoppe",
  customerId: "cujas",
  serviceId: "full-project",
  title: {
    fr: "HOPPE-Droit",
    en: "HOPPE-Droit",
  },
  subtitle: {
    fr: "Explorer une collection d'ouvrages pédagogiques en droit français du XIXe-XXe siècles",
    en: "Explore a 19th-20th centuries French law educational works collection",
  },
  links: [
    {
      label: { fr: "Le projet", en: "The project" },
      url: {
        fr: "https://bcujas-hoppe.univ-paris1.fr/",
        en: "https://bcujas-hoppe.univ-paris1.fr/",
      },
    },
    {
      label: { fr: "Le code source", en: "The sources" },
      url: { fr: "https://gitlab.com/ouestware/cujas-hoppe/", en: "https://gitlab.com/ouestware/cujas-hoppe/" },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          <i>HOPPE-Droit</i> vise à l’élaboration et la publication d'une collection de notices bibliographiques de
          productions pédagogiques en droit des XIXe et XXe siècles. Nous avons conçu et développé un{" "}
          <strong className="highlight">outil d’exploration</strong> qui permet d’étudier les évolutions du droit
          français à travers les manuels utilisés pour son enseignement depuis le XIXe siècle.
        </p>
        <p>
          Le corpus est édité par l'équipe CUJAS dans la base de données{" "}
          <a href="https://heuristnetwork.org/">Heurist</a>. Ces données sont exportées par l'API et indexées dans un{" "}
          <a href="https://www.elastic.co/fr/elasticsearch/">ElasticSearch</a> en prenant soin de conserver la
          complexité des dates aux différents niveau de précision (date annuelle, au jour près) et d'incertitude. Une
          application web permet d'explorer et visualiser le corpus sous différents angles: productions, auteurs,
          éditeurs, réseaux de co-publication, généalogies...
        </p>
      </>
    ),
    en: (
      <>
        <p>
          The <i>HOPPE-Droit</i> projects aims to create and publish a collection of French law educational works from
          the 19th-20th centuries. We designed and developed an <strong className="highlight">exploration tool</strong>{" "}
          which help studying the evolutions of the French law through education materials from the 19th century.
        </p>
        <p>
          The dataset is edited by the CUJAS team in a <a href="https://heuristnetwork.org/">Heurist</a> database. These
          data are exported through API to be indexed into{" "}
          <a href="https://www.elastic.co/elasticsearch">ElasticSearch</a>. We made sure to keep data complexity by for
          instance taking care of dates incertainty and levels of precision. A web application finally allows to explore
          the dataset under different angles: books, authors, editors, co-publication networks, genealogies...
        </p>
      </>
    ),
  },
  tags: ["digital-humanities", "dataviz", "conception", "elasticsearch", "react", "heurist", "sigma"],
  captures: [
    {
      image: hoppe_search_authors,
      comment: {
        fr: `Recherche par facettes des auteurs de la collection`,
        en: `Faceted search on the collection authors`,
      },
    },
    {
      image: hoppe_network_editor_author,
      comment: {
        fr: `Réseau auteurs-éditeurs liés par leurs productions pédagogiques communes`,
        en: `Network of authors-editors linked by educational works`,
      },
    },
    {
      image: hoppe_editor_genealogy,
      comment: {
        fr: `Généalogie d'un éditeur retraçant les association, acquisition...`,
        en: `Editor's genealogy page summing up association, acquisition...`,
      },
    },
  ],
};

export default REFERENCE_HOPPE;
