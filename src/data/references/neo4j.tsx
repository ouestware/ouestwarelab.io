import { type Reference } from "../types";

const REFERENCE_NEO4J: Reference = {
  id: "neo4j",
  serviceId: "consulting",
  customerId: "neo4j",
  title: {
    fr: "Sous-traitant Neo4j",
    en: "Contractor for Neo4j",
  },
  abstract: {
    fr: (
      <p>
        Nous intervenons pour le compte de Neo4j chez plusieurs de leurs clients pour les assister dans leurs projets de
        graphes. Cela va de la mission d'expertise sur Neo4j ou de chargement et/ou de visualisation de données, à la
        réalisation d'<i>innovation labs</i>, de prototypes, voire de projets web complets.
      </p>
    ),
    en: (
      <p>
        We work on behalf of Neo4j to assist their customers on their graphs projects. We do Neo4j consulting, from data
        modeling, loading and visualization, to prototypes and full web projects based on modern web technologies.
      </p>
    ),
  },
  tags: ["neo4j", "dataviz", "conception"],
};

export default REFERENCE_NEO4J;
