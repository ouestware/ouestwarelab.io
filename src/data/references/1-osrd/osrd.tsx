import { type Reference } from "../../types";
import osrd_capt_1 from "./osrd-capt-1.png";
import osrd_capt_2 from "./osrd-capt-2.png";
import osrd_capt_3 from "./osrd-capt-3.png";

const REFERENCE_OSRD: Reference = {
  id: "osrd",
  customerId: "SNCFReseaux",
  serviceId: "opensource",
  title: {
    fr: "OSRD : éditeur ferroviaire open source",
    en: "OSRD: Open Source Railway Designer",
  },
  links: [
    {
      label: { fr: "Le projet OSRD", en: "OSRD project" },
      url: {
        fr: "https://osrd.fr/fr/",
        en: "https://osrd.fr/en/",
      },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: {
        fr: "https://github.com/osrd-project/osrd",
        en: "https://github.com/osrd-project/osrd",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          Nous accompagnons les équipes du projet OSRD en participant au développement d'une application web Open Source
          rassemblant divers outils d'édition et de gestion d'une infrastructure ferroviaire. Nous intervenons plus
          particulièrement sur les interfaces web permettant l'édition de l'infrastructure dans un outil cartographique
          avancé. Nous intervenons également sur certaines briques de visualisation comme le graphique espace-temps.
        </p>
        <p>
          Si ce projet est centré pour le moment sur l'infrastructure française il a pour vocation à terme à être
          utilisable dans d'autres contextes. Il fait à ce titre partie de la{" "}
          <a href="https://openrailassociation.org/">OpenRail Association</a> qui coordonne les efforts transnationaux
          de convergence des outils open source ferroviaires.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We work for the OSRD team to help developing a web-based railway infrastructure edition and management tool.
          We more precisely work on web interfaces dedicated to the infrastructure edition based on an advanced
          cartographic tool. We also contribute to some specific visualization tools such as the time-space graph.
        </p>
        <p>
          If the project is for now centered on the french railway infrastructure, it is meant to be usable in other
          national contexts. It is supported by the <a href="https://openrailassociation.org/">OpenRail Association</a>{" "}
          which supports efforts to spread open source railway tools across borders.
        </p>
      </>
    ),
  },
  tags: ["industry", "javascript", "maplibre", "react", "typescript"],
  captures: [
    {
      image: osrd_capt_1,
      comment: {
        fr: `L'éditeur d'infrastructure dans OSRD permet de modifier les méta-données des différents éléments d'infrastructure (sections de ligne, signaux, aiguillages...), ou encore de corriger les erreurs d'imports des données d'infrastructure.`,
        en: `The editor allow to edit the metadata of many infrastructure elements (track sections, signals, switches...), or to review and correct data import errors.`,
      },
    },
    {
      image: osrd_capt_2,
      comment: {
        fr: `La vue "warpée" permet d'enrichir les graphiques espaces temps classiques de la SNCF, avec des données d'infrastructure exhaustives, ou encore du contexte géographique grâce aux données OpenStreetMap.`,
        en: `The "warped" view enhances the classical railway space-time diagram with exhaustive infrastructure data mapped on top of OpenStreetMap geographic layers.`,
      },
    },
    {
      image: osrd_capt_3,
      comment: {
        fr: `L'éditeur d'infrastructure permet aussi d'explorer les anomalies d'infrastructure, qui empêchent de faire des simulations correctes. Ces anomalies peuvent ensuite être corrigées manuellement, ou souvent automatiquement.`,
        en: `The editor reveals and allow to correct the infrastructure digital twin anomalies which block simulations to be run correctly. The application embed specific algorithms which can often ease those corrections.`,
      },
    },
  ],
};

export default REFERENCE_OSRD;
