import { type Reference } from "../types";

const REFERENCE_GDOTV: Reference = {
  id: "gdotv",
  serviceId: "consulting",
  customerId: "gdotv",
  title: {
    fr: "Conseil et développement sigma.js",
    en: "Sigma.js consulting",
  },
  abstract: {
    fr: (
      <>
        <p>
          G.V() est une plateforme d'exploration de bases de données graphes, conçue avec des technologies web,
          notamment sigma.js.
        </p>
        <p>
          Nous les accompagnons pour améliorer et optimiser leur intégration optimale de sigma.js. En outre, l'équipe
          G.V() nous sponsorise régulièrement pour développer de nouvelles fonctionnalités open-source, ou encore
          déboguer et optimiser la base de code de sigma.js.
        </p>
      </>
    ),
    en: (
      <>
        <p>G.V() is a graph database exploration platform, designed with web technologies, notably sigma.js.</p>
        <p>
          We support them in enhancing and optimizing their sigma.js integration. Furthermore, the G.V() team regularly
          sponsors us to develop new open-source features, as well as to debug and optimize the sigma.js codebase.
        </p>
      </>
    ),
  },
  tags: ["javascript", "dataviz", "graphology", "sigma"],
};

export default REFERENCE_GDOTV;
