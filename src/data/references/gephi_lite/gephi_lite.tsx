import type { Reference } from "../../types";
import gephi_lite_snapshot_1 from "./gephi_lite_snapshot_1.png";
import gephi_lite_snapshot_2 from "./gephi_lite_snapshot_2.png";
import gephi_lite_snapshot_3 from "./gephi_lite_snapshot_3.png";

const REFERENCE_GEPHI_LITE: Reference = {
  id: "gephi-lite",
  customerId: "gephi",
  serviceId: "opensource",
  title: {
    fr: "Gephi Lite",
    en: "Gephi Lite",
  },
  subtitle: {
    fr: "Développer une version web et allégée de Gephi",
    en: "Developing a lighter, web version of Gephi ",
  },
  links: [
    {
      label: { fr: "Le projet", en: "The project" },
      url: {
        fr: "https://gephi.org/gephi-lite/",
        en: "https://gephi.org/gephi-lite/",
      },
    },
    {
      label: { fr: "Le code source", en: "The sources" },
      url: {
        fr: "https://github.com/gephi/gephi-lite/",
        en: "https://github.com/gephi/gephi-lite/",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          Nous avons pris en charge le développement de Gephi Lite, une initiative visant à créer une version allégée et
          plus accessible du célèbre outil d'analyse de réseaux, Gephi. Notre équipe a conçu et développé Gephi Lite
          pour répondre à une demande croissante d'outils d'analyse de réseaux sociaux simples d'utilisation, sans
          sacrifier la profondeur des analyses.
        </p>
        <p>
          Ce projet, réalisé grâce à notre expertise approfondie en matière de visualisation de données et de
          développement d'applications web de cartographies de réseaux, souligne notre engagement envers le
          développement open-source et le soutien aux communautés académiques et de recherche.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We initiated the development of Gephi Lite, aimed at offering a lighter and more accessible version of the
          renowned network analysis tool, Gephi. Our team designed and developed Gephi Lite to meet the growing demand
          for easy-to-use social network analysis tools, while preserving analytical depth.
        </p>
        <p>
          This project, realized thanks to our extensive expertise in data visualization and the development of web
          applications for network mapping, highlights our commitment to open-source development and support for
          academic and research communities.
        </p>
      </>
    ),
  },
  tags: ["open-source", "dataviz", "javascript", "graphology", "react", "sigma"],
  captures: [
    {
      image: gephi_lite_snapshot_1,
      comment: {
        fr: `Sélection "rectangle" dans un graphe dans Gephi Lite`,
        en: `Marquee selection in a graph in Gephi Lite`,
      },
    },
    {
      image: gephi_lite_snapshot_2,
      comment: {
        fr: `Filtrage dans un graphe dans Gephi Lite`,
        en: `Filtering in a graph in Gephi Lite`,
      },
    },
    {
      image: gephi_lite_snapshot_3,
      comment: {
        fr: `Affichage d'images dans les noeuds dans Gephi Lite`,
        en: `Rendering pictures in nodes in Gephi Lite`,
      },
    },
  ],
};

export default REFERENCE_GEPHI_LITE;
