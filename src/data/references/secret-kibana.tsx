import { type Reference } from "../types";

const REFERENCE_SECRET_KIBANA: Reference = {
  id: "secret-kibana",
  serviceId: "consulting",
  title: {
    fr: "Tableaux de bords de suivi de production",
    en: "Production monitoring dashboard",
  },
  subtitle: {
    fr: (
      <>
        Développement de <i>plugins</i> métier pour Kibana
      </>
    ),
    en: <>Custom Kibana plug-ins development</>,
  },
  abstract: {
    fr: (
      <>
        <p>
          Notre client, un acteur industriel, voulait distribuer des tableaux de bord dans un de leur produit. Après une
          brève étude, <a href="https://www.elastic.co/fr/kibana">Kibana</a> a semblé la meilleure option, mais manquait
          certaines fonctionnalités.
        </p>
        <p>
          Nous avons développé{" "}
          <strong className="highlight">
            un <i>plugin</i> pour Kibana
          </strong>{" "}
          avec ces fonctionnalités (intégration des tableaux de bords dans une page sur mesure, styles personnalisés).
        </p>
      </>
    ),
    en: (
      <>
        <p>
          An industrial actor contacted us to help them distribute dashboards within one of their product. After a brief
          benchmarking, <a href="https://www.elastic.co/kibana">Kibana</a> felt the best solution, despite missing some
          key features.
        </p>
        <p>
          We developed <strong className="highlight">a custom plugin</strong> with these features (integrating the
          dashboards inside a custom web page, with custom styles).
        </p>
      </>
    ),
  },
  tags: ["industry", "kibana", "elasticsearch", "dashboard"],
};

export default REFERENCE_SECRET_KIBANA;
