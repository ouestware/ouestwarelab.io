import type { Reference } from "../../types";
import sigma_snapshot_1 from "./sigma_snapshot_1.png";
import sigma_snapshot_2 from "./sigma_snapshot_2.png";

const REFERENCE_SIGMA: Reference = {
  id: "sigmajs",
  customerId: "sigmajs",
  serviceId: "opensource",
  title: {
    fr: "Sigma.js",
    en: "Sigma.js",
  },
  subtitle: {
    fr: "Améliorer et maintenir une bibliothèque JavaScript open source",
    en: "Improving and maintaining an open source JavaScript library",
  },
  links: [
    {
      label: { fr: "Les exemples", en: "The examples" },
      url: {
        fr: "https://www.sigmajs.org/storybook/",
        en: "https://www.sigmajs.org/storybook/",
      },
    },
    {
      label: { fr: "La documentation", en: "The documentation" },
      url: {
        fr: "https://www.sigmajs.org/docs/",
        en: "https://www.sigmajs.org/docs/",
      },
    },
    {
      label: { fr: "Le code source", en: "The sources" },
      url: {
        fr: "https://github.com/jacomyal/sigma.js",
        en: "https://github.com/jacomyal/sigma.js",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          Nous développons et maintenons sigma.js, une bibliothèque JavaScript open source dédiée à l'affichage de
          graphes. Nous utilisons cette bibliothèque dans une bonne partie de nos projets.
        </p>
        <p>
          Nous pouvons développer à la demande des fonctionnalités métier dédiées à nos clients, ou des fonctionnalités
          plus génériques - open-source autant que possible. Nous maintenons également{" "}
          <a href="https://sim51.github.io/react-sigma/">React Sigma</a>, qui facilite l'utilisation de sigma.js dans
          des applications basées sur React.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We develop and maintain sigma.js, an open source JavaScript library dedicated to drawing networks. We use this
          library in many of our projects.
        </p>
        <p>
          We can develop specific business features on demand, or more generic open-source features when possible. We
          also maintain <a href="https://sim51.github.io/react-sigma/">React Sigma</a>, that makes it easier to use
          sigma.js in React-based applications.
        </p>
      </>
    ),
  },
  tags: ["javascript", "open-source", "dataviz", "graphology", "sigma"],
  captures: [
    {
      image: sigma_snapshot_1,
      comment: {
        fr: `L'application de démonstration du site sigmajs.org`,
        en: `The demo application of the sigmajs.org website`,
      },
    },
    {
      image: sigma_snapshot_2,
      comment: {
        fr: `Un exemple d'intégration métier chez ConspiracyWatch`,
        en: `A business specific integration example for ConspiracyWatch`,
      },
    },
  ],
};

export default REFERENCE_SIGMA;
