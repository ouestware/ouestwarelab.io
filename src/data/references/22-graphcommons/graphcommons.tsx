import type { Reference } from "../../types";
import graphcommons_1 from "./graphcommons_1.png";
import graphcommons_2 from "./graphcommons_2.png";
import graphcommons_3 from "./graphcommons_3.png";

const REFERENCE_GRAPHCOMMONS: Reference = {
  id: "graphcommons",
  serviceId: "consulting",
  customerId: "graphcommons",
  title: {
    fr: "Une nouvelle version de GraphCommons",
    en: "A new GraphCommons version",
  },
  abstract: {
    fr: (
      <p>
        Depuis 2021, nous développons et maintenons pour GraphCommons leur plateforme web de cartographie, d'analyse et
        de partage de données-réseaux. Ce projet a sollicité toute notre expertise en réseaux - de la modélisation et
        des bases de données jusqu'à la visualisation, ainsi qu'en développement web. Nous avons ainsi intégré{" "}
        <a href="https://neo4j.com/">Neo4j</a> côté données, ainsi que <a href="https://www.sigmajs.org/">sigma.js</a>{" "}
        et <a href="https://graphology.github.io/">graphology</a> côté client. Le site est basé sur{" "}
        <a href="https://nextjs.org/">Next.js</a> et <a href="https://react.dev">React</a>. Nous continuons à développer
        régulièrement de nouvelles fonctionnalités, tout en maintenant l'application.
      </p>
    ),
    en: (
      <p>
        Since 2021, we have been developing and maintaining the web platform for GraphCommons, which focuses on mapping,
        analyzing, and sharing network data. This project has leveraged our entire network expertise - from modeling and
        databases to visualization, as well as web development. We have integrated{" "}
        <a href="https://neo4j.com/">Neo4j</a> for data management, and <a href="https://www.sigmajs.org/">sigma.js</a>{" "}
        and <a href="https://graphology.github.io/">graphology</a> for client-side operations. The website is built on{" "}
        <a href="https://nextjs.org/">Next.js</a> and <a href="https://react.dev">React</a>. We continue to regularly
        develop new features while maintaining the application.
      </p>
    ),
  },
  tags: ["javascript", "graphology", "react", "neo4j", "dataviz", "devops", "architecture"],
  links: [
    {
      label: { fr: "Graph Commons", en: "Graph Commons" },
      url: { fr: "https://graphcommons.com/", en: "https://graphcommons.com/" },
    },
  ],
  captures: [
    {
      image: graphcommons_1,
      comment: {
        fr: `Dans un grand graphe sur la 'dataviz', le nœud 'Benjamin Ooghe-Tabanou' est sélectionné. Ses voisins sont mises en évidence sur le graph et dans le panneau de droite qui présente également les attributs du nœud.`,
        en: `In a large graph about 'dataviz', the node 'Benjamin Ooghe-Tabanou' is selected. Its neighbors are highlighted both on the graph and in the panel on the right which also displays the node's attributes.`,
      },
    },
    {
      image: graphcommons_2,
      comment: {
        fr: `La fonctionnalité payante 'Hub' permet de créer de multiples vues sur une même graphe de connaissance. Sur cet écran nous créons une vue à partir d'une recherche de chemins allant des universités aux outils en passant par les personnes.`,
        en: `The 'Hub' paid feature allow to create many views on one common knowledge graph. On this screen, we create a view by aggregating all paths from universities to tools passing through people.`,
      },
    },
    {
      image: graphcommons_3,
      comment: {
        fr: `Une vue permet d'isoler une partie du graphe de connaissance d'un Hub. Le panneau de droite contient sur cet écran la légende des choix de couleurs reflétant le modèle de données du graphe.`,
        en: `A view allow to isolate a part of a larger knowledge graph hosted in a Hub. The right panel shows on this screen the caption revealing the data model of the graph.`,
      },
    },
  ],
};

export default REFERENCE_GRAPHCOMMONS;
