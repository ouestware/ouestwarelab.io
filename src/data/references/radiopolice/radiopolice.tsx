import { type Reference } from "../../types";
import radiopolice_coocurrence_network_legitim from "./radiopolice_coocurrence_network_legitim.png";
import radiopolice_coocurrence_network_palet_grenade from "./radiopolice_coocurrence_network_palet_grenade.png";
import radiopolice_query_in_kibana from "./radiopolice_query_in_kibana.png";

const REFERENCE_RADIOPOLICE: Reference = {
  id: "radiopolice",
  customerId: "wedodata",
  serviceId: "data-valorization",
  title: {
    fr: "RadioPolice",
    en: "RadioPolice",
  },
  subtitle: {
    fr: "Analyse visuelle et extraction sémantique des thèmes d'un corpus de tweets",
    en: "Visual analysis and semantic extraction of themes of a tweets data-set",
  },
  links: [
    {
      label: { fr: "Chez David Dufresne", en: "By David Dufresne" },
      url: { fr: "https://www.davduf.net/-allo_place_beauvau-", en: "https://www.davduf.net/?lang=en" },
    },
    {
      label: {
        fr: "Chez WeDoData",
        en: "By WeDoData",
      },
      url: {
        fr: "https://wedodata.fr/mediapart-alloplacebeauvau.php",
        en: "https://wedodata.fr/mediapart-alloplacebeauvau.php",
      },
    },
    {
      label: { fr: "Mediapart", en: "Mediapart" },
      url: {
        fr: "https://www.mediapart.fr/studio/panoramique/allo-place-beauvau-cest-pour-un-bilan",
        en: "https://www.mediapart.fr/studio/panoramique/allo-place-beauvau-cest-pour-un-bilan",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          En réponse à un besoin d'analyse sémantique d'un corpus de tweets, nous avons mis en place une{" "}
          <strong className="highlight">chaîne d'extraction de thèmes</strong> de ce corpus, par analyse des
          cooccurrences et filtrage de tokens par CHI². Nous avons également sorti un outil en ligne pour explorer les
          communautés thématiques, sous forme de réseaux de cooccurrences des termes.
        </p>
        <p>
          Dans le cadre de la publication du corpus par <a href="http://www.davduf.net/">David Dufresne</a> et le
          journal <a href="https://www.mediapart.fr/">Mediapart</a>, nous avons ensuite proposé l'usage d'
          <a href="https://www.elastic.co/fr/elasticsearch/">ElasticSearch</a> et{" "}
          <a href="https://www.elastic.co/fr/kibana">Kibana</a> pour former des requêtes correspondant à chacun des
          thèmes définis par l'équipe éditoriale, et aggréger les indicateurs représentés dans l'interface finale conçue
          et développée par <a href="https://wedodata.fr">WeDoData</a>,{" "}
          <a href="https://etamin.studio/">Etamin Studio</a> et{" "}
          <a href="https://visionscarto.net/allo-place-beauvau">Philippe Rivière / Visions carto</a>.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We were contacted to semantically analyse a corpus of french tweets. We set up a{" "}
          <strong className="highlight">topic extraction pipe</strong>, through terms co-occurrences analysis and CHI²
          token filtering. We also shared some online tool to explore topic communities, in terms co-occurrences network
          maps.
        </p>
        <p>
          <a href="http://www.davduf.net/">David Dufresne</a> and the <a href="https://www.mediapart.fr/">Mediapart</a>{" "}
          french journal wanted to publish the corpus. We helped set up{" "}
          <a href="https://www.elastic.co/elasticsearch/">ElasticSearch</a> and{" "}
          <a href="https://www.elastic.co/kibana">Kibana</a> to forge one query per curated topic, and to get aggregated
          indicators for the final user interface designed and developed by <a href="https://wedodata.fr">WeDoData</a>,{" "}
          <a href="https://etamin.studio/">Etamin Studio</a> and{" "}
          <a href="https://visionscarto.net/allo-place-beauvau">Philippe Rivière / Visions carto</a>.
        </p>
      </>
    ),
  },
  tags: ["data-journalism", "python", "nlp", "data-science", "visual-analysis", "elasticsearch", "kibana"],
  captures: [
    {
      image: radiopolice_coocurrence_network_legitim,
      comment: {
        fr: `Réseaux de cooccurrences de termes du thème "(il)légitimité"`,
        en: `Co-occurrences network of terms from the "(il)légitimité" theme`,
      },
    },
    {
      image: radiopolice_coocurrence_network_palet_grenade,
      comment: {
        fr: `Voisins de "palet" dans le réseau de cooccurrence des termes significatifs`,
        en: `"palet" neighbors in the co-occurrence network of significant terms`,
      },
    },
    {
      image: radiopolice_query_in_kibana,
      comment: {
        fr: `Construction du thème "outrage" à l'aide d'une requête dans Kibana`,
        en: `Building the theme "outrage" as a search query in Kibana`,
      },
    },
  ],
};

export default REFERENCE_RADIOPOLICE;
