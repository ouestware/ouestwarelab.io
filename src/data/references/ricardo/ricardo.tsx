import { type Reference } from "../../types";
import ricardo_exchange_rates from "./ricardo_exchange_rates.png";
import ricardo_partners_heatmap from "./ricardo_partners_heatmap.png";
import ricardo_political_status from "./ricardo_political_status.png";

const REFERENCE_RICARDO: Reference = {
  id: "ricardo",
  customerId: "CHSP",
  serviceId: "data-valorization",
  title: {
    fr: "RICardo",
    en: "RICardo",
  },
  links: [
    {
      label: { fr: "RICardo", en: "RICardo" },
      url: { fr: "http://ricardo.medialab.sciences-po.fr", en: "http://ricardo.medialab.sciences-po.fr" },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: { fr: "https://github.com/medialab/ricardo/", en: "https://github.com/medialab/ricardo/" },
    },
    {
      label: { fr: "Sur notre blog", en: "On our blog" },
      url: { fr: "/2020/10/19/nouvelles-visualisations-RICardo/", en: "/2020/10/19/new-visualizations-RICardo/" },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          RICardo est un projet de recherche dédié au commerce entre les nations, de la Révolution industrielle à la
          veille de la Seconde Guerre mondiale.
        </p>
        <p>Nous avons amélioré l'application web existante :</p>
        <ul>
          <li>Refonte des visualisations existantes</li>
          <li>
            Nouvelles visualisations des <strong className="highlight">taux de change</strong> et des{" "}
            <strong className="highlight">statuts politiques</strong>
          </li>
          <li>Ajout de permaliens avec les paramètres des visualisations sur toutes les pages</li>
        </ul>
        <p>
          Lire notre post de blog{" "}
          <a href="/2020/10/19/nouvelles-visualisations-RICardo/">"De nouvelles visualisations pour RICardo"</a> pour
          découvrir les détails de cette prestation.
        </p>
      </>
    ),
    en: (
      <>
        <p>RICardo is a research project about international trades in the 19-20th centuries.</p>
        <p>We improved the existing web application:</p>
        <ul>
          <li>Refactoring of the existing visualizations</li>
          <li>
            New visualizations of <strong className="highlight">exchange rates</strong> and{" "}
            <strong className="highlight">political statuses</strong>
          </li>
          <li>Permalinks with visualization parameters, on all pages</li>
        </ul>
        <p>
          Read our blog post{" "}
          <a href="/2020/10/19/new-visualizations-RICardo/">"Some new visualizations for the RICardo project"</a> to
          learn more about this contract.
        </p>
      </>
    ),
  },
  tags: ["digital-humanities", "dataviz", "conception", "javascript", "angularjs"],
  captures: [
    {
      image: ricardo_political_status,
      comment: {
        fr: `Cette frise incite à considérer le contexte géopolitique dans l'analyse du commerce`,
        en: `This additionnal timeline foster taking the political context into account when analysing historical trade`,
      },
    },
    {
      image: ricardo_partners_heatmap,
      comment: {
        fr: `Utilisation d'une heatmap pour comparer l'importance relative des partenaires commerciaux`,
        en: `We created a heatmap to compare relative importance of trade partners`,
      },
    },
    {
      image: ricardo_exchange_rates,
      comment: {
        fr: `Exploration de la base de taux de change par des small-multiples`,
        en: `Exchange rates exploration through small-multiples`,
      },
    },
  ],
};

export default REFERENCE_RICARDO;
