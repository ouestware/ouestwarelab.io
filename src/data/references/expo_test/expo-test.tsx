import type { Reference } from "../../types";
import expo_test_data_infra_extract from "./expo_test_data_infra_extract.png";
import expo_test_physical_infra_extract from "./expo_test_physical_infra_extract.png";

const REFERENCE_EXPO_TEST: Reference = {
  id: "expo-test",
  customerId: "ensadlab",
  serviceId: "consulting",
  title: {
    fr: "Exposition-test",
    en: "Exhibition-test",
  },
  subtitle: {
    fr: "Spécifications de l'infrastructure de données d'une exposition interactive",
    en: "Data infrastructure specifications of an interactive exhibition",
  },
  links: [],
  abstract: {
    fr: (
      <>
        <p>
          Nous avons conçu l'infrastructure des données d'une exposition observant ses visiteurs : spécifications des
          flux de données depuis les systèmes de captation, jusqu'aux mur-écrans projetant les visualisations en passant
          par les processus d'analyse, d'archivage et de rendus graphiques.
        </p>
        <p>
          L'exposition ayant été annulée à cause de l'épidémie de COVID-19, nous n'avons pas pu passer en production
          pour le moment.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We designed the data infrastructure for an exhibition which observes its visitors: data flows specifications
          from data capture to video-walls projecting visualisations, going through analysis, archiving and graphic
          rendering processes.
        </p>
        <p>The exhibition was canceled due to COVID-19 epidemic. We haven't realized those plans yet.</p>
      </>
    ),
  },
  tags: ["digital-humanities", "realtime-data", "dataviz", "conception", "architecture"],
  captures: [
    {
      image: expo_test_data_infra_extract,
      comment: {
        fr: "Extrait du schéma de l'infrastucture de données",
        en: "Data infrastucture schema extract",
      },
    },
    {
      image: expo_test_physical_infra_extract,
      comment: {
        fr: "Extrait du schéma de l'infrastucture physique",
        en: "Physical infrastucture schema extract",
      },
    },
  ],
};

export default REFERENCE_EXPO_TEST;
