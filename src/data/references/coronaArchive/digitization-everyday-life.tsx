import type { Reference } from "../../types";
import coronaArchive_doc_view from "./coronaArchive_doc_view.png";
import coronaArchive_search from "./coronaArchive_search.png";
import coronaArchive_segment_view from "./coronaArchive_segment_view.png";

const REFERENCE_DIGITIZATION_EVERYDAY_LIFE: Reference = {
  id: "digitizationEverydaylife",
  customerId: "TANTLAB",
  serviceId: "data-valorization",
  title: {
    fr: "La numérisation de la vie quotidienne pendant la pandémie",
    en: "The Digitization of Everyday Life During the Corona Crisis",
  },
  links: [
    {
      label: { fr: "Le projet (en)", en: "Project" },
      url: {
        fr: "https://deltagelsensgrammatik.itu.dk/english/",
        en: "https://deltagelsensgrammatik.itu.dk/english/",
      },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: {
        fr: "https://github.com/tantlab/digitazation-of-everyday-life/",
        en: "https://github.com/tantlab/digitazation-of-everyday-life/",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          Nous avons développé une application web qui permet à une équipe de recherche d'analyser un corpus
          d'observations ethnographiques en permettant de naviguer et qualifier le matériel collecté. Ce corpus a été
          collecté pendant le confinement lié au COVID-19 entre avril et juin 2020 au Danemark. Il contient 222
          entretiens, 84 journaux, et 89 observations de terrain.
        </p>
        <p>
          {" "}
          Cette étude fait partie du projet{" "}
          <a href="https://deltagelsensgrammatik.itu.dk/om-projektet/">
            "The Grammar of Participation: The Digitization of Everyday Life During the Corona Crisis"
          </a>{" "}
          qui a été mené par des chercheurs du <a href="https://cdw.itu.dk">Centre for Digital Welfare</a> de la{" "}
          <a href="https://www.itu.dk">IT University of Copenhagen</a> et du{" "}
          <a href="https://www.tantlab.aau.dk">Techno-Anthropology Lab</a> de la{" "}
          <a href="https://www.aau.dk">University of Aalborg</a>.
        </p>
        <p>
          Cet outil n'est pas accessible publiquement. L'accès aux données est réservé à l'équipe de recherche. Les
          copies d'écran ci-dessous ont été réalisé avec de fausses données.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          We developped a web application which allows a research team to analyse an ethnographic data-set by navigating
          and qualifying the collected materials. The data-set was collected during the COVID-19 lockdown that took
          place between April and June, 2020 in Denmark. It includes 222 interviews, 84 online diaries, and 89 field
          observations.
        </p>
        <p>
          {" "}
          This study was part of the project{" "}
          <a href="https://deltagelsensgrammatik.itu.dk/om-projektet/">
            "The Grammar of Participation: The Digitization of Everyday Life During the Corona Crisis"
          </a>{" "}
          which was carried out in collaboration between researchers from the{" "}
          <a href="https://cdw.itu.dk">Centre for Digital Welfare</a> at the{" "}
          <a href="https://www.itu.dk">IT University of Copenhagen</a> and the{" "}
          <a href="https://www.tantlab.aau.dk">Techno-Anthropology Lab</a> at{" "}
          <a href="https://www.aau.dk">University of Aalborg</a>.
        </p>
        <p>
          This tools is not publicly available. Access to the data is restricted to the research team. The screenshots
          below were made on fake data.
        </p>
      </>
    ),
  },
  tags: ["digital-humanities", "javascript", "react", "typescript", "elasticsearch"],
  captures: [
    {
      image: coronaArchive_search,
      comment: {
        fr: `Un moteur de recherche de passage d'entretiens et d'observations de terrain`,
        en: `A search engine on interviews and field observations segments`,
      },
    },
    {
      image: coronaArchive_doc_view,
      comment: {
        fr: `Chaque document du corpus a sa propre page web.`,
        en: `Each data-set document has its own web page.`,
      },
    },
    {
      image: coronaArchive_segment_view,
      comment: {
        fr: `Les documents ont été découpés en segment. Chaque segment peut être référencé et qualifié par des tags.`,
        en: `Each document has been segmented. Segments can be referenced by their URL and qualified by inputing tags.`,
      },
    },
  ],
};

export default REFERENCE_DIGITIZATION_EVERYDAY_LIFE;
