import type { Reference } from "../../types";
import archelec_list_jpeg from "./archelec_list.png";
import archelec_pf from "./archelec_pf.png";
import archelec_viz_candidates from "./archelec_viz_candidates.png";
import archelec_viz_time_space from "./archelec_viz_time_space.png";

const REFERENCE_ARCHELEC: Reference = {
  id: "archelec",
  customerId: "SciencesPo",
  serviceId: "data-valorization",
  title: {
    fr: "Explorer les professions de foi des élections de la Ve République",
    en: "Explore the french elections candidates' professions of faith since 1958",
  },
  links: [
    {
      label: { fr: "ARCHELEC", en: "ARCHELEC" },
      url: {
        fr: "https://archelec.sciencespo.fr/",
        en: "https://archelec.sciencespo.fr/",
      },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: {
        fr: "https://github.com/SciencesPoDRIS/archelec4/",
        en: "https://github.com/SciencesPoDRIS/archelec4/",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          Depuis 2013, la <a href="https://www.sciencespo.fr/bibliotheque/">Bibliothèque de Sciences Po</a> pilote la
          mise en ligne des archives électorales réunies par le{" "}
          <a href="https://www.sciencespo.fr/cevipof">Centre de recherches politiques (CEVIPOF)</a> et désormais
          conservées au Département archives de la Bibliothèque : un fonds unique de professions de foi de candidats aux
          élections (législatives, mais aussi présidentielles, européennes, etc.) depuis 1958.
        </p>
        <p>
          {" "}
          Après l'avoir publié sur{" "}
          <a href="https://archive.org/details/archiveselectoralesducevipof?tab=about">Internet Archive</a>, la
          Bibliothèque de Sciences Po nous a confié la conception et le développement sur-mesure d'une application
          d'exploration du corpus, afin d'exploiter la très riche indexation de plus de trente mille documents. Nous
          avons développé une application web qui permet de filtrer les professions de foi par élection,
          circonscription, groupe politique, profil des candidat⋅e⋅s...
        </p>
        <p>
          Les résultats du filtrage peuvent ensuite être explorés en listes, en visualisations de données ou
          téléchargeables en CSV. Enfin le document original est consultable grâce au lecteur fourni par Internet
          Archive. Ainsi les choix d'indexation faits par les bibliothécaires et archivistes du projet peuvent être
          confrontés au document source.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          From 2013, <a href="https://www.sciencespo.fr/bibliotheque/">the Sciences Po Library</a> manage the
          publication of the electoral archives created by the{" "}
          <a href="https://www.sciencespo.fr/cevipof">Centre de recherches politiques (CEVIPOF)</a> and now preserved by
          the Library Département archives: a unique collection of election (legislative but also presidential,
          european, etc.) candidates professions of faith from 1958.
        </p>
        <p>
          After having published it on{" "}
          <a href="https://archive.org/details/archiveselectoralesducevipof?tab=about">Internet Archive</a>, the
          Sciences Po Library came to us to build a custom exploration tool. Indeed they built a very rich set of
          metadata which describe the candidates profils in details for the entire collection (more than thurty
          thousands documents). We created a faceted search engine alowing to filter the collection by election,
          electoral division, political group, candidates profils...
        </p>
        <p>
          The resulting documents can then be explored either through lists, data visualisations or downloaded in CSV.
          The original documents are available thanks to Internet Archive's embeded player. Thus indexation choices made
          by the librarists and archivists can be checked against the source.
        </p>
      </>
    ),
  },
  tags: ["digital-humanities", "javascript", "react", "typescript", "elasticsearch"],
  captures: [
    {
      image: archelec_list_jpeg,
      comment: {
        fr: `Un moteur de recherche par facettes de professions de foi des candidat⋅e⋅s aux élections législatives.`,
        en: `A faceted search engine on legislative elections' candidates' professions of faith.`,
      },
    },
    {
      image: archelec_viz_time_space,
      comment: {
        fr: `Répartition visuelle des documents sélectionnés par année et par département.`,
        en: `Visualisation of the selected document in time and in the french territory.`,
      },
    },
    {
      image: archelec_viz_candidates,
      comment: {
        fr: `Visualisation des profils des candidat⋅e⋅s: pyramide des âges, professions, soutiens...`,
        en: `Candidates' profils: gender and age, professions, political support...`,
      },
    },
    {
      image: archelec_pf,
      comment: {
        fr: `Pour une profession de foi, les métadonnées peuvent être comparées au document original hébergé par Internet Archive.`,
        en: `For one document, metadata and original scan can be read side by side.`,
      },
    },
  ],
};

export default REFERENCE_ARCHELEC;
