import type { Reference } from "../../types";
import lasso_bird_traffic from "./lasso_bird_traffic.png";
import lasso_home from "./lasso_home.png";
import lasso_pleasant_noise from "./lasso_pleasant_noise.png";

const REFERENCE_LASSO: Reference = {
  id: "lasso",
  customerId: ["UMRAE", "ETIS"],
  serviceId: "data-valorization",
  title: {
    fr: "Renouveler notre compréhension des paysages sonores urbains",
    en: "Renew our understanding of urban sonor landscapes",
  },
  links: [
    {
      label: { fr: "LASSO", en: "LASSO" },
      url: {
        fr: "https://universite-gustave-eiffel.github.io/lasso/",
        en: "https://universite-gustave-eiffel.github.io/lasso/",
      },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: {
        fr: "https://github.com/Universite-Gustave-Eiffel/lasso/",
        en: "https://github.com/Universite-Gustave-Eiffel/lasso/",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          The LASSO platform est une plateforme web publiant des jeux de données spatio-temporel décrivant des paysages
          sonores. Ces jeux de données ont été produits en collaboration par des équipes de recherche de l'Université de
          Gustave Eiffel et de l'Université de Cergy-Pontoise. Cette plateforme vise à présenter les avantages de la
          cartographie des paysages sonores qui propose une approche perceptive beaucoup plus fine que les cartes de
          bruits standards. Elle fournit aux chercheurs et décideurs des jeux de données exclusifs ainsi qu'un
          démonstrateur des potentiels d'analyse de cette approche.
        </p>
        <p>
          Cette plateforme a pour ambition de participer à une meilleure compréhension du rôle que les paysages de
          données ont à jouer dans la construction des environnements urbains de demain.
        </p>
        <p>
          Nous avons conçu et développé cette plateforme en une application react sans serveur utilisant la technologie
          de cartographie vectorielle <a href="https://maplibre.org/">MapLibre</a>.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          The LASSO platform is a web-based platform that presents a range of spatio-temporal dataset related to
          soundscapes. These dataset are the result of collaborative projects between Gustave Eiffel University and the
          University of Cergy-Pontoise. The platform aims to demonstrate the value of soundscape mapping in comparison
          to standardized noise mapping, and to provide researchers and data analysts with access to exclusive
          soundscapes datasets.
        </p>
        <p>
          In addition to serving as a repository of unique datasets, the LASSO platform is committed to advancing our
          understanding of the role of soundscapes in shaping our environment.
        </p>
        <p>
          We designed and developed this platform as a serverless react application powering advanced vector tile
          cartography thanks to <a href="https://maplibre.org/">MapLibre</a>.
        </p>
      </>
    ),
  },
  tags: ["acoustics", "maplibre", "typescript", "react", "javascript"],
  captures: [
    {
      image: lasso_home,
      comment: {
        fr: `La plateforme LASSO propose d'explorer plusieurs paysages sonores créés par les chercheurs du projet.`,
        en: `The LASSO platform proposes to explore different soundscapes created by the research team`,
      },
    },
    {
      image: lasso_pleasant_noise,
      comment: {
        fr: `Deux cartes sont synchronisées pour faciliter la comparaison de variables: 'agrément' à gauche, niveaux de bruits standards à droite.`,
        en: `Two maps are synchronised to ease to compare variables: 'pleasant' on the left, standard noise levels on the right.`,
      },
    },
    {
      image: lasso_bird_traffic,
      comment: {
        fr: `Pour chaque point de la carte, on retrouve les valeurs des variables composant le paysage sonore : temps de présence d'oiseaux, du traffic, de voix, niveau sonore et les deux variables émotionnelles agrément et animation.`,
        en: `On each data point are listed the soundscapes variables values: frequency of sound sources from birds, traffic, voices, the sound level as well as the two emotional variables pleasant and liveliness.`,
      },
    },
  ],
};
export default REFERENCE_LASSO;
