import type { Reference } from "../../types";
import hyphe_browser_3 from "./hyphe_browser_3.png";
import hyphe_browser_4 from "./hyphe_browser_4.png";
import hyphe_text_indexation_test from "./hyphe_text_indexation_test.png";

const REFERENCE_HYPHE: Reference = {
  id: "hyphe",
  customerId: "medialab",
  serviceId: "opensource",
  title: {
    fr: "Hyphe",
    en: "Hyphe",
  },
  subtitle: {
    fr: "Indexation de contenu web et déploiement automatisé sur OpenStack",
    en: "Web content indexation and automatized deployment on OpenStack",
  },
  links: [
    {
      label: { fr: "Hyphe", en: "Hyphe" },
      url: { fr: "http://hyphe.medialab.sciences-po.fr/", en: "http://hyphe.medialab.sciences-po.fr/" },
    },
    {
      label: { fr: "Hyphe Browser", en: "Hyphe Browser" },
      url: {
        fr: "https://medialab.sciencespo.fr/outils/hyphe-browser/",
        en: "https://medialab.sciencespo.fr/en/tools/hyphe-browser/",
      },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          <strong className="highlight">Hyphe</strong> est un <i>crawler web</i> conçu pour les chercheurs en sciences
          sociales, et développé par le <strong className="highlight">médialab de Sciences-Po</strong>.
        </p>
        <p>Nous y avons ajouté les fonctionnalités suivantes :</p>
        <ul>
          <li>
            Indexation textuelle automatique des corpus web par extraction puis indexation <i>multiprocess</i> des
            contenus dans <a href="https://www.elastic.co/fr/elasticsearch/">ElasticSearch</a>
          </li>
          <li>
            Déploiement automatique de serveurs Hyphe chez des hébergeurs compatibles{" "}
            <a href="https://www.openstack.org/">OpenStack</a>
          </li>
        </ul>
      </>
    ),
    en: (
      <>
        <p>
          <strong className="highlight">Hyphe</strong> is a <i>web crawler</i> designed for social scientists, and
          developed by <strong className="highlight">Sciences-Po médialab</strong>.
        </p>
        <p>We added the following features:</p>
        <ul>
          <li>
            Automatic textual indexation of web corpora by multiprocess content extraction and indexation in
            <a href="https://www.elastic.co/elasticsearch/">Elasticsearch</a>
          </li>
          <li>
            Automatic deployment of Hyphe server on <a href="https://www.openstack.org/">OpenStack</a> compatible
            hosting services
          </li>
        </ul>
      </>
    ),
  },
  tags: ["digital-humanities", "python", "elasticsearch", "javascript", "devops", "openstack"],
  captures: [
    {
      image: hyphe_text_indexation_test,
      comment: {
        fr: "Tests fonctionnels du processus d'indexation",
        en: "Functional tests of indexation processus",
      },
    },
    {
      image: hyphe_browser_3,
      comment: {
        fr: "Configuration du serveur Hyphe à déployer (Hyphe Browser)",
        en: "Hyphe server to be deployed configuration (Hyphe Browser)",
      },
    },
    {
      image: hyphe_browser_4,
      comment: {
        fr: "Choix des capacités du serveur cloud à déployer (Hyphe Browser)",
        en: "Cloud server to be deployed specifications (Hyphe Browser)",
      },
    },
  ],
};

export default REFERENCE_HYPHE;
