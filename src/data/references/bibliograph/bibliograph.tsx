import type { Reference } from "../../types";
import bibliograph_filters from "./bibliograph_filters.png";
import bibliograph_home from "./bibliograph_home.png";
import bibliograph_network from "./bibliograph_network.png";

const REFERENCE_BIBLIOGRAPH: Reference = {
  id: "bibliograph",
  customerId: "CIS",
  serviceId: "data-valorization",
  title: {
    fr: "Bibliograph",
    en: "Bibliograph",
  },
  links: [
    {
      label: { fr: "Bibliograph", en: "Bibliograph" },
      url: { fr: "https://tommv.github.io/bibliograph/", en: "https://tommv.github.io/bibliograph/" },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: { fr: "https://github.com/tommv/bibliograph/", en: "https://github.com/tommv/bibliograph/" },
    },
  ],
  abstract: {
    fr: (
      <p>
        Bibliograph est un outil en ligne que nous avons créé avec et pour{" "}
        <a href="http://www.tommasoventurini.it">Tommaso Venturini</a> dans le but d'
        <strong className="highlight">équiper ses recherches sur les dynamiques des communautés scientifiques</strong>.
        Notre mission consistait à reproduire une méthode d'analyse par co-références{" "}
        <a href="https://github.com/medialab/bibliotools3.0">déjà implémentées en python</a> dans un outils en ligne
        allant jusqu'à l'exploration visuelle des réseaux produits. Une contrainte très forte de temps nous a poussé à
        choisir de réaliser ce projet en <strong className="highlight">un atelier intensif colocalisé</strong> avec le
        client. En naviguant entre idées et contraintes à l'aide d'une méthode agile nous sommes parvenu à produire un
        outil simple et efficace de scientométrie conforme au besoin en un temps très court.
      </p>
    ),
    en: (
      <p>
        Bibliograph is an online tool which we created with and for{" "}
        <a href="http://www.tommasoventurini.it">Tommaso Venturini</a> to{" "}
        <strong className="highlight">equip his research on scientific communities dynamics</strong>. Our mission was to
        reproduce an analysis method based on co-references{" "}
        <a href="https://github.com/medialab/bibliotools3.0">already implemented in python</a> in a web tool allowing to
        visually explore the networks produced. A very high time constraint guides us to chose to develop this project
        under the form of <strong className="highlight">a colocalized intensive workshop</strong> with the client. By
        navigating between ideas and constraints with the help of an agile method, we succeeded in producing simple yet
        efficient scientometric tool complying the scientific needs in a very short time.
      </p>
    ),
  },
  tags: [
    "digital-humanities",
    "visual-analysis",
    "conception",
    "javascript",
    "react",
    "typescript",
    "sigma",
    "graphology",
  ],
  captures: [
    {
      image: bibliograph_home,
      comment: {
        fr: `Première étape: importer un corpus au format CSV.`,
        en: `Frist step: import CSV files data-set.`,
      },
    },
    {
      image: bibliograph_filters,
      comment: {
        fr: `Après parsing et indexation: définition des filtres.`,
        en: `After parsing and indexation: filters settings.`,
      },
    },
    {
      image: bibliograph_network,
      comment: {
        fr: `Enfin, le réseau de co-référence avec des noeuds de métadonnées est visualisé.`,
        en: `Finally, the co-reference network with metadata nodes visualized.`,
      },
    },
  ],
};

export default REFERENCE_BIBLIOGRAPH;
