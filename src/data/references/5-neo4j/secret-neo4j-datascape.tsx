import type { Reference } from "../../types";
import neo4j_cmdb_fullscreen from "./neo4j-cmdb-fullscreen.png";
import neo4j_cmdb_nodepage from "./neo4j-cmdb-nodepage.png";
import neo4j_cmdb_search from "./neo4j-cmdb-search.png";

const REFERENCE_SECRET_NEO4J_DATASCAPE: Reference = {
  id: "secret-neo4j-datascape",
  serviceId: "full-project",
  title: {
    fr: "Configuration management database",
    en: "Configuration management database",
  },
  subtitle: {
    fr: (
      <>
        Exploration d'une <i>CMDB</i> à l'aide de réseaux égocentrés
      </>
    ),
    en: (
      <>
        Exploring a <i>CMDB</i> through ego-centered networks
      </>
    ),
  },
  abstract: {
    fr: (
      <>
        <p>
          Un des plus grands groupes industriels français possédant l'intégralité de son système informatique (
          <a href="https://fr.wikipedia.org/wiki/Configuration_management_database">CMDB</a>) dans une base{" "}
          <a href="https://neo4j.com/">Neo4j</a>, avait besoin d'une interface d'exploration de son infrastructure.
        </p>
        <p>
          L'application se compose d'un <strong className="highlight">moteur de recherche</strong>, et d'une page pour
          chaque noeud du graphe, présentant son voisinage et ses métadonnées. Pour avoir un moteur de recherche
          efficace (tolérance d'erreurs, recherche sur différents champs), nous avons indexé les données dans un{" "}
          <a href="https://www.elastic.co/fr/elasticsearch/">ElasticSearch</a>.
        </p>
        <p>
          L'interface est développée avec <a href="https://angular.io/">Angular</a>, et{" "}
          <a href="https://nodejs.org/en/">Node</a> pour l'API - le tout avec{" "}
          <a href="https://www.typescriptlang.org/">TypeScript</a>.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          One of the largest French industrial group indexed their whole{" "}
          <a href="https://en.wikipedia.org/wiki/Configuration_management_database">CMDB</a> in a{" "}
          <a href="https://neo4j.com/">Neo4j</a> database. They contacted us to develop an interface to explore this
          dataset.
        </p>
        <p>
          The web application is composed of a <strong className="highlight">search engine</strong> and a unique page
          for each node, displaying its neighborhood and metadata. To make the search engine efficient (errors
          tolerance, searching on multiple fields), we indexed the corpus in an{" "}
          <a href="https://www.elastic.co/elasticsearch/">ElasticSearch</a> base.
        </p>
        <p>
          The frontend is developed with <a href="https://angular.io/">Angular</a>, and the API runs on{" "}
          <a href="https://nodejs.org/en/">Node</a> - the whole with{" "}
          <a href="https://www.typescriptlang.org/">TypeScript</a>.
        </p>
      </>
    ),
  },
  tags: ["industry", "neo4j", "elasticsearch", "angular", "sigma"],
  captures: [
    {
      image: neo4j_cmdb_search,
      comment: {
        fr: "Page de recherche, à travers les différents types de noeuds",
        en: `Search page, through the different node types`,
      },
    },
    {
      image: neo4j_cmdb_nodepage,
      comment: {
        fr: `Page d'un noeud, avec son réseau égocentré, la liste de ses voisins directs et ses métadonnées`,
        en: `Node page, with its ego-centered network, the list of its direct neighbors and its metadata`,
      },
    },
    {
      image: neo4j_cmdb_fullscreen,
      comment: {
        fr: `Exploration des réseaux en plein écran`,
        en: `Fullscreen network exploration`,
      },
    },
  ],
};

export default REFERENCE_SECRET_NEO4J_DATASCAPE;
