import { type Reference } from "../types";

const REFERENCE_OSCARO: Reference = {
  id: "oscaro",
  serviceId: "consulting",
  customerId: "oscaro",
  title: {
    fr: "Paiement en ligne et e-commerce",
    en: "E-commerce and online payment",
  },
  abstract: {
    fr: (
      <p>
        Nous avons participé au développement du tunnel de paiement d'un des plus gros sites français de e-commerce, en{" "}
        <a href="https://clojure.org/">Clojure</a> et <a href="https://clojurescript.org/">ClojureScript</a>.
      </p>
    ),
    en: (
      <p>
        We helped developing the payment process of one of the biggest French e-commerce websites, using{" "}
        <a href="https://clojure.org/">Clojure</a> and <a href="https://clojurescript.org/">ClojureScript</a>.
      </p>
    ),
  },
  tags: ["industry", "clojure", "clojurescript", "web-performance"],
};

export default REFERENCE_OSCARO;
