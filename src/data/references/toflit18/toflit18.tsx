import { type Reference } from "../../types";
import toflit18_cypher_optim_2 from "./toflit18_cypher_optim_2.png";
import toflit18_flows_table from "./toflit18_flows_table.png";
import toflit18_permalink_terms_network_Nantes_export from "./toflit18_permalink_terms_network_Nantes_export.png";

const REFERENCE_TOFLIT18: Reference = {
  id: "toflit18",
  customerId: "dauphineLEDA",
  serviceId: "opensource",
  title: {
    fr: "TOFLIT18",
    en: "TOFLIT18",
  },
  links: [
    {
      label: { fr: "TOFLIT18", en: "TOFLIT18" },
      url: { fr: "http://toflit18.medialab.sciences-po.fr", en: "http://toflit18.medialab.sciences-po.fr" },
    },
    {
      label: { fr: "Carnet de recherche", en: "Blog" },
      url: { fr: "https://toflit18.hypotheses.org/", en: "https://toflit18.hypotheses.org/" },
    },
    {
      label: { fr: "Code source", en: "Source code" },
      url: { fr: "https://github.com/medialab/toflit18/", en: "https://github.com/medialab/toflit18/" },
    },
  ],
  abstract: {
    fr: (
      <>
        <p>
          TOFLIT18 est un outil d'exploration visuelle du commerce par marchandise de la France au XVIIIe siècle. Nous
          avons amélioré cet outil créé par le médialab de Sciences Po en optimisant les requêtes Neo4j et en ajoutant
          une table de données de flux de commerce ainsi qu'un système de permaliens.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          Toflit18 is an online exploratory analysis tool on 18th century french trade by products. We updated this tool
          created by Sciences Po médialab by optimizing the Neo4j queries and by adding permalinks and a data table
          which lists and filters trade flows.
        </p>
      </>
    ),
  },
  tags: ["digital-humanities", "neo4j", "javascript", "react"],
  captures: [
    {
      image: toflit18_flows_table,
      comment: {
        fr: `Les flux de commerce exportés depuis Nantes entre 1720 et 1780`,
        en: `Nantes exports trade flows from 1720 to 1780`,
      },
    },
    {
      image: toflit18_cypher_optim_2,
      comment: {
        fr: `Optimisation du calcul du taux de couverture des classifications`,
        en: `Classifications coverage ratio pptimization`,
      },
    },
    {
      image: toflit18_permalink_terms_network_Nantes_export,
      comment: {
        fr: `Le permalien du réseau de termes des exports de Nantes au XVIIIe siècle`,
        en: `Permalink of 18th century Nantes exports terms networks`,
      },
    },
  ],
};

export default REFERENCE_TOFLIT18;
