import type { Reference } from "../../types";
import entites from "./cartographie_entites.png";
import perso from "./cartographie_personnalisee.png";
import tutoriel from "./cartographie_tutoriel.gif";
import home from "./home_animations.gif";

const REFERENCE_MEL: Reference = {
  id: "mel",
  customerId: ["mel", "datactivist"],
  serviceId: "data-valorization",
  title: {
    fr: "Découvrir la diversité des métiers d'une métropole",
    en: "Discover jobs diversity in a metropolis",
  },
  links: [],
  abstract: {
    fr: (
      <>
        <p>
          La Métropole Européenne de Lille (MEL) emploie de nombreux collaborateurs pour animer les services qu'elles
          offrent à ses administrés. De la jardinière au comptable, une très grande diversité de métiers et de
          compétences sont mobilisés dans cette grande organisation publique. Nous avons travaillé en collaboration avec
          les équipes de Datactivist à la création d'une série d'outils d'exploration visuelle permettant de se plonger
          dans l'environnement professionnel de la MEL.
        </p>
        <p>
          Il s'agit de démystifier cette grande organisation en révélant les détails des services, métiers et
          compétences qui l'animent. Pour des personnes extérieures à l'organisation qui pourraient par exemple
          souhaiter rejoindre l'organisation mais aussi pour les agents de la MEL qui souhaitent se situer voire
          identifier des pistes d'évolutions de carrière.
        </p>
        <p>
          Pour répondre à ces objectifs nous avons conçus deux modes d'exploration : tout représenter ou construire une
          représentation de proche en proche en fonction de ces intérêts. Dans les deux cas, un soin particulier a été
          porté à l'accompagnement des utilisateurs en proposant une navigation par menu contextuel ainsi que des
          tutoriels interactifs détaillant les modes d'interactions avec les visualisations.
        </p>
      </>
    ),
    en: (
      <>
        <p>
          The Métropole Européenne de Lille (MEL) provides public services thanks to many collaborators, whose
          occupations are numerous from gardeners to accounting. In collaboration with the Datactivist team, we designed
          and developed a series of visual exploration tools to dive into the professional landscapes of this large
          public administration.
        </p>
        <p>
          Our goals: reveal the many departments, occupations and skills which compose the MEL working community. For
          external people who might consider joining the organisation, or for internal collaborators who would like to
          better understand the bigger picture or identify possible future career paths.
        </p>
        <p>
          To meet those objectives, we designed two complementary exploration modes: draw the whole or build a custom
          small scale map from precised interests. In both cases, we made sure to guide users into their visual
          explorations by introducing our applications with rich interactive guided tours and by contextualizing them
          with a textual navigation panel.
        </p>
      </>
    ),
  },
  tags: ["rh", "sigma", "typescript", "react", "javascript"],
  captures: [
    {
      image: home,
      comment: {
        fr: `Une animation sur la page d'accueil représente la communauté des agents formant la MEL se recomposant par pôle et métiers les plus peuplés.`,
        en: `On the homepage, an animation depicts MEL's officers community grouping them by major departments and occupations.`,
      },
    },
    {
      image: entites,
      comment: {
        fr: `La cartographie des entités représente l'organisation de la MEL en pôle, directions, services... Quand une entité est sélectionnée, un panneau affiche la liste des entités qui y sont liées, ainsi qu'un bouton permettant de créer une cartographie personnalisée depuis cette entité.`,
        en: `The departments map reveals the metropolis internal administrative organisation by departments, services, team... When an entity is selected a panel displays the list of linked departments and proposes to create a custom map starting from it.  `,
      },
    },
    {
      image: perso,
      comment: {
        fr: `La cartographie personnalisée permet de créer un réseau en y ajoutant successivement les entités, compétence ou métier d'intérêt. Soit en les cherchant, soit en les découvrant dans le voisinage des éléments déjà ajoutés.`,
        en: `In the custom map one can create a network by adding point of interests (entities, skills or occupations) one by one. New elements can be added either by searching or by picking them from the neighborhood of current nodes.`,
      },
    },
    {
      image: tutoriel,
      comment: {
        fr: `Un tutoriel interactif présente les différents modes d'interaction de l'application. Le tutoriel met en évidence les boutons et déclenche automatiquement certaines actions pour mieux illustrer leurs impacts.`,
        en: `An interactive guided tour shows the possible interactions. Not only buttons are highlighted but some actions are automatically triggered to better illustrate their consequences.`,
      },
    },
  ],
};
export default REFERENCE_MEL;
