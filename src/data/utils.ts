import type { CollectionEntry } from "astro:content";

export function getPostPath(post: CollectionEntry<"posts">) {
  const slug = post.slug;
  const dateParsed = post.data.date.match(/^(\d{4})-(\d{2})-(\d{1,2})(T.*)?$/);
  if (slug && dateParsed) {
    const [_, year, month, day] = dateParsed;
    return { path: `/${year}/${month}/${day}/${slug}`, year, month, day, slug };
  }
  return null;
}
