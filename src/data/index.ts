import values from "lodash/values";

import achievements from "./achievements";
import events from "./events";
import productions from "./productions";
import publications from "./publications";
import releases from "./releases";
import services from "./services/services";
import tags from "./tags";
import { type Customer, type FullData, type Reference } from "./types";

// method to consume all promises produced by dynamic import at once
// we might let promise resolution be done at data load?
async function importCollection<T extends Customer | Reference>(
  importResults: Record<string, () => Promise<{ default: T }>>,
): Promise<T[]> {
  const collection = await Promise.all(values(importResults).map(async (v) => (await v()).default));
  return collection.filter((c) => !c.skip);
}

// dynamic import of all data file from collections folders
const customers = importCollection<Customer>(import.meta.glob<{ default: Customer }>("./customers/*.ts"));
const references = importCollection<Reference>(import.meta.glob<{ default: Reference }>("./references/**/*.{tsx,ts}"));

const fullData: FullData = {
  tags,
  services,
  customers,
  references,
};

export default fullData;
export { customers, references, services, tags, achievements, events, publications, releases, productions };
