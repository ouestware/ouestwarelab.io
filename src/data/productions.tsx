import type { Publication } from "./types.ts";

const productions: Publication[] = [
  {
    id: "graphcommons-v2",
    dataType: "publication",
    reference: "graphcommons",
    type: "production",
    label: {
      fr: "GraphCommons",
      en: "GraphCommons",
    },
    description: {
      fr: (
        <>Cette nouvelle version bêta entièrement refaite de GraphCommons a nécessité plus d'un an de développement.</>
      ),
      en: <>This entirely revamped beta version of GraphCommons took more than a year to develop.</>,
    },
    date: new Date("2022-11-22"),
    url: "https://graphcommons.com",
  },
  {
    id: "graphcommons-ssr",
    dataType: "publication",
    reference: "graphcommons",
    type: "production",
    label: {
      fr: "GraphCommons",
      en: "GraphCommons",
    },
    description: {
      fr: <>Une nouvelle version avec du rendu côté serveur, qui améliore entre autres le référencement.</>,
      en: (
        <>A new version with server-side rendering, which improves, among other things, search engine optimization.</>
      ),
    },
    date: new Date("2024-02-14"),
    url: "https://graphcommons.com",
  },

  {
    id: "bibliograph",
    dataType: "publication",
    reference: "bibliograph",
    type: "production",
    label: {
      fr: "Bibliograph",
      en: "Bibliograph",
    },
    description: {
      fr: (
        <>
          Cet outil avec un périmètre bien défini est publié après un premier sprint avec le client, puis quelques
          itérations rapides.
        </>
      ),
      en: (
        <>
          This tool, with a well-defined scope, is published after an initial sprint with the client, followed by a few
          rapid iterations.
        </>
      ),
    },
    date: new Date("2023-08-25"),
    url: "https://tommv.github.io/bibliograph",
  },

  {
    id: "archelec-v1",
    dataType: "publication",
    reference: "archelec",
    type: "production",
    label: {
      fr: "Archelec",
      en: "Archelec",
    },
    description: {
      fr: <>La première version comprend le moteur de recherche, ainsi que les pages professions de foi.</>,
      en: <>The first version includes the search engine as well as the "professions of faith" pages.</>,
    },
    date: new Date("2021-07-01"),
    url: "https://archelec.sciencespo.fr/",
  },
  {
    id: "archelec-v2",
    dataType: "publication",
    reference: "archelec",
    type: "production",
    label: {
      fr: "Archelec",
      en: "Archelec",
    },
    description: {
      fr: <>Cette nouvelle version embarque la page des visualisations de données.</>,
      en: <>This new version brings the data visualizations page and various fine tunings.</>,
    },
    date: new Date("2022-03-30"),
    url: "https://archelec.sciencespo.fr/",
  },

  {
    id: "cujas-v1",
    dataType: "publication",
    reference: "hoppe",
    type: "production",
    label: {
      fr: "HOPPE-Droit",
      en: "HOPPE-Droit",
    },
    description: {
      fr: <>Cette première version contient déjà l'essentiel de l'application.</>,
      en: <>This first version already contains the essentials of the application.</>,
    },
    date: new Date("2020-12-09"),
    url: "https://bcujas-hoppe.univ-paris1.fr/",
  },
  {
    id: "cujas-v2",
    dataType: "publication",
    reference: "hoppe",
    type: "production",
    label: {
      fr: "HOPPE-Droit",
      en: "HOPPE-Droit",
    },
    description: {
      fr: (
        <>
          Cette nouvelle version embarque{" "}
          <a href="https://bcujas-hoppe.univ-paris1.fr/editeur/35599/genealogie">la généalogie des éditeurs</a>, et
          divers <i>bug fixes</i>.
        </>
      ),
      en: (
        <>
          This new version includes the{" "}
          <a href="https://bcujas-hoppe.univ-paris1.fr/editeur/35599/genealogie">genealogy of publishers</a> and various
          bug fixes.
        </>
      ),
    },
    date: new Date("2021-06-22"),
    url: "https://bcujas-hoppe.univ-paris1.fr/",
  },
  {
    id: "cujas-v3",
    dataType: "publication",
    reference: "hoppe",
    type: "production",
    label: {
      fr: "HOPPE-Droit",
      en: "HOPPE-Droit",
    },
    description: {
      fr: (
        <>
          Cette version, dans le cadre de notre contrat de maintenance, ajoute la gestion des{" "}
          <a href="https://bcujas-hoppe.univ-paris1.fr/collection/36203">collections</a>, et met à jour les dépendances
          du code.
        </>
      ),
      en: (
        <>
          This version, as part of our maintenance contract, adds the management of{" "}
          <a href="https://bcujas-hoppe.univ-paris1.fr/collection/36203">collections</a> and updates the code
          dependencies.
        </>
      ),
    },
    date: new Date("2024-04-11"),
    url: "https://bcujas-hoppe.univ-paris1.fr/",
  },

  {
    id: "lasso-v1",
    dataType: "publication",
    reference: "lasso",
    type: "production",
    label: {
      fr: "Lasso",
      en: "Lasso",
    },
    description: {
      fr: <>La première version a déjà la quasi-totalité des fonctionnalités.</>,
      en: <>The first version already has almost all of the functionalities.</>,
    },
    date: new Date("2023-02-24"),
    url: "https://universite-gustave-eiffel.github.io/lasso/",
  },
  {
    id: "lasso-v2",
    dataType: "publication",
    reference: "lasso",
    type: "production",
    label: {
      fr: "Lasso",
      en: "Lasso",
    },
    description: {
      fr: <>Une nouvelle version qui ajoute la gestion du son et des images dans l'outil d'exploration.</>,
      en: <>A new version that adds sound and image management to the exploration tool.</>,
    },
    date: new Date("2024-03-26"),
    url: "https://universite-gustave-eiffel.github.io/lasso/",
  },
];

export default productions;
