import type { Publication } from "./types.ts";

const publications: Publication[] = [
  {
    id: "2023-fosdem-data-control",
    dataType: "publication",
    type: "conference",
    authors: ["Paul Girard"],
    label: {
      fr: "Better Engineer-Researcher Collaborations Though Data Control",
      en: "Better Engineer-Researcher Collaborations Though Data Control",
    },
    date: new Date("2023-02-04"),
    event: "fosdem-2023",
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    url: "https://archive.fosdem.org/2023/schedule/event/openresearch_research_data_control/",
  },
  {
    id: "2024-fosdem-gephi-lite",
    dataType: "publication",
    type: "conference",
    authors: ["Paul Girard", "Alexis Jacomy", "Benoit Simard"],
    label: {
      fr: "Bridging Research and Open Source: The Genesis of Gephi Lite",
      en: "Bridging Research and Open Source: The Genesis of Gephi Lite",
    },
    date: new Date("2024-02-03"),
    event: "fosdem-2024",
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    url: "https://fosdem.org/2024/schedule/event/fosdem-2024-3253-bridging-research-and-open-source-the-genesis-of-gephi-lite/",
  },
  {
    id: "2024-fosdem-osrd",
    dataType: "publication",
    type: "conference",
    authors: ["Alexis Jacomy"],
    label: {
      fr: "Bending geographic maps for enhanced railway space-time diagrams",
      en: "Bending geographic maps for enhanced railway space-time diagrams",
    },
    date: new Date("2024-02-03"),
    event: "fosdem-2024",
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    url: "https://fosdem.org/2024/schedule/event/fosdem-2024-2594-bending-geographic-maps-for-enhanced-railway-space-time-diagrams/",
  },
  {
    id: "2025-fosdem-sigma",
    dataType: "publication",
    type: "conference",
    authors: ["Alexis Jacomy"],
    label: {
      fr: "Developing Custom UIs to Explore Graph Databases Using Sigma.js",
      en: "Developing Custom UIs to Explore Graph Databases Using Sigma.js",
    },
    date: new Date("2025-02-01"),
    event: "fosdem-2025",
    location: {
      fr: "Bruxelles, Belgique",
      en: "Brussels, Belgium",
    },
    url: "https://fosdem.org/2025/schedule/event/fosdem-2025-5614-developing-custom-uis-to-explore-graph-databases-using-sigma-js/",
  },
  {
    id: "2023-public-datalab",
    dataType: "publication",
    type: "paper",
    authors: ["OuestWare", "Public Data Lab"],
    label: {
      fr: "Network exploration on the web: an interview with Gephi Lite",
      en: "Network exploration on the web: an interview with Gephi Lite",
    },
    date: new Date("2023-05-23"),
    url: "https://publicdatalab.org/2023/05/23/gephi-lite-interview/",
  },
  {
    id: "2022-humanites-numeriques",
    dataType: "publication",
    type: "paper",
    authors: ["Diego Antolinos-Basso", "Audrey Baneyx", "Héloïse Thero", "Benjamin Ooghe", "Paul Girard"],
    label: {
      fr: "L’atelier de méthodes de Sciences Po : apprendre, aider, rassembler",
      en: "L’atelier de méthodes de Sciences Po : apprendre, aider, rassembler",
    },
    location: {
      fr: <>Humanités numériques, Enseigner et apprendre les humanités numériques</>,
      en: <>Humanités numériques, Enseigner et apprendre les humanités numériques</>,
    },
    date: new Date("2022-05"),
    url: "https://doi.org/10.4000/revuehn.2799",
  },
  {
    id: "2022-toflit18",
    dataType: "publication",
    type: "paper",
    authors: ["Loïc Charles", "Guillaume Daudin", "Paul Girard", "Guillaume Plique"],
    label: {
      fr: "Exploring the Transformation of French Trade in the Long Eighteenth Century (1713–1823): The TOFLIT18 Project",
      en: "Exploring the Transformation of French Trade in the Long Eighteenth Century (1713–1823): The TOFLIT18 Project",
    },
    date: new Date("2022-04-18"),
    location: {
      fr: "Historical Methods: A Journal of Quantitative and Interdisciplinary History",
      en: "Historical Methods: A Journal of Quantitative and Interdisciplinary History",
    },
    url: "https://doi.org/10.1080/01615440.2022.2032522",
  },
  {
    id: "2021-ricardo",
    dataType: "publication",
    type: "paper",
    authors: ["Béatrice Dedinger", "Paul Girard"],
    label: {
      fr: "How many countries in the world? The geopolitical entities of the world and their political status from 1816 to the present",
      en: "How many countries in the world? The geopolitical entities of the world and their political status from 1816 to the present",
    },
    date: new Date("2021-07-02"),
    location: {
      fr: "Historical Methods: A Journal of Quantitative and Interdisciplinary History",
      en: "Historical Methods: A Journal of Quantitative and Interdisciplinary History",
    },
    url: "https://doi.org/10.1080/01615440.2021.1939826",
  },
  {
    id: "2020-toflit18",
    dataType: "publication",
    type: "conference",
    reference: "toflit18",
    authors: ["Paul Girard", "Guillaume Daudin", "Loïc Charles", "Guillaume Plique"],
    label: {
      fr: "L’exploration visuelle comme instrument de recherche en histoire économique et sociale",
      en: "L’exploration visuelle comme instrument de recherche en histoire économique et sociale",
    },
    date: new Date("2020-05-13"),
    location: {
      fr: (
        <>
          <a href="https://humanistica2020.sciencesconf.org/">Humanistica 2020</a>, Bordeaux, France
        </>
      ),
      en: (
        <>
          <a href="https://humanistica2020.sciencesconf.org/">Humanistica 2020</a>, Bordeaux, France
        </>
      ),
    },
    url: "http://medialab.github.io/publications/explorationVisuelle@humanistica2020/",
  },
  {
    id: "2020-ricardo",
    dataType: "publication",
    type: "conference",
    reference: "ricardo",
    authors: ["Paul Girard", "Béatrice Dedinger"],
    label: {
      fr: "Pourquoi et comment géolocaliser le commerce mondial des XIXe-miXXe siècles ?",
      en: "Pourquoi et comment géolocaliser le commerce mondial des XIXe-miXXe siècles ?",
    },
    date: new Date("2020-05-13"),
    location: {
      fr: (
        <>
          <a href="https://humanistica2020.sciencesconf.org/">Humanistica 2020</a>, Bordeaux, France
        </>
      ),
      en: (
        <>
          <a href="https://humanistica2020.sciencesconf.org/">Humanistica 2020</a>, Bordeaux, France
        </>
      ),
    },
    url: "http://medialab.github.io/publications/geolocaliserRICardo@humanistica2020/",
  },
  {
    id: "2022-tremulator",
    dataType: "publication",
    type: "conference",
    authors: ["David F. Johnson", "Paul Girard", "Benoit Simard"],
    label: {
      fr: "Studying Signs of Use in Medieval Manuscripts: data collection through annotations",
      en: "Studying Signs of Use in Medieval Manuscripts: data collection through annotations",
    },
    date: new Date("2022-07-28"),
    location: {
      fr: (
        <>
          <a href="https://dh2022.adho.org/">Digital Humanities 2022</a>, Tokyo, Japon
        </>
      ),
      en: (
        <>
          <a href="https://dh2022.adho.org/">Digital Humanities 2022</a>, Tokyo, Japan
        </>
      ),
    },
    url: "http://medialab.github.io/publications/geolocaliserRICardo@humanistica2020/",
  },
  {
    id: "2019-devfest",
    dataType: "publication",
    type: "conference",
    authors: ["Alexis Jacomy"],
    label: {
      fr: "Svelte, la nouvelle révolution des applications web",
      en: "Svelte, la nouvelle révolution des applications web",
    },
    date: new Date("2019-10-21"),
    location: {
      fr: (
        <>
          <a href="https://devfest2019.gdgnantes.com/">DevFest Nantes 2019</a>, Nantes, France
        </>
      ),
      en: (
        <>
          <a href="https://devfest2019.gdgnantes.com/">DevFest Nantes 2019</a>, Nantes, France
        </>
      ),
    },
    url: "https://devfest2019.gdgnantes.com/sessions/svelte__la_nouvelle_revolution_des_applications_web/",
  },
];

export default publications;
