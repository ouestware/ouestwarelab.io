import { type Tag } from "./types";

const tags: Tag[] = [
  {
    id: "web-dev",
    label: {
      fr: "Développement web",
      en: "Web development",
    },
  },
  {
    id: "dataviz",
    label: {
      fr: "Visualisation de données",
      en: "Data visualisation",
    },
  },
  {
    id: "dashboards",
    label: {
      fr: "Tableaux de bord",
      en: "Dashboards",
    },
  },
  { id: "shs", label: { fr: "SHS", en: "Social Sciences" } },
  { id: "python", label: { fr: "Python", en: "Python" } },
  { id: "javascript", label: { fr: "JavaScript", en: "JavaScript" } },
  { id: "clojure", label: { fr: "Clojure", en: "Clojure" } },
  { id: "clojurescript", label: { fr: "ClojureScript", en: "ClojureScript" } },
  { id: "elasticsearch", label: { fr: "ElasticSearch", en: "ElasticSearch" } },
  { id: "kibana", label: { fr: "Kibana", en: "Kibana" } },
  { id: "devops", label: { fr: "DevOps", en: "DevOps" } },
  { id: "dashboard", label: { fr: "Tableau de bord", en: "Dashboard" } },
  { id: "openstack", label: { fr: "OpenStack", en: "OpenStack" } },
  { id: "opensource", label: { fr: "Open source", en: "Open source" } },
  { id: "realtime-data", label: { fr: "Données temps réel", en: "Realtime data" } },
  { id: "architecture", label: { fr: "Architecture", en: "Architecture" } },
  { id: "conception", label: { fr: "Conception", en: "Conception" } },
  { id: "data-science", label: { fr: "Science des données", en: "Data science" } },
  { id: "visual-analysis", label: { fr: "Analyse visuelle", en: "Visual analysis" } },
  { id: "nlp", label: { fr: "Traitement Automatique de la Langue", en: "Natural Language Processing" } },
  { id: "web-performance", label: { fr: "Performance Web", en: "Web performance" } },
  { id: "neo4j", label: { fr: "Neo4j", en: "Neo4j" } },
  { id: "react", label: { fr: "React", en: "React" } },
  { id: "angular", label: { fr: "Angular", en: "Angular" } },
  { id: "sigma", label: { fr: "Sigma.js", en: "Sigma.js" } },
  { id: "baobab", label: { fr: "Baoab", en: "Baobab" } },
  { id: "digital-humanities", label: { fr: "Humanités numériques", en: "Digital humanities" } },
  { id: "data-journalism", label: { fr: "Journalisme de données", en: "Data journalism" } },
  { id: "industry", label: { fr: "Industrie", en: "Industry" } },
  { id: "heurist", label: { fr: "Heurist", en: "Heurist" } },
  { id: "angularjs", label: { fr: "AngularJS", en: "AngularJS" } },
  { id: "typescript", label: { fr: "TypeScript", en: "TypeScript" } },
  { id: "graphology", label: { fr: "Graphology", en: "Graphology" } },
  { id: "ruby", label: { fr: "Ruby", en: "Ruby" } },
  { id: "maplibre", label: { fr: "MapLibre", en: "MapLibre" } },
  { id: "rh", label: { fr: "Ressources Humaines", en: "Human Resources" } },
];

export default tags;
