import { type ComponentType, type FC } from "react";

const Wrap: FC<{ component: ComponentType } | { content: JSX.Element }> = (props) => {
  if ("component" in props) {
    const Component = props.component;
    return <Component />;
  }

  return <>{props.content}</>;
};

export default Wrap;
