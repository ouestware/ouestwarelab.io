import type { Translations } from ".";

const translations: Translations = {
  fr: {
    "footer-credits": () => (
      <>
        Site généré avec <a href="https://astro.build/">Astro</a>
      </>
    ),
    "contact-us": "Contactez-nous",
    "footer-alliance-libre": (
      <>
        OuestWare est membre du réseau <a href="http://www.alliance-libre.org/">Alliance Libre</a>
      </>
    ),
    "footer-legals": "Mentions légales",
    "footer-services": "Nos services",
    "footer-references": "Nos références",
    "footer-blog": "Notre blog",
    "footer-blog-ouestware": "Blog Ouestware",
    "footer-logo-gitlab": "Logo GitLab",
    "footer-logo-linkedin": "Logo LinkedIn",
    "footer-logo-twitter": "Logo Twitter",
    "footer-logo-mastodon": "Logo Mastodon",
    "footer-sign-arobase": "Signe @",
  },
  en: {
    "footer-credits": () => (
      <>
        Website generated by <a href="https://astro.build/">Astro</a>
      </>
    ),
    "contact-us": "Contact us",
    "footer-alliance-libre": (
      <>
        OuestWare is a member of the <a href="http://www.alliance-libre.org/">Alliance Libre</a> network
      </>
    ),
    "footer-legals": "Legal Notice",
    "footer-services": "Our services",
    "footer-references": "Our productions",
    "footer-blog": "Our blog",
    "footer-blog-ouestware": "OuestWare blog",
    "footer-logo-gitlab": "GitLab logo",
    "footer-logo-linkedin": "LinkedIn logo",
    "footer-logo-twitter": "Twitter logo",
    "footer-logo-mastodon": "Logo Mastodon",
    "footer-sign-arobase": "@ sign",
  },
};

export default translations;
