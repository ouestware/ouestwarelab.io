import type { Translations } from ".";

const headerTranslations: Translations = {
  fr: {
    "blog-name": "les actualités",
    "header-navigation": "Navigation",
    "header-home": "OuestWare",
    "header-services": "Services",
    "header-references": "Références",
    "header-blog": "Actualités",
  },
  en: {
    "blog-name": "activity",
    "header-navigation": "Navigation",
    "header-home": "OuestWare",
    "header-services": "Services",
    "header-references": "Productions",
    "header-blog": "News",
  },
};

export default headerTranslations;
