import type { Translations } from ".";

const translations: Translations = {
  fr: {
    "services-page-title": "OuestWare - Nos services",
    "services-title": "Nos services",
    "services-your-needs": "Vos besoins",
    "services-our-solutions": "Nos propositions",
    "services-showcase-project": "Découvrir le projet ",
    "services-showcase-customer": " pour ",
    "services-references-presentation": "Vous souhaitez voir plus précisément nos projets déjà réalisés ?",
    "services-open-references": "Découvrez nos références",
  },
  en: {
    "services-page-title": "OuestWare - Our services",
    "services-title": "Our services",
    "services-your-needs": "Your needs",
    "services-our-solutions": "Our propositions",
    "services-showcase-project": "Discover the project ",
    "services-showcase-customer": " for ",
    "services-references-presentation": "You want to know more about our past projects?",
    "services-open-references": "Learn more about our references",
  },
};

export default translations;
