import type { Translations } from "./index";

const legalTranslations: Translations = {
  fr: {
    "legals-page-title": "OuestWare - Mentions légales",
    "legals-title": "Mentions légales",
    "legals-content": () => (
      <article>
        <h2>
          <span className="highlight">OuestWare</span>
        </h2>

        <p>
          20 rue du Clos Siban
          <br />
          44800 Saint-Herblain
          <br />
          France
        </p>

        <p>SARL au capital de 6000&nbsp;&euro;</p>
        <p>SIREN&nbsp;: 877 975 250, au RCS de Nantes</p>
        <p>N° TVA intracommunautaire&nbsp;: FR 81 877975250</p>
        <p>Code APE&nbsp;: 6201Z</p>
      </article>
    ),
  },
  en: {
    "legals-page-title": "OuestWare - Legal Notice",
    "legals-title": "Legal Notice",
    "legals-content": () => (
      <article>
        <h2>OuestWare</h2>

        <p>
          20 rue du Clos Siban
          <br />
          44800 Saint-Herblain
          <br />
          France
        </p>

        <p>Registration number (SIREN)&nbsp;: 877 975 250 at the "chambre de commerce (RCS)" of Nantes</p>
        <p>VAT Number&nbsp;: FR 81 877975250</p>
      </article>
    ),
  },
};

export default legalTranslations;
