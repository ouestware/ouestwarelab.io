import type { FC } from "react";

import { type LanguageCode, type Translation, type TranslationObject, type Translations, useTranslatedPath } from ".";
import commonTranslations from "./common";

export type PlainObject<T = any> = { [k: string]: T };

export function translate(
  translations: Translations,
  lang: LanguageCode,
  key: string,
  params?: PlainObject,
): JSX.Element | string {
  return renderTranslation((translations[lang] || {})[key] || key, params);
}

export function translateObject(
  translation: TranslationObject,
  lang: LanguageCode,
  params?: PlainObject,
): JSX.Element | string {
  return renderTranslation(translation[lang] || "", params);
}

export function renderTranslation(Value: Translation, params?: PlainObject): JSX.Element | string {
  return typeof Value === "function" ? <Value {...(params || {})} /> : Value;
}

/**
 * This functions create a react component which render translations
 */
export const useTranslate = (lang: LanguageCode, translations?: Translations) => {
  const translationsWithCommons = {
    fr: { ...commonTranslations.fr, ...(translations || {}).fr },
    en: { ...commonTranslations.en, ...(translations || {}).en },
  };
  const Translate: FC<{ k: string; params?: PlainObject }> = ({ k, params }) => {
    return translate(translationsWithCommons, lang, k, params);
  };
  const TO: FC<{ o: TranslationObject; params?: PlainObject }> = ({ o, params }) => {
    return <>{translateObject(o, lang, params)}</>;
  };
  const translatePath = useTranslatedPath(lang);

  return {
    T: Translate,
    t: (k: string, params?: PlainObject) => translate(translationsWithCommons, lang, k, params) as string,
    to: (object: TranslationObject, params?: PlainObject) => translateObject(object, lang, params) as string,
    TO,
    translatePath,
  };
};
