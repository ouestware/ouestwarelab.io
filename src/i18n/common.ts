import type { Translations } from "./index";

const commonTranslations: Translations = {
  fr: {
    "site-name": "OuestWare",
    "site-description": "Agence nantaise de solutions d'analyse de données",
    "site-full-description": "OuestWare, agence nantaise de solutions d'analyse de données",
    "ouestware-team": "L'équipe OuestWare",
    "404-title": "Page non trouvée",
    "404-content": "Désolé nous ne trouvons pas la page que vous cherchez.",
    post: "Blog",
    event: "Événement",
    release: "Version",
    paper: "Publication",
    production: "Mise en prod",
    conference: "Conférence",
  },
  en: {
    "site-name": "OuestWare",
    "site-description": "Power your solutions of data analysis, with a French touch",
    "site-full-description": "OuestWare, Open code for data",
    "ouestware-team": "The OuestWare team",
    "404-title": "Page not found",
    "404-content": "Sorry we can't find the page you're looking for.",
    post: "Blog",
    event: "Event",
    release: "Release",
    paper: "Publication",
    production: "Production",
    conference: "Conference",
  },
};

export default commonTranslations;
