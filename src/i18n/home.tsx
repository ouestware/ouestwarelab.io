import type { Translations } from ".";

const translations: Translations = {
  fr: {
    "contact-us": "Contactez-nous",

    "values-foss-title": "La culture du Libre",
    "values-foss-content": () => (
      <>
        Nous produisons et utilisons des{" "}
        <a href="https://fr.wikipedia.org/wiki/Free/Libre_Open_Source_Software">
          logiciel libre et à code source ouvert
        </a>
        . Nous partageons régulièrement nos connaissances dans des conférences telles que le{" "}
        <a href="https://fosdem.org/">FOSDEM</a> ou encore <a href="https://nantesjs.org">Nantes.js</a>.
      </>
    ),
    "values-data-title": "Le travail des données",
    "values-data-content": () => (
      <>
        Que les données soient tabulaires ou <a href="https://fr.wikipedia.org/wiki/Science_des_réseaux">connectées</a>,
        nous savons les fouiller, les raffiner et les visualiser, pour permettre de mieux les exploiter. C’est notre
        coeur de métier.
      </>
    ),
    "values-web-title": "La pratique du Web",
    "values-web-content": () => (
      <>
        Nous concevons nos applications pour le Web. Nous suivons les nouvelles technologies, pour développer des outils
        portables, performants et légers, basés sur les derniers standards.
      </>
    ),

    "methods-technologies": "Méthode et technologies",
    "methods-technologies-data":
      "La modélisation des données est un fondement important de tous nos projets. En fonction de la nature des " +
      "données et des questions que l’on souhaite leur poser, nous mobilisons des bases de données relationnelles, " +
      "connectées ou orientées document.",
    "methods-technologies-data-processing":
      "Les requêtes et algorithmes de traitement des données sont orchestrés par un serveur d’application développé " +
      "sur-mesure, avec une puissance de calcul adaptée au besoin.",
    "methods-technologies-front":
      "Les experts métier peuvent accéder aux données et les manipuler depuis des interfaces web, délivrant une " +
      "expérience de navigation au coeur des données.",
    "methods-technologies-dataviz":
      "Visualiser les données, c’est permettre de mieux les lire pour leur donner du sens. L’interactivité permet de " +
      "multiplier les points de vue, favorisant une meilleure compréhension des enjeux métier.",

    "open-references": "Découvrez nos références",

    "team-benoit": () => (
      <>
        Benoît (<a href="https://twitter.com/logisima">@logisima</a>) est un expert dans le développement web et le
        traitement des graphes grâce à ses passages chez <a href="https://www.smile.eu/fr">Smile</a> et{" "}
        <a href="https://neo4j.com">Neo4j</a>. Il apprécie d’accompagner les projets au delà du code, depuis la
        conception jusqu’à la production et les opérations.
      </>
    ),
    "team-alexis": () => (
      <>
        Alexis (
        <a rel="me" href="https://vis.social/@jacomyal">
          @jacomyal
        </a>
        ) est un expert dans le développement web et l’exploration et la visualisation des données. Créateur de{" "}
        <a href="http://www.sigmajs.org/">sigma.js</a>, il aime explorer les capacités avancées des navigateurs, sans
        jamais négliger l’expérience utilisateur.
      </>
    ),
    "team-paul": () => (
      <>
        Paul (
        <a rel="me" href="https://piaille.fr/@paulanomalie">
          @paulanomalie
        </a>
        ) est un expert des{" "}
        <a href="https://fr.wikipedia.org/wiki/Humanit%C3%A9s_num%C3%A9riques">humanités numériques</a> après 10 ans
        passés comme directeur technique du <a href="https://medialab.sciencespo.fr">médialab de Sciences-Po</a>.
        Ingénieur aspirant designer, il cherche à développer les meilleures interfaces homme-données.
      </>
    ),

    // Images alt titles:
    "img-ouestware": "Logo OuestWare",
    "img-neo4j": "Neo4j",
    "img-postgressql": "Logo PostgresSQL",
    "img-elasticsearch": "Logo ElasticSearch",
    "img-graphql": "Logo GraphQL",
    "img-java": "Logo Java",
    "img-node-js": "Node.js",
    "img-react": "Logo react",
    "img-angular": "Logo Angular",
    "img-javascript": "Logo javascript",
    "img-sigma.js": "Logo Sigma.js",
    "img-d3.js": "Logo D3.js",
    "img-html5": "Logo HTML5",
    "img-benoit": "Photo de Benoît Simard",
    "img-alexis": "Photo de Alexis Jacomy",
    "img-paul": "Photo de Paul Girard",
  },
  en: {
    "contact-us": "Contact us",

    "values-foss-title": "Free culture",
    "values-foss-content": () => (
      <>
        We produce and use{" "}
        <a href="https://en.wikipedia.org/wiki/Free/Libre_Open_Source_Software">free/libre and open-source software</a>.
        We regularly share our knowledge in various conferences such as <a href="https://fosdem.org/">FOSDEM</a> or{" "}
        <a href="https://devfest.gdgnantes.com/">Nantes DevFest</a>.
      </>
    ),
    "values-data-title": "Data craftsmanship",
    "values-data-content": () => (
      <>
        Dealing with data is our core business. We can mine, refine and visualize data, regardless of whether they are
        tabular or <a href="https://en.wikipedia.org/wiki/Network_science">connected</a>.
      </>
    ),
    "values-web-title": "Web oriented",
    "values-web-content": () => (
      <>
        We keep pace with the latest Web technologies, standards and good practices. We produce lightweight, portable
        and performant UIs.
      </>
    ),

    "methods-technologies": "Methodology and technologies",
    "methods-technologies-data":
      "Most projects we do start with data modeling. Depending on the nature of the data and the questions we want to " +
      "ask them, we use relational, graph or document oriented databases.",
    "methods-technologies-data-processing":
      "Queries, data wrangling and data processing algorithms run on ad-hoc application servers, with a suitable " +
      "computation power.",
    "methods-technologies-front":
      "Business experts can access and manipulate their data from web interfaces, with a data-centered navigation " +
      "experience.",
    "methods-technologies-dataviz":
      "Visualizing data enables a better reading and interpretable insights. Interactivity allows multiple points of " +
      "view, to get a better understanding of the business challenges. ",

    "open-references": "Learn more about our references",

    "team-benoit": () => (
      <>
        Benoît (<a href="https://twitter.com/logisima">@logisima</a>) is a web development and graph analysis expert. He
        worked at <a href="https://www.smile.eu/en">Smile</a> and <a href="https://neo4j.com">Neo4j</a>. He likes
        leading projects beyond just code, from design to production and operations.
      </>
    ),
    "team-alexis": () => (
      <>
        Alexis (
        <a rel="me" href="https://vis.social/@jacomyal">
          @jacomyal
        </a>
        ) is a web development and data visualization expert. He created <a href="http://www.sigmajs.org/">sigma.js</a>.
        He likes exploring browsers advanced features, and never fails to consider the experience of his users.
      </>
    ),
    "team-paul": () => (
      <>
        Paul (
        <a rel="me" href="https://piaille.fr/@paulanomalie">
          @paulanomalie
        </a>
        ) is a <a href="https://en.wikipedia.org/wiki/Digital_humanities">digital humanist</a> who worked ten years as{" "}
        <a href="https://medialab.sciencespo.fr">Sciences-Po médialab</a>'s CTO. Trained as an engineer, inspired by
        designers, he aims at developing the best human-data interfaces.
      </>
    ),

    // Images alt titles:
    "img-ouestware": "OuestWare logo",
    "img-neo4j": "Neo4j",
    "img-postgressql": "PostgresSQL logo",
    "img-elasticsearch": "ElasticSearch logo",
    "img-graphql": "GraphQL logo",
    "img-java": "Java logo",
    "img-node-js": "Node.js",
    "img-react": "React logo",
    "img-angular": "Angular logo",
    "img-javascript": "JavaScript logo",
    "img-sigma.js": "Sigma.js logo",
    "img-d3.js": "D3.js logo",
    "img-html5": "HTML5 logo",
    "img-benoit": "Benoît Simard picture",
    "img-alexis": "Alexis Jacomy picture",
    "img-paul": "Paul Girard picture",
  },
};

export default translations;
