import { type Translations } from ".";

const blogTranslations: Translations = {
  fr: {
    "blog-page-title": "OuestWare - Nos actualités",
    "blog-title": "Nos actualités",
  },
  en: {
    "blog-page-title": "OuestWare - News",
    "blog-title": "News",
  },
};

export default blogTranslations;
