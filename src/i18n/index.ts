import { type Locale, format } from "date-fns";
import { enUS, fr } from "date-fns/locale";
import type { FC } from "react";

import astroConfig from "../../astro.config.mjs";
import type { PlainObject } from "./Translate";

export type LanguageCode = "fr" | "en";

export type Translation = string | JSX.Element | FC<PlainObject>;
export type Translations = Record<LanguageCode, Record<string, Translation>>;
export type TranslationObject = Record<LanguageCode, Translation>;

const i18n = {
  defaultLocale: "fr",
  locales: ["fr", "en"],
  routing: {
    prefixDefaultLocale: false,
  },
};
export default i18n;

export const langToLocale: Record<LanguageCode, string> = {
  fr: "fr_FR",
  en: "en_US",
};

function getLangFromPath(path: string) {
  const [, lang] = path.split("/");
  if (i18n.locales.includes(lang)) return lang as LanguageCode;
  return i18n.defaultLocale as LanguageCode;
}

export function getLangFromUrl(url: URL) {
  return getLangFromPath(url.pathname);
}

export function useTranslatedPath(lang: LanguageCode) {
  return function translatePath(path: string, l: string = lang) {
    const pathLang = getLangFromPath(path);
    const pathWithoutLang = path.replace(new RegExp(`^\/${pathLang}\/`), "/");
    return !astroConfig.i18n?.routing?.prefixDefaultLocale && l === astroConfig.i18n?.defaultLocale
      ? pathWithoutLang
      : `/${l}${pathWithoutLang}`;
  };
}

const locales: Record<LanguageCode, Locale> = {
  fr,
  en: enUS,
};

const formats = {
  fr: "d LLLL yyyy",
  en: "LLLL	d, yyyy",
};

export const localeToLang: { [key: string]: string } = {
  fr_FR: "fr",
  en_US: "en",
};

export function formatDate(date: Date, lang: LanguageCode): string {
  return format(date, formats[lang], { locale: locales[lang] });
}
