import { type Translations, useTranslatedPath } from ".";

const translatePathFR = useTranslatedPath("fr");
const translatePathEN = useTranslatedPath("en");

const referencesTranslations: Translations = {
  fr: {
    "reference-page-title": "OuestWare - Nos références",
    "reference-title": "Nos références",
    "reference-service": ({ service }) => (
      <>
        Un projet de <a href={`${translatePathFR(`/services`)}#${service.id}`}>{service.title.fr}</a>
      </>
    ),
    "reference-confidential-customer": "Client confidentiel",
    "reference-services-presentation": "Vous souhaitez voir plus en détails notre offre ?",
    "reference-open-services": "Découvrez nos services",
    "reference-link-icon": "Lien vers :",
    "reference-left-arrow": "Passer à l'image précédente",
    "reference-right-arrow": "Passer à l'image suivante",
  },
  en: {
    "reference-page-title": "OuestWare - Our productions",
    "reference-title": "Our productions",
    "reference-service": ({ service }) => (
      <>
        A <a href={`${translatePathEN(`/services`)}#${service.id}`}>{service.title.en}</a> project
      </>
    ),
    "reference-confidential-customer": "Confidential customer",
    "reference-services-presentation": "You want to know more about what we do?",
    "reference-open-services": "Learn more about our services",
    "reference-link-icon": "Link to:",
    "reference-left-arrow": "Go to previous image",
    "reference-right-arrow": "Go to next image",
  },
};

export default referencesTranslations;
